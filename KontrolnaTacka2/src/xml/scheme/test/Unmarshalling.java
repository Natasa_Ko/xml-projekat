package xml.scheme.test;

import java.io.File;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.JAXBIntrospector;
import javax.xml.bind.Unmarshaller;


import xml.scheme.model.Resenja;

public class Unmarshalling {

	public void unmarshallResenja(File file) throws JAXBException {
		
		JAXBContext context = JAXBContext.newInstance("xml.scheme.model");
		Unmarshaller unmarshaller = context.createUnmarshaller();
		Resenja resenja = (Resenja) unmarshaller.unmarshal(file);
		
		printResenja(resenja);
	}

	private void printResenja(Resenja resenja) {
		
		System.out.println("-Resenja");
		System.out.println("\t-Resenje");
		System.out.println("\t[Tip: "+resenja.getResenje().getTip()+"]");
		System.out.println("\t[Broj: "+resenja.getResenje().getBroj()+"]");
		System.out.println("\t[Datum: "+resenja.getResenje().getDatum()+"]");
		System.out.println("\t\t-Uvod");
		System.out.println("\t\t\t-Podnosilac zalbe: ");
		System.out.println("\t\t\t\t-Ime: " + resenja.getResenje().getUvod().getPodnosilacZalbe().getIme());
		System.out.println("\t\t\t\t-Prezime: " + resenja.getResenje().getUvod().getPodnosilacZalbe().getPrezime());
		System.out.println("\t\t\t-Razlog zalbe: "+ resenja.getResenje().getUvod().getRazlogZalbe());
		System.out.println("\t\t\t-Datum zahteva: "+ resenja.getResenje().getUvod().getDatumZahteva());
		System.out.println("\t\t\t-Zakon: "+ resenja.getResenje().getUvod().getZakon());
		
		System.out.println("\t\t-Tekst resenja");
		System.out.println("\t\t\t-Primalac resenja: " + resenja.getResenje().getTekstResenja().getPrimalacResenja());
		
		System.out.println("\t\t-Tekst obrazlozenja");
		System.out.println("\t\t\t-Razlog zalbe: "+ resenja.getResenje().getTekstObrazlozenja().getRazlogZalbe());
		System.out.println("\t\t\t-Razlog zalbe: "+ resenja.getResenje().getTekstObrazlozenja().getPostupanjePoZalbi());
		System.out.println("\t\t\t-Razlog zalbe: "+ resenja.getResenje().getTekstObrazlozenja().getStaJeUtvrdjeno());
		System.out.println("\t\t\t-Razlog zalbe: "+ resenja.getResenje().getTekstObrazlozenja().getStaJeNaredjeno());
		System.out.println("\t\t\t-Razlog zalbe: "+ resenja.getResenje().getTekstObrazlozenja().getUputstvoOPravnomSredstvu());
		
		System.out.println("\t\t-Poverenik");
		System.out.println("\t\t\t-Ime: "+resenja.getResenje().getPoverenik().getIme());
		System.out.println("\t\t\t-Prezime: "+resenja.getResenje().getPoverenik().getPrezime());
	}
}
