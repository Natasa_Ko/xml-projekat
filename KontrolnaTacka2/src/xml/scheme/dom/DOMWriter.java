package xml.scheme.dom;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class DOMWriter {

	private static String TARGET_NAMESPACE = "http://localhost:8080/xml";
	private static String XSI_NAMESPACE = "http://www.w3.org/2001/XMLSchema-instance";
	
	private DocumentBuilderFactory factory;
	private TransformerFactory transformerFactory;
	private Document document;
	
	public DOMWriter() {
		factory = DocumentBuilderFactory.newInstance();
		transformerFactory = TransformerFactory.newInstance();
	}
	
	public void createDocument() throws ParserConfigurationException {
		DocumentBuilder builder = factory.newDocumentBuilder();
		document = builder.newDocument();
	}
	
	public void generateDOM() throws TransformerFactoryConfigurationError, TransformerException {
		 Element root = document.createElementNS(TARGET_NAMESPACE, "obavestenje");	
		 document.appendChild(root);
		 root.setAttributeNS(XSI_NAMESPACE, "xsi:schemaLocation", "http://localhost:8080/xml");
		 root.setAttribute("naziv_i_sediste_organa", "");
		 root.setAttribute("broj_predmeta", "");
		 root.setAttribute("datum", "");
		 root.setAttribute("ime_i_prezime_naziv", "");
		 root.setAttribute("adresa_podnosioca_zahteva", "");
		 Element naslov = document.createElementNS(TARGET_NAMESPACE, "naslov");
		 naslov.appendChild(document.createTextNode(" О Б А В Е Ш Т Е Њ Е \r\n" + 
		 		"            о стављању на увид документа који садржи\r\n" + 
		 		"            тражену информацију и о изради копије"));
		 naslov.setTextContent(" О Б А В Е Ш Т Е Њ Е \r\n" + 
		 		"            о стављању на увид документа који садржи\r\n" + 
		 		"            тражену информацију и о изради копије");
		 root.appendChild(naslov);
		 
		 Element trazena_informacija = document.createElementNS(TARGET_NAMESPACE, "trazena_informacija");
		 trazena_informacija.appendChild(document.createTextNode("На основу члана 16. ст. 1. Закона о слободном приступу информацијама од јавног значаја,\r\n" + 
		 		"        поступајући по вашем захтеву за слободан приступ информацијама од"));
		 trazena_informacija.appendChild(document.createElementNS(TARGET_NAMESPACE, "datum"));
		 trazena_informacija.appendChild(document.createTextNode("год.,\r\n" + 
		 		"        којим сте тражили увид у документ/е са информацијама о / у вези са:"));
		 trazena_informacija.appendChild(document.createElementNS(TARGET_NAMESPACE, "opis_trazene_informacije"));
		 root.appendChild(trazena_informacija);
		 
		 Element o_obavestenju = document.createElementNS(TARGET_NAMESPACE, "o_obavestenju");
		 o_obavestenju.appendChild(document.createTextNode("обавештавамо вас да дана"));
		 o_obavestenju.appendChild(document.createElementNS(TARGET_NAMESPACE, "datum"));
		 o_obavestenju.appendChild(document.createTextNode("у"));
		 o_obavestenju.appendChild(document.createElementNS(TARGET_NAMESPACE, "vreme"));
		 o_obavestenju.appendChild(document.createTextNode("часова, односно у времену од"));
		 o_obavestenju.appendChild(document.createElementNS(TARGET_NAMESPACE, "od_vreme"));
		 o_obavestenju.appendChild(document.createTextNode("до"));  
		 o_obavestenju.appendChild(document.createElementNS(TARGET_NAMESPACE, "do_vreme"));
		 o_obavestenju.appendChild(document.createTextNode("часова, у просторијама органа у"));
		 o_obavestenju.appendChild(document.createElementNS(TARGET_NAMESPACE, "grad"));
		 o_obavestenju.appendChild(document.createTextNode(" ул. "));
		 o_obavestenju.appendChild(document.createElementNS(TARGET_NAMESPACE, "ulica"));
		 o_obavestenju.appendChild(document.createTextNode(" бр. "));
		 o_obavestenju.appendChild(document.createElementNS(TARGET_NAMESPACE, "broj_ulice"));
		 o_obavestenju.appendChild(document.createTextNode(",\r\n" + 
		 		"        канцеларија бр. "));
		 o_obavestenju.appendChild(document.createElementNS(TARGET_NAMESPACE, "broj_kancelarije"));
		 o_obavestenju.appendChild(document.createTextNode(" можете извршити увид у документ/е у коме је садржана тражена информација.\r\n" + 
		 		"        Том приликом, на ваш захтев, може вам се издати и копија документа са траженом информацијом."));
		 root.appendChild(o_obavestenju);
		 
		 Element troskovi = document.createElementNS(TARGET_NAMESPACE, "troskovi");
		 troskovi.appendChild(document.createTextNode("Трошкови су утврђени Уредбом Владе Републике Србије („Сл. гласник РС“, бр. 8/06),\r\n" + 
		 		"        и то: копија стране "));
		 Element format = document.createElementNS(TARGET_NAMESPACE, "format");
		 format.setAttribute("tip", "A4");;
		 format.setAttribute("cena", "3");
		 format.appendChild(document.createTextNode(" А4 формата износи 3 динара..."));
		 troskovi.appendChild(format);
		 
		 Element uplata = document.createElementNS(TARGET_NAMESPACE, "uplata");
		 uplata.appendChild(document.createTextNode("Износ укупних трошкова израде копије документа по вашем захтеву износи "));
		 uplata.appendChild(document.createElementNS(TARGET_NAMESPACE, "iznos_za_uplatu"));
		 uplata.appendChild(document.createTextNode("динара и уплаћује се на жиро-рачун Буџета Републике Србије"));
		 Element racun = document.createElementNS(TARGET_NAMESPACE, "racun");
		 racun.setAttribute("broj", "840-742328-843-30" );
		 racun.setAttribute("model", "97");
		 racun.appendChild(document.createTextNode("бр. 840-742328-843-30,\r\n" + 
		 		"        с позивом на број 97 "));
		 uplata.appendChild(racun);
		 uplata.appendChild(document.createTextNode("– ознака шифре општине/града где се налази орган власти \r\n" + 
		 		"        (из Правилника о условима и начину вођења рачуна – „Сл. гласник РС“, 20/07... 40/10). "));
		 root.appendChild(uplata);
		 
		 Element dostavljeno = document.createElementNS(TARGET_NAMESPACE, "dostavljeno");
		 dostavljeno.appendChild(document.createTextNode("Достављено:"));
		 Element opcija = document.createElementNS(TARGET_NAMESPACE, "opcija");
		 opcija.setAttribute("broj", "1");
		 opcija.appendChild(document.createTextNode("1.	Именованом 		(М.П.)"));
		 dostavljeno.appendChild(opcija);
		 Element opcija2 = document.createElementNS(TARGET_NAMESPACE, "opcija");
		 opcija2.setAttribute("broj", "2");
		 opcija2.appendChild(document.createTextNode("2.	Архиви"));
		 dostavljeno.appendChild(opcija2);
		 
		 root.appendChild(dostavljeno);
		 
		 root.appendChild(document.createElementNS(TARGET_NAMESPACE, "potpis_ovlascenog_lica"));
		 
		 
	}
	
	public void saveDocumentAsFile(String file) throws TransformerFactoryConfigurationError, TransformerException {
		
		 Transformer transformer = TransformerFactory.newInstance().newTransformer();
		 Result output = new StreamResult(new File(file));
		 Source input = new DOMSource(document);

		 transformer.transform(input, output);
	}
	
	
	public void generateDOMZalbaCutanje() throws TransformerFactoryConfigurationError, TransformerException {
		 Element root = document.createElementNS(TARGET_NAMESPACE, "zalba");	
		 document.appendChild(root);
		 
		 root.setAttributeNS(XSI_NAMESPACE, "xsi:schemaLocation", "http://localhost:8080/xml");
		
		 Element naslov = document.createElementNS(TARGET_NAMESPACE, "naslov");
		 naslov.appendChild(document.createTextNode("\r\nЖАЛБА КАДА ОРГАН ВЛАСТИ НИЈЕ ПОСТУПИО/ није поступио у целости/ ПО ЗАХТЕВУ  \r\n" + 
		 		"            ТРАЖИОЦА У ЗАКОНСКОМ  РОКУ  (ЋУТАЊЕ УПРАВЕ)\r\n"));
		 naslov.setTextContent("  ЖАЛБА КАДА ОРГАН ВЛАСТИ НИЈЕ ПОСТУПИО/ није поступио у целости/ ПО ЗАХТЕВУ  \r\n" + 
			 		"            ТРАЖИОЦА У ЗАКОНСКОМ  РОКУ  (ЋУТАЊЕ УПРАВЕ)");
		 root.appendChild(naslov);
		 
		 Element podaci_o_primaocu = document.createElementNS(TARGET_NAMESPACE, "podaci_o_primaocu");
		
		 Element kome = document.createElementNS(TARGET_NAMESPACE, "kome");
		 kome.appendChild(document.createTextNode("Повереникy за информације од јавног значаја и заштиту података о личности"));
		 podaci_o_primaocu.appendChild(kome);
		 
		 
		 Element adresa_za_postu = document.createElementNS(TARGET_NAMESPACE, "adresa_za_postu");
		
		 Element grad = document.createElementNS(TARGET_NAMESPACE, "grad");	 
		 grad.appendChild(document.createTextNode("Београд"));
		 adresa_za_postu.appendChild(grad);
		 
		 Element ulica= document.createElementNS(TARGET_NAMESPACE, "ulica");
		 ulica.appendChild(document.createTextNode("Булевар краља Александрa"));
		 adresa_za_postu.appendChild(ulica);
		 
		 Element broj = document.createElementNS(TARGET_NAMESPACE, "broj");	 
		 broj.appendChild(document.createTextNode("15"));
		 adresa_za_postu.appendChild(broj);
		 
		 podaci_o_primaocu.appendChild(adresa_za_postu);
		 root.appendChild(podaci_o_primaocu);
		 
		 Element sadrzaj = document.createElementNS(TARGET_NAMESPACE, "sadrzaj");
		 sadrzaj.appendChild(document.createTextNode("У складу са чланом 22. Закона о слободном приступу информацијама од јавног значаја подносим:\r\n" + 
		 		"      \r\n" + 
		 		"        Ж А Л Б У \r\n" + 
		 		"        против"));
		 sadrzaj.appendChild(document.createElementNS(TARGET_NAMESPACE, "naziv_organa"));
		 sadrzaj.appendChild(document.createTextNode("због тога што орган власти:"));
		 
		 Element razlog_zalbe = document.createElementNS(TARGET_NAMESPACE, "razlog_zalbe");
		 Element opcija = document.createElementNS(TARGET_NAMESPACE, "opcija");
		 opcija.setAttribute("izabrano", "");
		 opcija.appendChild(document.createTextNode("није поступио"));
		 razlog_zalbe.appendChild(opcija);
		 
		 Element opcija2 = document.createElementNS(TARGET_NAMESPACE, "opcija");
		 opcija2.setAttribute("izabrano", "");
		 opcija2.appendChild(document.createTextNode("није поступио у целости"));
		 razlog_zalbe.appendChild(opcija2);
		 
		 Element opcija3 = document.createElementNS(TARGET_NAMESPACE, "opcija");
		 opcija3.setAttribute("izabrano", "");
		 opcija3.appendChild(document.createTextNode("у законском року"));
		 razlog_zalbe.appendChild(opcija3);
		 
		 sadrzaj.appendChild(razlog_zalbe);
		 sadrzaj.appendChild(document.createTextNode("(подвући  због чега се изјављује жалба)"));
		 
		 sadrzaj.appendChild(document.createTextNode("по мом захтеву  за слободан приступ информацијама од јавног значаја који сам поднео  том\r\n" + 
		 		"            органу  дана"));
		 sadrzaj.appendChild(document.createElementNS(TARGET_NAMESPACE, "datum_podnosenja_zahteva"));		 
		 sadrzaj.appendChild(document.createTextNode("године, а којим сам тражио/ла да ми се у складу са Законом о \r\n" + 
		 		"            слободном приступу информацијама од јавног значаја омогући увид- копија документа који\r\n" + 
		 		"            садржи информације  о /у вези са :"));
		 sadrzaj.appendChild(document.createElementNS(TARGET_NAMESPACE, "podaci_o_zahtevu_i_informacijama"));
		 sadrzaj.appendChild(document.createTextNode("На основу изнетог, предлажем да Повереник уважи моју жалбу и омогући ми приступ \r\n" + 
		 		"            траженој/им  информацији/ма.\r\n" + 
		 		"\r\n" + 
		 		"            Као доказ , уз жалбу достављам копију захтева са доказом о предаји органу власти.\r\n" + 
		 		"            Напомена: Код жалбе  због непоступању по захтеву у целости, треба приложити и добијени\r\n" + 
		 		"            одговор органа власти."));		 
		 root.appendChild(sadrzaj);
		 
		 Element podnosilac_zalbe = document.createElementNS(TARGET_NAMESPACE, "podnosilac_zalbe");
		 podnosilac_zalbe.appendChild(document.createElementNS(TARGET_NAMESPACE, "ime_i_prezime"));
		 podnosilac_zalbe.appendChild(document.createElementNS(TARGET_NAMESPACE, "potpis"));
		 Element adresa = document.createElementNS(TARGET_NAMESPACE, "adresa");
		 
		 Element grad2 = document.createElementNS(TARGET_NAMESPACE, "grad");	 
		 adresa.appendChild(grad2);
		 
		 Element ulica2= document.createElementNS(TARGET_NAMESPACE, "ulica");
		 adresa.appendChild(ulica2);
		 
		 Element broj2 = document.createElementNS(TARGET_NAMESPACE, "broj");	 
		 adresa.appendChild(broj2);
		 
		 podnosilac_zalbe.appendChild(adresa);
		 root.appendChild(podnosilac_zalbe);
		 
		 Element podaci_o_zalbi = document.createElementNS(TARGET_NAMESPACE, "podaci_o_zalbi");
		 podaci_o_zalbi.appendChild(document.createTextNode("У"));
		 podaci_o_zalbi.appendChild(document.createElementNS(TARGET_NAMESPACE, "mesto"));
		 podaci_o_zalbi.appendChild(document.createTextNode(", дана"));
		 podaci_o_zalbi.appendChild(document.createElementNS(TARGET_NAMESPACE, "dan"));
		 podaci_o_zalbi.appendChild(document.createTextNode("06"));
		 podaci_o_zalbi.appendChild(document.createElementNS(TARGET_NAMESPACE, "mesec"));
		 podaci_o_zalbi.appendChild(document.createTextNode("10"));
		 podaci_o_zalbi.appendChild(document.createElementNS(TARGET_NAMESPACE, "godina"));
		 podaci_o_zalbi.appendChild(document.createTextNode("2020"));
		 podaci_o_zalbi.appendChild(document.createTextNode(". године"));
		 root.appendChild(podaci_o_zalbi);
		 
		 
	}
	
	public void generateDOMZalbaOdluka() {
		
		 Element root = document.createElementNS(TARGET_NAMESPACE, "zalba");	
		 document.appendChild(root);
		 root.setAttributeNS(XSI_NAMESPACE, "xsi:schemaLocation", "http://localhost:8080/xml");
		 
		 Element vrsta_zalbe = document.createElementNS(TARGET_NAMESPACE, "vrsta_zalbe");
		 vrsta_zalbe.appendChild(document.createTextNode(" ЖАЛБА ПРОТИВ ОДЛУКЕ ОРГАНА ВЛАСТИ КОЈОМ ЈЕ  \r\n" + 
		 		"            ОДБИЈЕН ИЛИ ОДБАЧЕН ЗАХТЕВ ЗА ПРИСТУП ИНФОРМАЦИЈИ\r\n"));
		 vrsta_zalbe.setTextContent(" ЖАЛБА ПРОТИВ ОДЛУКЕ ОРГАНА ВЛАСТИ КОЈОМ ЈЕ  \r\n" + 
			 		"            ОДБИЈЕН ИЛИ ОДБАЧЕН ЗАХТЕВ ЗА ПРИСТУП ИНФОРМАЦИЈИ\r\n");
		 root.appendChild(vrsta_zalbe);
		 
		 
		 Element poverenik = document.createElementNS(TARGET_NAMESPACE, "poverenik");
		 poverenik.appendChild(document.createTextNode("Поверенику за информације од јавног значаја и заштиту података о личности  \r\n"));
		 poverenik.setTextContent("Поверенику за информације од јавног значаја и заштиту података о личности  \r\n");
		 root.appendChild(poverenik);
		 
		 Element adresa_za_postu = document.createElementNS(TARGET_NAMESPACE, "adresa_za_postu");
		 Element grad = document.createElementNS(TARGET_NAMESPACE, "grad");
		 Element ulica = document.createElementNS(TARGET_NAMESPACE, "ulica");
		 Element broj = document.createElementNS(TARGET_NAMESPACE, "broj");
		 
		 grad.appendChild(document.createTextNode(" Београд \r\n"));
		 
		 ulica.appendChild(document.createTextNode("Булевар краља Александрa \r\n"));
		 
		 broj.appendChild(document.createTextNode("15 \r\n"));
		 
		 adresa_za_postu.appendChild(grad);
		 adresa_za_postu.appendChild(ulica);
		 adresa_za_postu.appendChild(broj);
		 root.appendChild(adresa_za_postu);
		 
		 
		 Element naslov = document.createElementNS(TARGET_NAMESPACE, "naslov");
		 naslov.appendChild(document.createTextNode(" Ж А Л Б А  \r\n"));
		 naslov.setTextContent("Ж А Л Б А  \r\n");
		 root.appendChild(naslov);
		 
		 Element informacije_o_zaliocu = document.createElementNS(TARGET_NAMESPACE, "informacije_o_zaliocu");
		 Element osoba = document.createElementNS(TARGET_NAMESPACE, "fizicko_lice");
		 Element institucija = document.createElementNS(TARGET_NAMESPACE, "pravno_lice");
		 
		 Element adresa_zalioca = document.createElementNS(TARGET_NAMESPACE, "adresa_zalioca");
		 adresa_zalioca.appendChild(document.createElementNS(TARGET_NAMESPACE, "grad"));
		 adresa_zalioca.appendChild(document.createElementNS(TARGET_NAMESPACE, "ulica"));
		 adresa_zalioca.appendChild(document.createElementNS(TARGET_NAMESPACE, "broj"));
		 
		 osoba.appendChild(document.createElementNS(TARGET_NAMESPACE, "ime_zalioca"));
		 osoba.appendChild(document.createElementNS(TARGET_NAMESPACE, "prezime_zalioca"));
		 osoba.appendChild(adresa_zalioca);
		 
		 Element adresa_zalioca1 = document.createElementNS(TARGET_NAMESPACE, "adresa_zalioca");
		 adresa_zalioca1.appendChild(document.createElementNS(TARGET_NAMESPACE, "grad"));
		 adresa_zalioca1.appendChild(document.createElementNS(TARGET_NAMESPACE, "ulica"));
		 adresa_zalioca1.appendChild(document.createElementNS(TARGET_NAMESPACE, "broj"));
		 
		 institucija.appendChild(document.createElementNS(TARGET_NAMESPACE, "naziv"));
		 institucija.appendChild(adresa_zalioca1);
		 institucija.appendChild(document.createElementNS(TARGET_NAMESPACE, "sediste"));
		 
		 informacije_o_zaliocu.appendChild(osoba);
		 informacije_o_zaliocu.appendChild(institucija);
		 root.appendChild(informacije_o_zaliocu);
		 
		 
		 Element odluka_koja_se_pobija = document.createElementNS(TARGET_NAMESPACE, "odluka_koja_se_pobija");
		 
		 odluka_koja_se_pobija.appendChild(document.createElementNS(TARGET_NAMESPACE, "naziv_organa"));
		 odluka_koja_se_pobija.appendChild(document.createElementNS(TARGET_NAMESPACE, "broj_resenja"));
		 
		 Element datum_resenja = document.createElementNS(TARGET_NAMESPACE, "datum_resenja");
		 datum_resenja.appendChild(document.createElementNS(TARGET_NAMESPACE, "dan"));
		 datum_resenja.appendChild(document.createElementNS(TARGET_NAMESPACE, "mesec"));
		 datum_resenja.appendChild(document.createElementNS(TARGET_NAMESPACE, "godina"));
		 
		 odluka_koja_se_pobija.appendChild(datum_resenja);
		 root.appendChild(odluka_koja_se_pobija);
		 
		 
		 Element razlog_zalbe = document.createElementNS(TARGET_NAMESPACE, "razlog_zalbe");
		 razlog_zalbe.appendChild(document.createTextNode(" Наведеном одлуком органа власти (решењем, закључком, обавештењем у писаној форми\r\n" + 
		 		"        са елементима одлуке) , супротно закону, одбијен-одбачен је мој захтев који сам поднео/ла-\r\n" + 
		 		"        упутио/ла дана"));
		 
		 Element datum_odbijenog_zahteva = document.createElementNS(TARGET_NAMESPACE, "datum_odbijenog_zahteva");
		 datum_odbijenog_zahteva.appendChild(document.createElementNS(TARGET_NAMESPACE, "dan"));
		 datum_odbijenog_zahteva.appendChild(document.createElementNS(TARGET_NAMESPACE, "mesec"));
		 datum_odbijenog_zahteva.appendChild(document.createElementNS(TARGET_NAMESPACE, "godina"));
		 razlog_zalbe.appendChild(datum_odbijenog_zahteva);
		 
		 razlog_zalbe.appendChild(document.createTextNode("  године и тако ми ускраћено-онемогућено остваривање уставног и \r\n" + 
		 		"        законског права на слободан приступ информацијама од јавног значаја. Oдлуку побијам у \r\n" + 
		 		"        целости, односно у делу којим"));
		 
		 razlog_zalbe.appendChild(document.createElementNS(TARGET_NAMESPACE, "sporni_dio"));
		 
		 razlog_zalbe.appendChild(document.createTextNode(" јер није заснована на Закону о слободном приступу информацијама од јавног значаја. \r\n" + 
		 		"        На основу изнетих разлога, предлажем да Повереник уважи моју жалбу, поништи \r\n" + 
		 		"        одлука првостепеног органа и омогући ми приступ траженој/им информацији/ма. \r\n" + 
		 		"        Жалбу подносим благовремено, у законском року утврђеном у члану 22. ст. 1.Закона о \r\n" + 
		 		"        слободном приступу информацијама од јавног значаја."));
		 root.appendChild(razlog_zalbe);
		 
		 Element podaci_o_zalbi = document.createElementNS(TARGET_NAMESPACE, "podaci_o_zalbi");
		 podaci_o_zalbi.appendChild(document.createTextNode("У"));
		 podaci_o_zalbi.appendChild(document.createElementNS(TARGET_NAMESPACE, "mesto"));
		 
		 Element datum_zalbe = document.createElementNS(TARGET_NAMESPACE, "datum_zalbe");
		 datum_zalbe.appendChild(document.createTextNode("дана"));
		 datum_zalbe.appendChild(document.createElementNS(TARGET_NAMESPACE, "dan"));
		 datum_zalbe.appendChild(document.createElementNS(TARGET_NAMESPACE, "mesec"));
		 datum_zalbe.appendChild(document.createTextNode("201"));
		 datum_zalbe.appendChild(document.createElementNS(TARGET_NAMESPACE, "godina"));
		 datum_zalbe.appendChild(document.createTextNode("године"));
		 
		 podaci_o_zalbi.appendChild(datum_zalbe);
		 root.appendChild(podaci_o_zalbi);
		 
		 
		 Element informacije_podnosiocu_zalbe = document.createElementNS(TARGET_NAMESPACE, "informacije_podnosiocu_zalbe");
		 informacije_podnosiocu_zalbe.appendChild(document.createElementNS(TARGET_NAMESPACE, "ime_i_prezime_podnosioca"));
		 Element adresa_podnosioca = document.createElementNS(TARGET_NAMESPACE, "adresa_podnosioca");
		 adresa_podnosioca.appendChild(document.createElementNS(TARGET_NAMESPACE, "grad"));
		 adresa_podnosioca.appendChild(document.createElementNS(TARGET_NAMESPACE, "ulica"));
		 adresa_podnosioca.appendChild(document.createElementNS(TARGET_NAMESPACE, "broj"));
		 informacije_podnosiocu_zalbe.appendChild(adresa_podnosioca);
		 informacije_podnosiocu_zalbe.appendChild(document.createElementNS(TARGET_NAMESPACE, "podaci_za_kontakt"));
		 informacije_podnosiocu_zalbe.appendChild(document.createElementNS(TARGET_NAMESPACE, "potpis_podnosioca"));
		 root.appendChild(informacije_podnosiocu_zalbe);
		 
		 Element napomena = document.createElementNS(TARGET_NAMESPACE, "napomena");
		 Element tacka = document.createElementNS(TARGET_NAMESPACE, "tacka");
		 Element tacka1 = document.createElementNS(TARGET_NAMESPACE, "tacka");
		 tacka.appendChild(document.createTextNode("У жалби се мора навести одлука која се побија (решење, закључак,\r\n" + 
		 		"            обавештење), назив органа који је одлуку донео, као и број и датум одлуке. Довољно је да\r\n" + 
		 		"            жалилац наведе у жалби у ком погледу је незадовољан одлуком, с тим да жалбу не мора\r\n" + 
		 		"            посебно образложити. Ако жалбу изјављује на овом обрасцу, додатно образложење може\r\n" + 
		 		"            посебно приложити."));
		 napomena.appendChild(tacka);
		 tacka1.appendChild(document.createTextNode("Уз жалбу обавезно приложити копију поднетог захтева и доказ о његовој\r\n" + 
		 		"            предаји-упућивању органу као и копију одлуке органа која се оспорава жалбом. "));
		 napomena.appendChild(tacka1);
		 root.appendChild(napomena);
		 
	}
	
}
