package xml.scheme.dom;

import static org.apache.xerces.jaxp.JAXPConstants.JAXP_SCHEMA_LANGUAGE;
import static org.apache.xerces.jaxp.JAXPConstants.W3C_XML_SCHEMA;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Attr;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Entity;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ProcessingInstruction;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class DOMParser {
	
	private DocumentBuilderFactory factory;
	
	private Document document;

	public DOMParser() {
		factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(false);
		
		factory.setNamespaceAware(true);
		factory.setIgnoringComments(true);
		factory.setIgnoringElementContentWhitespace(true);
		
		
		factory.setAttribute(JAXP_SCHEMA_LANGUAGE, W3C_XML_SCHEMA);
		
	}
	
	public void buildDocument(File _file) {

		try {
			
			DocumentBuilder builder = factory.newDocumentBuilder();
			
			/* Postavlja error handler. */
			//builder.setErrorHandler(this);
			
			document = builder.parse(_file); 

			/* Detektuju eventualne greske */
			if (document != null)
				System.out.println("[INFO] File parsed with no errors.");
			else
				System.out.println("[WARN] Document is null.");

		} catch (SAXParseException e) {
			
			System.out.println("[ERROR] Parsing error, line: " + e.getLineNumber() + ", uri: " + e.getSystemId());
			System.out.println("[ERROR] " + e.getMessage() );
			System.out.print("[ERROR] Embedded exception: ");
			
			Exception embeddedException = e;
			if (e.getException() != null)
				embeddedException = e.getException();

			// Print stack trace...
			embeddedException.printStackTrace();
			
			System.exit(0);
			
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void printElement() {
		
		System.out.println("Prikaz sadržaja DOM stabla parsiranog XML dokumenta.");
	
		printNode(document);
		/*
		NodeList nodes = document.getElementsByTagName("obavestenje");
		System.out.println("<Obavestenje>");
		for (int i = 0; i < nodes.item(0).getAttributes().getLength(); i++) {
			System.out.println("\t"+nodes.item(0).getAttributes().item(i));
		}
		//System.out.println(document.getE));
		
		Element element = (Element) document;
		
		NamedNodeMap attrs = element.getAttributes();
		for (int i = 0; i < attrs.getLength(); i++) {
			Node attribute = attrs.item(i);
			printNode(attribute);
			if (i < attributes.getLength()-1)
    			System.out.print(", ");
		}
		*/
		
		
	}

	private void printNode(Node node) {
		
		if (node == null)
			return;
		
		if(node instanceof Document) {
			System.out.println("START_DOCUMENT");

			// Rekurzivni poziv za prikaz korenskog elementa
			Document doc = (Document) node;
			printNode(doc.getDocumentElement());
		}
		else if (node instanceof Element) {
			
			Element element = (Element) node;
			System.out.println("START_ELEMENT: "+ "<"+element.getTagName()+">");
			NamedNodeMap attrs = element.getAttributes();
			
			if (attrs.getLength() > 0) {
				
				System.out.print("\tATTRIBUTES: \n\t\t");
				for (int i = 0; i < attrs.getLength(); i++) {
					Node attribute = attrs.item(i);
					printNode(attribute);
					if (i < attrs.getLength()-1)
	        			System.out.print(",\n\t\t");
				}
			}
			
			System.out.println();
			// Prikaz svakog od child nodova, rekurzivnim pozivom
			NodeList children = element.getChildNodes();
						
			if (children != null) {
				for (int i = 0; i < children.getLength(); i++) {
					Node aChild = children.item(i);
					printNode(aChild);
				}
			}
		}
		// Za naredne čvorove nema rekurzivnog poziva jer ne mogu imati podelemente
		else if (node instanceof Attr) {
			Attr attr = (Attr) node;
			System.out.print(attr.getName() + "=" + attr.getValue());
		}
		else if (node instanceof Text) {
			Text text = (Text) node;
			
			if (text.getTextContent().trim().length() > 0)
				System.out.println("CHARACTERS: " + text.getTextContent().trim());
		}
		else if (node instanceof CDATASection) {
			System.out.println("CDATA: " + node.getNodeValue());
		}
		else if (node instanceof Comment) {
			System.out.println("COMMENT: " + node.getNodeValue());
		}
		else if (node instanceof ProcessingInstruction) {
			System.out.print("PROCESSING INSTRUCTION: ");

			ProcessingInstruction instruction = (ProcessingInstruction) node;
			System.out.print("data: " + instruction.getData());
			System.out.println(", target: " + instruction.getTarget());
		}
		else if (node instanceof Entity) {
			Entity entity = (Entity) node;
			System.out.println("ENTITY: " + entity.getNotationName());
		}
	}
}
