import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { AuthGuard } from 'src/app/core/guards/auth.guard';
import { AppealService } from 'src/app/core/services/appeals-service/appeal.service';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { DecisionService } from 'src/app/core/services/decisions-service/decision.service';
import { AppealDecision } from 'src/app/model/appeal-Decision';
import * as txml from 'txml';
import { DecisionShowComponent } from '../../decision-show/decision-show/decision-show.component';

@Component({
  selector: 'app-decision-list',
  templateUrl: './decision-list.component.html',
  styleUrls: ['./decision-list.component.css']
})
export class DecisionListComponent implements OnInit {

  data : MatTableDataSource<any>;

  appeals : AppealDecision;
  isAdmin: boolean;

  constructor(
    private decisionService : DecisionService,
    private authService: AuthenticationService,
    private authGuard: AuthGuard
  ) { }


  ngOnInit() {
    console.log("jesmo li ");
    const user = this.authService.getCurrentUser();
    this.isAdmin = user.isAdmin;

    if(this.isAdmin){
      console.log("tu smo");
    this.decisionService.getDecisionsForCommissioner().subscribe((appeals) =>{
      console.log(appeals);
      let obj : any = txml.parse(appeals);
      let requests : any = txml.simplify(obj);
      console.log(requests);
      this.extract(requests);
    });
    }else{
      this.decisionService.getDecisionsForCitizien().subscribe((appeals) =>{
        console.log(appeals);
        let obj : any = txml.parse(appeals);
        let requests : any = txml.simplify(obj);
        this.extract(requests);
      });
    }
  }

  extract( requests : any){
    let tableReq : any[] = [];
    let number = 1;
    console.log("exstract");
    console.log(requests.List.item[0]);
    if(!requests.List.item[0]){
      console.log("tu sam");
      console.log(requests.List);
     let request = {
      'id' : requests.List.item.brojResenja.value,
        'podnosilac' : requests.List.item.uvod.podnosilacZalbe.ime + " " + requests.List.item.uvod.podnosilacZalbe.prezime,
        'datum' : requests.List.item.datum.split('године')[0] ,
        'poverenik' : requests.List.item.poverenik.ime + " "  + requests.List.item.poverenik.prezime
       }
     tableReq.push(request);
     this.data =new MatTableDataSource(tableReq);
     console.log(this.data);
    }else{
    requests.List.item.forEach(req =>{
      let request = {
        'id' : req.brojResenja.value,
        'podnosilac' : req.uvod.podnosilacZalbe.ime + " " + req.uvod.podnosilacZalbe.prezime,
        'datum' : req.datum.split('године')[0] ,
        'poverenik' : req.poverenik.ime + " "  + req.poverenik.prezime
      }
      tableReq.push(request);
      number++;
    });
    this.data =new MatTableDataSource(tableReq);
  }
}
}
