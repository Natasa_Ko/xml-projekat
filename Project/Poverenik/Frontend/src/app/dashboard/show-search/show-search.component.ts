import { Component, Input, OnChanges, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { Title } from '@angular/platform-browser';
import { NGXLogger } from 'ngx-logger';
import { AppealService } from 'src/app/core/services/appeals-service/appeal.service';
import { DecisionService } from 'src/app/core/services/decisions-service/decision.service';
import { NotificationService } from 'src/app/core/services/notification.service';
import * as txml from 'txml';

@Component({
  selector: 'app-show-search',
  templateUrl: './show-search.component.html',
  styleUrls: ['./show-search.component.css']
})
export class ShowSearchComponent implements OnChanges {

  displayedColumns: string[] = ['id', 'podnosilac', 'poverenik', 'datum', 'preuzmi', 'izvezi'];
  
  displayedColumnsAppeal: string[] =
   ["id",
  "podnosilac",
  "organ",
  "datum",
  "mesto",
  "status",
  "preuzmi",
  "izvezi"];
  
  // displayedColumnsAppealSilence: string[] = ['id', 'podnosilac', 'poverenik', 'datum', 'PDF', 'XHTML'];
  pagNumber: number;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private logger: NGXLogger,
    private notificationService: NotificationService,
    private titleService: Title,
    private decisionService : DecisionService,
    private service: AppealService,) {
      this.pagNumber = 0;
     }

  @Input()
  public showData: MatTableDataSource<any>;
  
  @Input()
  public showDataAppealDecision: MatTableDataSource<any>;
  
  @Input()
  public showDataAppealSilence: MatTableDataSource<any>;

  ngOnChanges() {
    console.log(
"On changes"
    );
    console.log(this.showData);
    console.log("ajde");
    console.log(this.showDataAppealDecision);
    this.titleService.setTitle('Žalbe na odbijanje zahteva za informacije od javnog značaja'); 
    console.log("Ispisujemo sada sva rjesenja!");
    console.log(this.showData);
    // if(typeof(this.showData)!= "undefined"){
        // this.logger.log('Customers loaded');
        // this.showData.sort = this.sort;
        // this.showData.paginator = this.paginator;
    // }
  }

  preuzmiPDF1(element : any){
    this.decisionService.export(element.id, "PDF").subscribe((response) => {
      let file = new Blob([response], { type: "application/pdf" });
      var fileURL = URL.createObjectURL(file);
      let a = document.createElement("a");
      document.body.appendChild(a);
      a.setAttribute("style", "display: none");
      a.href = fileURL;
      a.download = `Rešena žalba broj ${element.id}.pdf`;
      a.click();
      window.URL.revokeObjectURL(fileURL);
      a.remove();
    }),
      (error) => this.notificationService.openSnackBar("Desila se greška!");
      () => this.notificationService.openSnackBar("Uspješno ste skinuli dokument u PDF formatu!");
  }

  preuzmXHTML1(element : any){
    this.decisionService.export(element.id, "HTML").subscribe((response) => {
      let file = new Blob([response], { type: 'text/html' });
      var fileURL = URL.createObjectURL(file);
      let a = document.createElement("a");
      document.body.appendChild(a);
      a.setAttribute("style", "display: none");
      a.href = fileURL;
      a.download = `Rešena žalba broj ${element.id}.html`;
      a.click();
      window.URL.revokeObjectURL(fileURL);
      a.remove();
    }),
      (error) => this.notificationService.openSnackBar("Desila se greška!");
      () => this.notificationService.openSnackBar("Uspješno ste skinuli dokument u HTML formatu!");
  }
  preuzmiPDF(element: any) {
    if (element.id.split("-")[0] === "RJ") {
      this.service.exportDecision(element.id, "PDF").subscribe((response) => {
        console.log("da vidimo");
        let file = new Blob([response], { type: "application/pdf" });
        var fileURL = URL.createObjectURL(file);
        let a = document.createElement("a");
        document.body.appendChild(a);
        a.setAttribute("style", "display: none");
        a.href = fileURL;
        a.download = `Rešena žalba broj ${element.id}.pdf`;
        a.click();
        window.URL.revokeObjectURL(fileURL);
        a.remove();
      }),
        (error) => this.notificationService.openSnackBar("Desila se greška!");
        () => this.notificationService.openSnackBar("Uspješno ste skinuli dokument u PDF formatu!");
    } else if (element.id.split("-")[0] === "RS") {
      this.service.exportSilence(element.id, "PDF").subscribe((response) => {
        let file = new Blob([response], { type: "application/pdf" });
        var fileURL = URL.createObjectURL(file);
        let a = document.createElement("a");
        document.body.appendChild(a);
        a.setAttribute("style", "display: none");
        a.href = fileURL;
        a.download = `Rešena žalba broj ${element.id}.pdf`;
        a.click();
        window.URL.revokeObjectURL(fileURL);
        a.remove();
      }),
        (error) => this.notificationService.openSnackBar("Desila se greška!");
        () => this.notificationService.openSnackBar("Uspješno ste skinuli dokument u PDF formatu!");
    }
  }
  preuzmXHTML(element: any) {
    if (element.id.split("-")[0] === "RJ") {
      this.service.exportDecision(element.id, "HTML").subscribe((response) => {
        let file = new Blob([response], { type: 'text/html' });
        var fileURL = URL.createObjectURL(file);
        let a = document.createElement("a");
        document.body.appendChild(a);
        a.setAttribute("style", "display: none");
        a.href = fileURL;
        a.download = `Rešena žalba broj ${element.id}.html`;
        a.click();
        window.URL.revokeObjectURL(fileURL);
        a.remove();
      }),
        (error) => this.notificationService.openSnackBar("Desila se greška!");
        () => this.notificationService.openSnackBar("Uspješno ste skinuli dokument u HTML formatu!");
    } else if (element.id.split("-")[0] === "RS") {
      this.service.exportSilence(element.id, "HTML").subscribe((response) => {
        let file = new Blob([response], { type: 'text/html' });
        var fileURL = URL.createObjectURL(file);
        let a = document.createElement("a");
        document.body.appendChild(a);
        a.setAttribute("style", "display: none");
        a.href = fileURL;
        a.download = `Rešena žalba broj ${element.id}.html`;
        a.click();
        window.URL.revokeObjectURL(fileURL);
        a.remove();
      }),
        (error) => this.notificationService.openSnackBar("Desila se greška!");
        () => this.notificationService.openSnackBar("Uspješno ste skinuli dokument u HTML formatu!");
    }
  }
  preuzmiJSON(element : any){
    if (element.id.split("-")[0] === "RJ") {
      this.service.exportDecision(element.id, "JSON").subscribe((response) => {
        let file = new Blob([response], { type: 'application/JSON' });
        var fileURL = URL.createObjectURL(file);
        let a = document.createElement("a");
        document.body.appendChild(a);
        a.setAttribute("style", "display: none");
        a.href = fileURL;
        a.download = `Rešena žalba broj ${element.id}.json`;
        a.click();
        window.URL.revokeObjectURL(fileURL);
        a.remove();
      }),
        (error) => this.notificationService.openSnackBar("Desila se greška!");
        () => this.notificationService.openSnackBar("Uspješno ste skinuli dokument u JSON formatu!");
    } else if (element.id.split("-")[0] === "RS") {
      this.service.exportSilence(element.id, "JSON").subscribe((response) => {
        let file = new Blob([response], { type: 'application/JSON' });
        var fileURL = URL.createObjectURL(file);
        let a = document.createElement("a");
        document.body.appendChild(a);
        a.setAttribute("style", "display: none");
        a.href = fileURL;
        a.download = `Rešena žalba broj ${element.id}.json`;
        a.click();
        window.URL.revokeObjectURL(fileURL);
        a.remove();
      }),
        (error) => this.notificationService.openSnackBar("Desila se greška!");
        () => this.notificationService.openSnackBar("Uspješno ste izvezli dokument u JSON formatu!");
    }
  }
  preuzmiRDF(element:any){
    if (element.id.split("-")[0] === "RJ") {
      this.service.exportDecision(element.id, "RDF").subscribe((response) => {
        let file = new Blob([response], { type: 'application/RDF' });
        var fileURL = URL.createObjectURL(file);
        let a = document.createElement("a");
        document.body.appendChild(a);
        a.setAttribute("style", "display: none");
        a.href = fileURL;
        a.download = `Rešena žalba broj ${element.id}.rdf`;
        a.click();
        window.URL.revokeObjectURL(fileURL);
        a.remove();
      }),
        (error) => this.notificationService.openSnackBar("Desila se greška!");
        () => this.notificationService.openSnackBar("Uspješno ste skinuli dokument u RDF formatu!");
    } else if (element.id.split("-")[0] === "RS") {
      this.service.exportSilence(element.id, "RDF").subscribe((response) => {
        let file = new Blob([response], { type: 'application/RDF' });
        var fileURL = URL.createObjectURL(file);
        let a = document.createElement("a");
        document.body.appendChild(a);
        a.setAttribute("style", "display: none");
        a.href = fileURL;
        a.download = `Rešena žalba broj ${element.id}.rdf`;
        a.click();
        window.URL.revokeObjectURL(fileURL);
        a.remove();
      }),
        (error) => this.notificationService.openSnackBar("Desila se greška!");
        () => this.notificationService.openSnackBar("Uspješno ste izvezli dokument u RDF formatu!");
    }
  }

  preuzmiRDF1(element:any){

      this.decisionService.export(element.id, "RDF").subscribe((response) => {
        let file = new Blob([response], { type: 'application/RDF' });
        var fileURL = URL.createObjectURL(file);
        let a = document.createElement("a");
        document.body.appendChild(a);
        a.setAttribute("style", "display: none");
        a.href = fileURL;
        a.download = `Rešena žalba broj ${element.id}.rdf`;
        a.click();
        window.URL.revokeObjectURL(fileURL);
        a.remove();
      }),
        (error) => this.notificationService.openSnackBar("Desila se greška!");
        () => this.notificationService.openSnackBar("Uspješno ste skinuli dokument u RDF formatu!");

  }
  preuzmiJSON1(element : any){
      this.decisionService.export(element.id, "JSON").subscribe((response) => {
        let file = new Blob([response], { type: 'application/JSON' });
        var fileURL = URL.createObjectURL(file);
        let a = document.createElement("a");
        document.body.appendChild(a);
        a.setAttribute("style", "display: none");
        a.href = fileURL;
        a.download = `Rešena žalba broj ${element.id}.json`;
        a.click();
        window.URL.revokeObjectURL(fileURL);
        a.remove();
      }),
        (error) => this.notificationService.openSnackBar("Desila se greška!");
        () => this.notificationService.openSnackBar("Uspješno ste skinuli dokument u JSON formatu!");
  
  }
  
}