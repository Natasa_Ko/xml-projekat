import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { SharedModule } from '../shared/shared.module';
import { DashboardHomeComponent } from './dashboard-home/dashboard-home.component';
import { ShowSearchComponent } from './show-search/show-search.component';
@NgModule({
  declarations: [
    DashboardHomeComponent,
    ShowSearchComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule
  ],
  entryComponents: [],
})
export class DashboardModule { }
