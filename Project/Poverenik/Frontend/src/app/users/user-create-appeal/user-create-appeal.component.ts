import { Component, Input, OnChanges, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { Title } from '@angular/platform-browser';
import { NGXLogger } from 'ngx-logger';
import { AppealService } from 'src/app/core/services/appeals-service/appeal.service';
import { NotificationService } from 'src/app/core/services/notification.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DialogInsertAppealInformationsComponent } from '../dialog-insert-appeal-informations/dialog-insert-appeal-informations.component';

export interface DialogData {
  id: string;
  infoOZahtevu : string;
  mesto : string;
  datum : Date;
  organ : string;
  sediste : string;
  isDecision : boolean;
}

@Component({
  selector: 'app-user-create-appeal',
  templateUrl: './user-create-appeal.component.html',
  styleUrls: ['./user-create-appeal.component.css']
})
export class UserCreateAppealComponent implements OnChanges {

  animal: string;
  name: string;

  displayedColumns: string[] = [
    "id",
    "infoOZahtevu",
    "organ",
    "datum",
    "mesto",
    "akcija"
  ];
  pagNumber: number;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    public dialog: MatDialog,
    private logger: NGXLogger,
    private notificationService: NotificationService,
    private service: AppealService,
    private titleService: Title
  ) {
    this.pagNumber = 0;
  }

  @Input()
  public showData: MatTableDataSource<any>;

  ngOnChanges() {
    this.titleService.setTitle(
      "Kreiranje zalbe"
    );
    if (typeof this.showData != "undefined") {
      this.logger.log("Customers loaded");
      this.showData.sort = this.sort;
      this.showData.paginator = this.paginator;
    }
  }

  createAppeal(request: any) {

    if (request.tip === "rejected") {
      console.log(request);
      console.log("tip");
      console.log(typeof(request));
      const dialogRef = this.dialog.open(DialogInsertAppealInformationsComponent, {
        width: '90%',
        data: {
          id : request.id,
          infoOZahtevu : request.infoOZahtevu,
          mesto : request.mesto,
          datum : new Date(parseInt(request.datum)),
          organ : request.organ,
          sediste : request.sediste,
          isDecision: true
        }
      });
  
      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
        this.animal = result;
      });
      console.log("zalba na odbijanje ");
      console.log(request.id);
      console.log(request.tip);
    } else if (request.tip === "silence") {
      console.log("zalba na cutanje uprave");
      console.log(request);
      console.log("tip");
      console.log(typeof(request));
      const dialogRef = this.dialog.open(DialogInsertAppealInformationsComponent, {
        width: '90%',
        data: {
          id : request.id,
          infoOZahtevu : request.infoOZahtevu,
          mesto : request.mesto,
          datum : new Date(parseInt(request.datum)),
          organ : request.organ,
          sediste : request.sediste,
          isDecision : false
        }
      });
  
      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
        this.animal = result;
      });
      console.log("zalba na odbijanje ");
      console.log(request.id);
      console.log(request.tip);
      console.log(request.id);
      console.log(request.tip);
    }
  }
 

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.showData.filter = filterValue.trim().toLowerCase();

    if (this.showData.paginator) {
      this.showData.paginator.firstPage();
    }
  }

}
