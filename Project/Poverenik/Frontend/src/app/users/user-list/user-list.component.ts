import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import * as txml from 'txml';

import { NotificationService } from '../../core/services/notification.service';
import { NGXLogger } from 'ngx-logger';
import { RequestServiceService } from 'src/app/core/services/request-service/request-service.service';
import { AuthGuard } from 'src/app/core/guards/auth.guard';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { MatDialog, MatDialogRef, MatTableDataSource } from '@angular/material';
import { DialogInsertAppealInformationsComponent } from '../dialog-insert-appeal-informations/dialog-insert-appeal-informations.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  isAdmin: boolean;
  dataDecision: MatTableDataSource<any>;
  dataSilence: MatTableDataSource<any>;


  constructor(
    public dialog: MatDialog,
    private requestService: RequestServiceService,
    private authService: AuthenticationService,
    private authGuard: AuthGuard
  ) { }

  ngOnInit() {
    const user = this.authService.getCurrentUser();

    this.isAdmin = user.isAdmin;
    if (!this.isAdmin) {
      this.requestService.getUnresolvedRequestsForUser().subscribe((response) => {
        let obj: any = txml.parse(response);
        let requests: any = txml.simplify(obj);
        this.extract(requests);
        console.log(requests);
      });
    }
  }
  extract(requests: any) {
    let requestRejected: any[] = [];
    let requestExpired: any[] = [];
    let number = 1;
    console.log(requests.List.item[0]);
    requests.List.item.forEach(req => {
      console.log(req);
      if (req.about.split('/ZA')[1].split('_')[0] === "N") {
        let request = {
          'id': req.about.split('request/')[1],
          'infoOZahtevu': req.informacijeNaKojeSeOdnosiZahtev,
          'mesto': req.mesto,
          'datum': req.datum,
          'organ': req.podaciOOrganu.nazivOrgana.value,
          'sediste': req.podaciOOrganu.sedisteOrgana.value,
          'tip': "rejected"
        }
        requestRejected.push(request);
        number++;
      } else if (req.about.split('/Z')[1].split('_')[0] === "A") {
        let request = {
          'id': req.about.split('request/')[1],
          'infoOZahtevu': req.informacijeNaKojeSeOdnosiZahtev,
          'mesto': req.mesto,
          'datum': req.datum,
          'organ': req.podaciOOrganu.nazivOrgana.value,
          'sediste': req.podaciOOrganu.sedisteOrgana.value,
          'tip': "silence"
        }
        let requestDate = new Date(parseInt(request.datum));
        let now = new Date();
        var diffMs = (now.getTime() - requestDate.getTime()); // milliseconds between now & Christmas
        var diffDays = Math.floor(diffMs / 86400000); // days
        var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
        var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
        console.log(diffDays + " days, " + diffHrs + " hours, " + diffMins + " minutes");
        if (diffDays > 30 || diffHrs > 24 
          // || diffMins > 5
          ) {
          requestExpired.push(request);
        }
        number++
      }
    });
    this.dataSilence = new MatTableDataSource(requestExpired);
    this.dataDecision = new MatTableDataSource(requestRejected);
  }

  createDecision() {
    console.log("kreiraj resenje");
    const dialogRef = this.dialog.open(DialogInsertAppealInformationsComponent, {
      width: '90%',
      data: {
        // id : request.id,
        // infoOZahtevu : request.infoOZahtevu,
        // mesto : request.mesto,
        // datum : new Date(parseInt(request.datum)),
        // organ : request.organ,
        // sediste : request.sediste,
        // isDecision: true
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.animal = result;
    });
  }
}
