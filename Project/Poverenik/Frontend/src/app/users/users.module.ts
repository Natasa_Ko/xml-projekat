import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UserListComponent } from './user-list/user-list.component';
import { SharedModule } from '../shared/shared.module';
import { UserCreateAppealComponent } from './user-create-appeal/user-create-appeal.component';
import { MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS, MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material';
import { DialogInsertAppealInformationsComponent } from './dialog-insert-appeal-informations/dialog-insert-appeal-informations.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MatRadioModule } from '@angular/material/radio';

@NgModule({
  imports: [  
    MatRadioModule,
    MatDialogModule,
    CommonModule,
    SharedModule,
    UsersRoutingModule,
    
  ],
  declarations: [
    DialogInsertAppealInformationsComponent,
    UserListComponent,
    UserCreateAppealComponent
  ],
  entryComponents :[
    DialogInsertAppealInformationsComponent
  ],
  providers: [
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}}
  ]
})
export class UsersModule { }
