import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppealsRoutingModule } from './appeals-routing.module';
import { SharedModule } from '../shared/shared.module';
import { AppealsRejection } from './appeals-rejection-list/appeals-rejection-list.component';
import { AppealShowComponent } from './appeal-show/appeal-show/appeal-show.component';
import { AppealsSilenceComponent } from './appeals-silence/appeals-silence-list/appeals-silence/appeals-silence.component';

@NgModule({
  imports: [
    CommonModule,
    AppealsRoutingModule,
    SharedModule
  ],
  declarations: [
    AppealsRejection,
    AppealShowComponent,
    AppealsSilenceComponent,
  ],
  entryComponents: [
  ]
})
export class AppealsModule { }
