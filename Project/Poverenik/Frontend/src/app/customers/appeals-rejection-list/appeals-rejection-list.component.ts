import { Component, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import * as txml from 'txml';

import { AppealService } from 'src/app/core/services/appeals-service/appeal.service';
import { AppealDecision } from 'src/app/model/appeal-Decision';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { AuthGuard } from 'src/app/core/guards/auth.guard';

@Component({
  selector: 'app-appeals-rejection-list',
  templateUrl: './appeals-rejection-list.component.html',
  styleUrls: ['./appeals-rejection-list.component.css']
})

export class AppealsRejection implements OnInit{

  data : MatTableDataSource<any>;
  dataUnresolved  : MatTableDataSource<any>;
  dataCitizien  : MatTableDataSource<any>;
  appeals : AppealDecision;
  isAdmin: boolean;

  constructor(
    private appealsService : AppealService,
    private authService: AuthenticationService,
    private authGuard: AuthGuard
  ) { }


  ngOnInit() {
    const user = this.authService.getCurrentUser();

    this.isAdmin = user.isAdmin;
    if(this.isAdmin){
        this.appealsService.getAppealsDecision().subscribe((appeals) =>{
          let obj : any = txml.parse(appeals);
          let requests : any = txml.simplify(obj);
          this.extractResolved(requests);
        });
      setTimeout(() => {

        this.appealsService.getAppealdDecisionUnresolved().subscribe((appeals) => {
          let obj: any = txml.parse(appeals);
          let requests: any = txml.simplify(obj);
          this.extractUnresolved(requests);
        });
      },
        100);
    console.log("uresolved");
    console.log("now");



    }else{
      this.appealsService.getAppealdDecisionCitizien().subscribe((appeals) =>{
        let obj : any = txml.parse(appeals);
        let requests : any = txml.simplify(obj);
        this.extractCitizien(requests);
        console.log("gradjanin");
        console.log(requests);
      });
    }
  }
  extractCitizien( requests : any){
    let tableReq : any[] = [];
    let number = 1;
    if(!requests.List.item[0]){
      console.log("tu sam");
      console.log(requests.List);
      let request = {
        'id' : requests.List.item.about.split('appealDecision/')[1],
        'podnosilac' : requests.List.item.informacijePodnosiocuZalbe.imeIPrezimePodnosioca.value,
        'mesto': requests.List.item.podaciOZalbi.mesto,
        'datum' :new Date(requests.List.item.odlukaKojaSePobija.datumResenja.godina,requests.List.item.odlukaKojaSePobija.datumResenja.mesec-1,requests.List.item.odlukaKojaSePobija.datumResenja.dan) ,
        'organ' : requests.List.item.odlukaKojaSePobija.nazivOrgana.value,
        'status' : (requests.List.item.about.split('R')[1].split('-')[0] === "JP") ? "Rešeno" : "Nerešeno"
      }
     tableReq.push(request);
     this.dataCitizien =new MatTableDataSource(tableReq);
     console.log(this.data);
    }else{
    requests.List.item.forEach(req =>{
      let request = {
        'id' : req.about.split('appealDecision/')[1],
        'podnosilac' : req.informacijePodnosiocuZalbe.imeIPrezimePodnosioca.value,
        'mesto': req.podaciOZalbi.mesto,
        'datum' :new Date(req.odlukaKojaSePobija.datumResenja.godina,req.odlukaKojaSePobija.datumResenja.mesec-1,req.odlukaKojaSePobija.datumResenja.dan) ,
        'organ' : req.odlukaKojaSePobija.nazivOrgana.value,
        'status' : (req.about.split('R')[1].split('-')[0] === "JP") ? "Rešeno" : "Nerešeno"
      }
      tableReq.push(request);
      number++;
    });
    this.dataCitizien =new MatTableDataSource(tableReq);
  }
}

  extractResolved( requests : any){
    let tableReq : any[] = [];
    let number = 1;
    if(!requests.List.item[0]){
      console.log("tu sam");
      console.log(requests.List);
      let request = {
        'id' : requests.List.item.about.split('appealDecision/')[1],
        'podnosilac' : requests.List.item.informacijePodnosiocuZalbe.imeIPrezimePodnosioca.value,
        'mesto': requests.List.item.podaciOZalbi.mesto,
        'datum' :new Date(requests.List.item.odlukaKojaSePobija.datumResenja.godina,requests.List.item.odlukaKojaSePobija.datumResenja.mesec,requests.List.item.odlukaKojaSePobija.datumResenja.dan) ,
        'organ' : requests.List.item.odlukaKojaSePobija.nazivOrgana.value,
        'status' : "rešeno"
      }
     tableReq.push(request);
     this.data =new MatTableDataSource(tableReq);
     console.log(this.data);
    }else{
    requests.List.item.forEach(req =>{
      let request = {
        'id' : 'RJ' + req.about.split('/RJ')[1],
        'podnosilac' : req.informacijePodnosiocuZalbe.imeIPrezimePodnosioca.value,
        'mesto': req.podaciOZalbi.mesto,
        'datum' :new Date(req.odlukaKojaSePobija.datumResenja.godina,req.odlukaKojaSePobija.datumResenja.mesec,req.odlukaKojaSePobija.datumResenja.dan) ,
        'organ' : req.odlukaKojaSePobija.nazivOrgana.value,
        'status' : "rešeno"
      }
      tableReq.push(request);
      number++;
    });
    this.data =new MatTableDataSource(tableReq);
  }
}

  extractUnresolved( requests : any){
    let tableReq : any[] = [];
    let number = 1;
    if(!requests.List.item[0]){
      console.log("tu sam");
      console.log(requests.List);
      let request = {
        'id' : requests.List.item.about.split('appealDecision/')[1],
        'podnosilac' : requests.List.item.informacijePodnosiocuZalbe.imeIPrezimePodnosioca.value,
        'mesto': requests.List.item.podaciOZalbi.mesto,
        'datum' :new Date(requests.List.item.odlukaKojaSePobija.datumResenja.godina,requests.List.item.odlukaKojaSePobija.datumResenja.mesec,requests.List.item.odlukaKojaSePobija.datumResenja.dan) ,
        'organ' : requests.List.item.odlukaKojaSePobija.nazivOrgana.value,
        'status' : "nerešeno"
      }
     tableReq.push(request);
     this.dataUnresolved =new MatTableDataSource(tableReq);
     console.log(this.data);
    }else{
    requests.List.item.forEach(req =>{
      let request = {
        'id' :  req.about.split('appealDecision/')[1],
        'podnosilac' : req.informacijePodnosiocuZalbe.imeIPrezimePodnosioca.value,
        'mesto': req.podaciOZalbi.mesto,
        'datum' :new Date(req.odlukaKojaSePobija.datumResenja.godina,req.odlukaKojaSePobija.datumResenja.mesec,req.odlukaKojaSePobija.datumResenja.dan) ,
        'organ' : req.odlukaKojaSePobija.nazivOrgana.value,
        'status' : "nerešeno"
      }
      tableReq.push(request);
      number++;
    });
    this.dataUnresolved =new MatTableDataSource(tableReq);
  }
}
}
