export class AppealDecision {
    id : number;
    podnosilac: string;
    organ: string;
    datum: Date;
    mesto : string;
}
