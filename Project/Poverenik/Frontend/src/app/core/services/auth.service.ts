import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import * as jwt_decode from 'jwt-decode';
import * as moment from 'moment';
import 'rxjs/add/operator/delay';
import * as JsonToXML from 'js2xmlparser';

import { environment } from '../../../environments/environment';
import { of, EMPTY } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {
  parser = new DOMParser();

    register(email: string, password: string, first_name : string, last_name : string) {
        const headers = new HttpHeaders({
            'Content-Type': 'application/xml',
            'Accept': 'application/xml'
          });
          let user = {
              first_name : first_name,
              last_name : last_name,
              email : email,
              password : password
          }
          const options = {
            declaration: {
              include: false
            }
          };
        let data : any = JsonToXML.parse("DTOUserSignUp", user, options);
       
        return  this.http.post("http://localhost:8081/auth/sign-up", data, {headers: headers, responseType: 'text'});
    }

    constructor(private http: HttpClient,
        @Inject('LOCALSTORAGE') private localStorage: Storage) {
    }

    login(email: string, password: string) {
        const headers = new HttpHeaders({
            'Content-Type': 'application/xml',
            'Accept': 'application/xml'
          });
          let user = {
            username : email,
            password : password
          }
          const options = {
            declaration: {
              include: false
            }
          };
          console.log("user" + user);
        //   data = "let data: any = JsonToXML.parse("korisnikSignUpDTO", signUpDto, options);"
        let data : any = JsonToXML.parse("DTOUserLogin", user, options);
       
        return  this.http.post("http://localhost:8081/auth/log-in", data, {headers: headers, responseType: 'text'})
            .pipe(map((response) => {
                // set token property
           
                let xmlDoc = this.parser.parseFromString(response,"text/xml");
         
                let tokenInfo = this.getDecodedAccessToken(xmlDoc.getElementsByTagName("accessToken")[0].childNodes[0].nodeValue); // decode token
               
                // store email and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('currentUser', JSON.stringify({
                    token: xmlDoc.getElementsByTagName("accessToken")[0].childNodes[0].nodeValue,
                    isAdmin: tokenInfo['isAdmin'],
                    email: tokenInfo['sub'],
                    alias:tokenInfo['sub'].split('@')[0],
                    iat:new Date(tokenInfo['iat']*1001),
                    expiration:new Date(tokenInfo['exp']*1000),
                    fullName: tokenInfo['fullName']
                }));
                console.log("idemo dalje");
                return true;
            }));
    }

  getDecodedAccessToken(token: string): any {
    try{
        return jwt_decode(token);
    }
    catch(Error){
        return null;
    }
  }

    logout(): void {
        // clear token remove user from local storage to log user out
        this.localStorage.removeItem('currentUser');
    }

    getCurrentUser(): any {
        console.log("1");
        // TODO: Enable after implementation
        console.log(JSON.parse(this.localStorage.getItem('currentUser')));
        return JSON.parse(this.localStorage.getItem('currentUser'));
        // console.log("getCurrentUser()");
        // return {
        //     token: 'aisdnaksjdn,axmnczm',
        //     isAdmin: true,
        //     email: 'john.doe@gmail.com',
        //     id: '12312323232',
        //     alias: 'john.doe@gmail.com'.split('@')[0],
        //     expiration: moment().add(1, 'days').toDate(),
        //     fullName: 'John Doe'
        // };
    }

    passwordResetRequest(email: string) {
        return of(true).delay(1000);
    }

    changePassword(email: string, currentPwd: string, newPwd: string) {
        return of(true).delay(1000);
    }

    passwordReset(email: string, token: string, password: string, confirmPassword: string): any {
        return of(true).delay(1000);
    }
}
