import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RequestServiceService {

constructor(private http: HttpClient,
    @Inject('LOCALSTORAGE') private localStorage: Storage) {
  
  }

getUnresolvedRequestsForUser():Observable<any>{
  const headers = new HttpHeaders({
    'Content-Type': 'application/xml',
    'Accept': '*/*',
    'Authorization' : 'Bearer ' + localStorage.getItem("id_token")
  });
  
  return  this.http.get("http://localhost:8081/requests", {headers: headers, responseType: 'text'});
}
}
