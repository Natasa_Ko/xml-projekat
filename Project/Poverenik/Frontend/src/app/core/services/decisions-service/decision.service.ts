import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DecisionService {

  constructor(private http: HttpClient,
    @Inject('LOCALSTORAGE') private localStorage: Storage) {
  
  }

  
getDecisionsForCommissioner():Observable<any>{
  const headers = new HttpHeaders({
    'Content-Type': 'application/xml',
    'Accept': 'application/xml',
    'Authorization' : 'Bearer ' + localStorage.getItem("id_token")
  });
  
  return  this.http.get("http://localhost:8081/decisions/all", {headers: headers, responseType: 'text'});
}

getDecisionsForCitizien():Observable<any>{
  const headers = new HttpHeaders({
    'Content-Type': 'application/xml',
    'Accept': 'application/xml',
    'Authorization' : 'Bearer ' + localStorage.getItem("id_token")
  });
  
  return  this.http.get("http://localhost:8081/decisions", {headers: headers, responseType: 'text'});
}  
searchDecision(value : string):Observable<any>{
  const headers = new HttpHeaders({
    'Content-Type': 'application/xml',
    'Accept': 'application/xml',
    'Authorization' : 'Bearer ' + localStorage.getItem("id_token")
  });
  
  return  this.http.get("http://localhost:8081/decisions/search/" + value, {headers: headers, responseType: 'text'});
}  

export(id: string, type :string){
  console.log("export");
  const headers = new HttpHeaders({
    'Authorization' : 'Bearer ' + localStorage.getItem("id_token")
  });
  
  return  this.http.get("http://localhost:8081/decisions/export/" + id +"/" + type, {headers: headers, responseType: 'arraybuffer' as 'text' });
}
createAppealOnDecision(data : string){
  const headers = new HttpHeaders({
    'Content-Type': 'application/xml',
    'Accept': 'application/xml',
    'Authorization' : 'Bearer ' + localStorage.getItem("accessToken")
  });
  return this.http.put("http://localhost:8081/appealDecisions", data, {headers: headers, responseType: 'text'});
}
createAppealOnSilence(data : string){
  const headers = new HttpHeaders({
    'Content-Type': 'application/xml',
    'Accept': 'application/xml',
    'Authorization' : 'Bearer ' + localStorage.getItem("accessToken")
  });
  return this.http.put("http://localhost:8081/appealSilence", data, {headers: headers, responseType: 'text'});
};

createDecision(data : string){
  console.log("tu smooo");
  const headers = new HttpHeaders({
    'Content-Type': 'application/xml',
    'Accept': 'application/xml',
    'Authorization' : 'Bearer ' + localStorage.getItem("accessToken")
  });
  return this.http.put("http://localhost:8081/decisions", data, {headers: headers, responseType: 'text'});
};
}
