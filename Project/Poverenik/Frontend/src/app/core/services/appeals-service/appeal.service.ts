import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject } from '@angular/core';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppealService {

constructor(private http: HttpClient,
  @Inject('LOCALSTORAGE') private localStorage: Storage) {

}

getAppealsDecision():Observable<any>{
  const headers = new HttpHeaders({
    'Content-Type': 'application/xml',
    'Accept': 'application/xml',
    'Authorization' : 'Bearer ' + localStorage.getItem("id_token")
  });
  
  return  this.http.get("http://localhost:8081/appealDecisions", {headers: headers, responseType: 'text'});
}

getAppealsSilence():Observable<any>{
  const headers = new HttpHeaders({
    'Content-Type': 'application/xml',
    'Accept': 'application/xml',
    'Authorization' : 'Bearer ' + localStorage.getItem("id_token")
  });
  
  return  this.http.get("http://localhost:8081/appealSilence", {headers: headers, responseType: 'text'});
}

getAppealdDecisionUnresolved(){
  const headers = new HttpHeaders({
    'Content-Type': 'application/xml',
    'Accept': 'application/xml',
    'Authorization' : 'Bearer ' + localStorage.getItem("id_token")
  });
  
  return  this.http.get("http://localhost:8081/appealDecisions/unresolved", {headers: headers, responseType: 'text'});
}
//vraca sve od gradjanina, i odobrene i neodobrene
getAppealdDecisionCitizien(){
  const headers = new HttpHeaders({
    'Content-Type': 'application/xml',
    'Accept': 'application/xml',
    'Authorization' : 'Bearer ' + localStorage.getItem("id_token")
  });
  
  return  this.http.get("http://localhost:8081/appealDecisions/own", {headers: headers, responseType: 'text'});
}

getAppealdSilenceUnresolved(){
  const headers = new HttpHeaders({
    'Content-Type': 'application/xml',
    'Accept': 'application/xml',
    'Authorization' : 'Bearer ' + localStorage.getItem("id_token")
  });
  
  return  this.http.get("http://localhost:8081/appealSilence/unresolved", {headers: headers, responseType: 'text'});
}

//vraca sve od gradjanina
getAppealdSilenceCitizien(){
  console.log("da vidimo");
  const headers = new HttpHeaders({
    'Content-Type': 'application/xml',
    'Accept': 'application/xml',
    'Authorization' : 'Bearer ' + localStorage.getItem("id_token")
  });
  
  return  this.http.get("http://localhost:8081/appealSilence/own", {headers: headers, responseType: 'text'});
}

searchAppealDecision(value : string){
  console.log("da vidimo");
  const headers = new HttpHeaders({
    'Content-Type': 'application/xml',
    'Accept': 'application/xml',
    'Authorization' : 'Bearer ' + localStorage.getItem("id_token")
  });
  
  return  this.http.get("http://localhost:8081/appealDecisions/search/" + value, {headers: headers, responseType: 'text'});
}

searchAppealSilence(value:string){
  console.log("da vidimo");
  const headers = new HttpHeaders({
    'Content-Type': 'application/xml',
    'Accept': 'application/xml',
    'Authorization' : 'Bearer ' + localStorage.getItem("id_token")
  });
  
  return  this.http.get("http://localhost:8081/appealSilence/search/" + value, {headers: headers, responseType: 'text'});
}

exportSilence(id: string, type :string){
  console.log("da vidimo");
  const headers = new HttpHeaders({
    'Authorization' : 'Bearer ' + localStorage.getItem("id_token")
  });
  
  return  this.http.get("http://localhost:8081/appealSilence/export/" + id +"/" + type, {headers: headers, responseType: 'arraybuffer' as 'text' });
}
exportDecision(id: string, type :string){
  console.log("exportDecision");
  const headers = new HttpHeaders({
    'Authorization' : 'Bearer ' + localStorage.getItem("id_token")
  });
  
  return  this.http.get("http://localhost:8081/appealDecisions/export/" + id +"/" + type, {headers: headers, responseType: 'arraybuffer' as 'text' });
}
}
