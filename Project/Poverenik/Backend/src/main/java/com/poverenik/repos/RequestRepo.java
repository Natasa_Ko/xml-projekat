package com.poverenik.repos;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.exist.xupdate.XUpdateProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.XMLResource;

import org.xmldb.api.base.ResourceIterator;
import com.poverenik.database.ExistManager;
import com.poverenik.model.appealDecision.Zalba;
import com.poverenik.model.requests.Zahtev;
import com.poverenik.model.requests.Zahtevi;

import com.poverenik.model.user.User;
import com.poverenik.util.Constants;

@Repository
public class RequestRepo {
	private final String TARGET_NAMESPACE = "http://www.euprava.poverenik.gov.rs/request";
	private final String APPEND = "<xu:modifications version=\"1.0\" xmlns:xu=\"" + XUpdateProcessor.XUPDATE_NS
			+ "\" xmlns=\"" + TARGET_NAMESPACE + "\">" + "<xu:append select=\"%1$s\" child=\"last()\">%2$s</xu:append>"
			+ "</xu:modifications>";

	private final String UPDATE = "<xu:modifications version=\"1.0\" xmlns:xu=\"" + XUpdateProcessor.XUPDATE_NS
			+ "\" xmlns=\"" + TARGET_NAMESPACE + "\">" + "<xu:update select=\"%1$s\">%2$s</xu:update>"
			+ "</xu:modifications>";
	
	@Autowired
	private ExistManager existManager;
	
	private String query;
	private JAXBContext context;
	private Unmarshaller unmarshaller;
	List<Zahtev> requests;
	
	
	public List<Zahtev> findAllByUserEmail(String email) throws XMLDBException, JAXBException{
		
		query = String.format("/zahtevi/zahtev[@podnosilac='%s']", email);
	
					
		
		ResourceSet set = this.existManager.retrieve(Constants.COLLECTION_URI, query, TARGET_NAMESPACE);
		
		if(set.getSize() != 0) {
			
			requests = new ArrayList<Zahtev>();
			context = JAXBContext.newInstance("com.poverenik.model.requests");
			unmarshaller = context.createUnmarshaller();
			Zahtev z = null;
			for (int i = 0; i < set.getSize(); i++) {
				z = (Zahtev) unmarshaller.unmarshal(((XMLResource)set.getResource(i)).getContentAsDOM());
				requests.add(z);
			}
			 
			return requests;	
		}
		else {
			return null;
		}
		
	}


	public void create(Zahtev zahtevObj, String zahtev) throws Exception {
		existManager.add(Constants.COLLECTION_URI,
						 Constants.ZAHTEV_ID,
						 "/zahtevi",
						 zahtev,
						 APPEND);
		
	}


	public List<Zahtev> getAll() throws XMLDBException, JAXBException {
		query = String.format("/zahtevi/*");
		ResourceSet set = this.existManager.retrieve(Constants.COLLECTION_URI, query, TARGET_NAMESPACE);
		if(set.getSize() != 0) {
			requests = new ArrayList<Zahtev>();
			context = JAXBContext.newInstance("com.poverenik.model.requests");
			unmarshaller = context.createUnmarshaller();
			Zahtev z = null;
			for (int i = 0; i < set.getSize(); i++) {
				z = (Zahtev) unmarshaller.unmarshal(((XMLResource)set.getResource(i)).getContentAsDOM());
				
				requests.add(z);
			}
			 
			return requests;	
		}
		else {
			return null;
		}
	
	}

}
