package com.poverenik.repos;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.exist.xupdate.XUpdateProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.xmldb.api.base.Resource;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.XMLResource;

import com.poverenik.database.ExistManager;
import com.poverenik.model.user.User;
import com.poverenik.util.Constants;


@Repository
public class UserRepo {
	
	private final String TARGET_NAMESPACE = "http://www.euprava.poverenik.gov.rs/user";
	private final String APPEND = "<xu:modifications version=\"1.0\" xmlns:xu=\"" + XUpdateProcessor.XUPDATE_NS
			+ "\" xmlns=\"" + TARGET_NAMESPACE + "\">" + "<xu:append select=\"%1$s\" child=\"last()\">%2$s</xu:append>"
			+ "</xu:modifications>";

	private final String UPDATE = "<xu:modifications version=\"1.0\" xmlns:xu=\"" + XUpdateProcessor.XUPDATE_NS
			+ "\" xmlns=\"" + TARGET_NAMESPACE + "\">" + "<xu:update select=\"%1$s\">%2$s</xu:update>"
			+ "</xu:modifications>";
	@Autowired
	private ExistManager existManager;
	
	private String query;
	private JAXBContext context;
	private Unmarshaller unmarshaller;

	

	public void create(String user) throws Exception {
		
		this.existManager.add(Constants.COLLECTION_URI, Constants.KORISNICI_ID, "/users", user, APPEND);
	}
	
	public User findByEmail(String email) throws JAXBException, XMLDBException {
		query = String.format("/users/user[email='%s']", email);
		System.out.println(query);
		ResourceSet set = this.existManager.retrieve(Constants.COLLECTION_URI, query, TARGET_NAMESPACE);
		if(set.getSize() != 0) {
			context = JAXBContext.newInstance("com.poverenik.model.user");
			unmarshaller = context.createUnmarshaller();
			User u = (User) unmarshaller.unmarshal(((XMLResource)set.getResource(0)).getContentAsDOM()); 
			return u;	
		}
		else {
			return null;
		}
			
	}
	
}
