package com.poverenik.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;
import org.xmldb.api.base.XMLDBException;

import com.poverenik.database.FusekiManager;
import com.poverenik.model.appealDecision.OdlukaKojaSePobija;
import com.poverenik.model.appealDecision.Zalba;
import com.poverenik.model.decision.Resenje;
import com.poverenik.model.requests.Zahtev;
import com.poverenik.repos.AppealDecisionRepo;
import com.poverenik.repos.DecisionRepo;
import com.poverenik.util.AppealDecisionTransform;
import com.poverenik.util.AppealSilenceTransform;
import com.poverenik.util.Constants;
import com.poverenik.util.MetadataExtractor;
import com.poverenik.util.Person;

@Service
public class AppealDecisionService {
	
	private final String TARGET_NAMESPACE = "http://www.euprava.poverenik.gov.rs/";
	private JAXBContext context;
	private Unmarshaller unmarshaller;
	private SchemaFactory schemaFactory;
	private Schema schema;

	@Autowired
	FusekiManager fusekiManager;
	
	private String newId;
	
	@Autowired
	private AppealDecisionRepo appealDecisionRepo;
	
	public List<Zalba> getAll() throws XMLDBException, JAXBException{
		return appealDecisionRepo.getAll();
	}

	public void create(String zalba) throws FactoryConfigurationError, Exception {
		getNextId();
		zalba = zalba.replace("xml:space='preserve'", "");
		context = JAXBContext.newInstance("com.poverenik.model.appealDecision");
		unmarshaller = context.createUnmarshaller();
		schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		schema = schemaFactory.newSchema(new File(Constants.ZALBAODLUKA_XSD_LOCATION));
		unmarshaller.setSchema(schema);
		
		XMLStreamReader reader = XMLInputFactory.newInstance().createXMLStreamReader(new StringReader(zalba));
		JAXBElement<Zalba> zalbaObj =  unmarshaller.unmarshal(reader, Zalba.class);
		
		Zalba z = zalbaObj.getValue();
		z.setAbout(TARGET_NAMESPACE+"rdf/examples/appealDecision/"+newId);
		z.setVocab("http://www.euprava.poverenik.gov.rs/predicate/");
		z.getOdlukaKojaSePobija().getBrojResenja().setValue(newId);
		z.setPodnosilac(((Person) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getEmail());
		
		Marshaller marshaller = context.createMarshaller();
		StringWriter stringWriter = new StringWriter();
		marshaller.marshal(z, stringWriter);
		
		String tst = stringWriter.toString().substring(55); //smacinje <?xml.... sa pocetka , damo ze da se upise

		appealDecisionRepo.create(z, tst);
		
	}
	

	private void getNextId() throws XMLDBException, JAXBException {
		List<Zalba> zalbe = getAll();
		
		Collections.sort(zalbe, new Comparator<Zalba>() {
			@Override
			public int compare(Zalba o1, Zalba o2) {
				
			return o1.getOdlukaKojaSePobija().getBrojResenja().getValue().split("-")[1]
					.compareTo(o2.getOdlukaKojaSePobija().getBrojResenja().getValue().split("-")[1]);
				
			}
			});
	
		newId = "RJ-"+ (Integer.parseInt(zalbe.get(zalbe.size()-1).getOdlukaKojaSePobija()
				.getBrojResenja().getValue().split("-")[1])+1)+"-555";
	}

	public List<Zalba> getAllUnresolved() throws XMLDBException, JAXBException {
		List<Zalba> zalbe = getAll();
		zalbe.removeIf(zalba -> zalba.getOdlukaKojaSePobija().getBrojResenja().getValue().contains("P"));
		return zalbe;
	}

	public List<Zalba> getAllByUserEmail(String email) throws JAXBException, XMLDBException {
		// TODO Auto-generated method stub
		return appealDecisionRepo.findAllByUserEmail(email);
	}
	
	public String findById(String id) throws XMLDBException {
		return appealDecisionRepo.findById(id);
	}
	
	
	public File exportAsPDF(String id) throws XMLDBException, SAXException, IOException, TransformerException {
		String zalba = findById(id);
		AppealDecisionTransform ast = new AppealDecisionTransform();
		String file = String.format("src/main/resources/static/documents/zalbaNaOdluku-%s.pdf", id);
		ast.makePDF(this.removeNamespace(zalba), file);
		
		return new File(file);
	}
	
	public File exportAsHTML(String id) throws XMLDBException, ParserConfigurationException, SAXException, IOException, TransformerException {
		String zalba = findById(id);
		
		AppealDecisionTransform ast = new AppealDecisionTransform();
		String file = String.format("src/main/resources/static/documents/zalbaNaOdluku-%s.html", id);

		ast.makeHTML(this.removeNamespace(zalba), file);
		
		return new File(file);
	}
	
	public File exportMetadataAsJSON(String id) throws IOException {
		
		String file = String.format("src/main/resources/static/documents/zalbaNaOdluku-%s.JSON", id);
		fusekiManager.makeJSONForAppealDecision(id, file);
		
		return new File(file);
	}
	
	public File exportMetadataAsRDF(String id) throws SAXException, IOException, XMLDBException {
		String file = String.format("src/main/resources/static/documents/zalbaNaOdluku-%s.RDF", id);
		String fileXml = String.format("src/main/resources/static/documents/zalbaNaOdluku-%s.xml", id);
		MetadataExtractor metadataExtractor = new MetadataExtractor();
		
		String zalba = removeNamespace(findById(id));
		
		String output = zalba.substring(0, 44) + 
				" xmlns:pred=\"http://www.euprava.poverenik.gov.rs/rdf/examples/predicate/\" "+
				zalba.substring(45);
				
		FileWriter fw = new FileWriter(fileXml);
		fw.write(output);
		fw.close();
		
		try {
			metadataExtractor.extractMetadata(new FileInputStream(new File(fileXml)),
					new FileOutputStream(new File(file)));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return new File(file);
	}
	
	public String removeNamespace(String xml) {
		 try{
	       TransformerFactory factory = TransformerFactory.newInstance();
	        Source xslt = new StreamSource(new File(Constants.NS_REMOVER));
	        Transformer transformer = factory.newTransformer(xslt);
	        StringReader r = new StringReader(xml);
	        StreamSource text = new StreamSource(r);
	        
	        StringWriter writer = new StringWriter();
	        
	        
	        transformer.transform(text, new StreamResult(writer));
	        System.out.println("Done");
	        return writer.toString();
	        } catch (TransformerConfigurationException e) {
	           return "";
	        } catch (TransformerException e) {
	        	return "";
	        }
	
  }

	public List<Zalba> findAllByContains(String text) throws XMLDBException, JAXBException {
		return appealDecisionRepo.findAllByContains(text);
	}

}
