package com.poverenik.util;

public class Constants {

	public static final String COLLECTION_URI= "/db/documentsPoverenik";
	public static final String PREDICATE_NAMESPACE = "http://localhost:8080/rdf/examples/predicate/";										   
	
	public static final String NS_REMOVER = "src/main/resources/static/documents/removeNs.xslt"; 
	
	//korisnici
	
	public static final String KORISNICI_ID = "users.xml";
	public static final String KORISNICI_LOCATION = "src/main/resources/static/documents/user.xml";
	
	//obavestenje
	public static final String OBAVESTENJE_ID = "obavestenje.xml";
	public static final String OBAVESTENJE_LOCATION = "documents/obavestenje.xml";
	public static final String OBAVESTENJE_RDF_LOCATION = "documents/obavestenje.rdf"; //tu ce napraviti rdf
	public static final String OBAVESTENJE_SPARQL_NAMED_GRAPH_URI = "/obavestenja";
	
	//zalba na odluku
	public static final String ZALBAODLUKA_ID = "appealsDecision.xml";
	public static final String ZALBAODLUKA_LOCATION = "src/main/resources/static/documents/appealDecision.xml";
	public static final String ZALBAODLUKA_XSD_LOCATION = "src/main/resources/static/documents/appealDecision.xsd";
	public static final String ZALBAODLUKA_RDF_LOCATION = "src/main/resources/static/documents/appealDecision.rdf"; //tu ce napraviti rdf
	public static final String ZALBAODLUKA_SPARQL_NAMED_GRAPH_URI = "/appealDecision";
	public static final String ZALBAODLUKA_XSL_LOCATION = "src/main/resources/static/documents/appealDecisionFop.xsl";
	public static final String ZALBAODLUKA_XHTML_LOCATION= "src/main/resources/static/documents/appealDecision.xsl";
	
	
	//zahtjev za informacije od javnog znacaja
	public static final String ZAHTEV_ID = "requests.xml";
	public static final String ZAHTEV_LOCATION = "src/main/resources/static/documents/request.xml";
	public static final String ZAHTEV_XSD_LOCATION = "src/main/resources/static/documents/request.xsd";
	public static final String ZAHTEV_RDF_LOCATION = "documentsPoverenik/request.rdf"; //tu ce napraviti rdf
	public static final String ZAHTEV_SPARQL_NAMED_GRAPH_URI = "/request";
	

	//zalba na cutanje
	public static final String ZALBACUTANJE_ID = "appealsSilence.xml";
	public static final String ZALBACUTANJE_LOCATION = "src/main/resources/static/documents/appealSilence.xml";
	public static final String ZALBACUTANJE_XSD_LOCATION = "src/main/resources/static/documents/appealSilence.xsd";
	public static final String ZALBACUTANJE_RDF_LOCATION = "src/main/resources/static/documents/appealSilence.rdf"; //tu ce napraviti rdf
	public static final String ZALBACUTANJE_SPARQL_NAMED_GRAPH_URI = "/appealSilence";
	public static final String ZALBACUTANJE_XSL_LOCATION = "src/main/resources/static/documents/appealSilenceFop.xsl";
	public static final String ZALBACUTANJE_XHTML_LOCATION= "src/main/resources/static/documents/appealSilence.xsl";
															
	
	//resenje
	public static final String RESENJE_ID = "decisions.xml";
	public static final String RESENJE_LOCATION = "src/main/resources/static/documents/decision.xml";
	public static final String RESENJE_XSD_LOCATION = "src/main/resources/static/documents/decision.xsd";
	public static final String RESENJE_RDF_LOCATION = "src/main/resources/static/documents/resenje.rdf"; //tu ce napraviti rdf
	public static final String RESENJE_SPARQL_NAMED_GRAPH_URI = "/decision";
	public static final String RESENJE_XSL_LOCATION = "src/main/resources/static/documents/decisionFop.xsl";
	public static final String RESENJE_XHTML_LOCATION= "src/main/resources/static/documents/decision.xsl";
	
	
	
}
