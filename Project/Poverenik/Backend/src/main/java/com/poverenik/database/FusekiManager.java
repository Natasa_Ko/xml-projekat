package com.poverenik.database;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.update.UpdateExecutionFactory;
import org.apache.jena.update.UpdateFactory;
import org.apache.jena.update.UpdateProcessor;
import org.apache.jena.update.UpdateRequest;
import org.springframework.stereotype.Component;

import com.poverenik.model.decision.Resenje;
import com.poverenik.util.AuthenticationUtilRDF;
import com.poverenik.util.AuthenticationUtilRDF.ConnectionProperties;
import com.poverenik.util.Constants;
import com.poverenik.util.SparqlUtill;

@Component
public class FusekiManager {
	private final String PREDICATE_NAMESPACE = "http://www.euprava.poverenik.gov.rs/rdf/examples/predicate/";
	private ConnectionProperties conn;
	
	public void addAppealSilence(com.poverenik.model.appealSilence.Zalba zalba) throws IOException {
		conn = AuthenticationUtilRDF.loadProperties();
		
		Model model = ModelFactory.createDefaultModel();
		model.setNsPrefix("pred", PREDICATE_NAMESPACE);
	
		// Making the changes manually 
		Resource resource = model.createResource(zalba.getAbout());
		
		Property property1 = model.createProperty(PREDICATE_NAMESPACE, "naziv_organa");
		Literal literal1 = model.createLiteral(zalba.getSadrzaj().getNazivOrgana().getValue());
		
		Property property2 = model.createProperty(PREDICATE_NAMESPACE, "razlog_zalbe");
		Literal literal2 = model.createLiteral(zalba.getSadrzaj().getRazlogZalbe().getOpcija().get(0).getValue());
		
		Property property3 = model.createProperty(PREDICATE_NAMESPACE, "datum_podnosenja_zahteva");
		Literal literal3 = model.createLiteral(zalba.getSadrzaj().getDatumPodnosenjaZahteva().getDan()+"/"+
											   zalba.getSadrzaj().getDatumPodnosenjaZahteva().getMesec()+"/"+
											   zalba.getSadrzaj().getDatumPodnosenjaZahteva().getGodina());
		
		Property property4 = model.createProperty(PREDICATE_NAMESPACE, "ime_i_prezime");
		Literal literal4 = model.createLiteral(zalba.getPodnosilacZalbe().getImeIPrezime().getValue());
		
		//Property property5 = model.createProperty(PREDICATE_NAMESPACE, "adresa");
		//Literal literal5 = model.createLiteral("Novi Sad Fruskogorska 12");
		
		

		// Adding the statements to the model
		Statement statement1 = model.createStatement(resource, property1, literal1);
		Statement statement2 = model.createStatement(resource, property2, literal2);
		Statement statement3 = model.createStatement(resource, property3, literal3);
		Statement statement4 = model.createStatement(resource, property4, literal4);
		//Statement statement5 = model.createStatement(resource, property5, literal5);
		
	
		model.add(statement1);
		model.add(statement2);
		model.add(statement3);
		model.add(statement4);
		//model.add(statement5);
	
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		model.write(out, SparqlUtill.NTRIPLES);
		
		String sparqlUpdate = SparqlUtill.insertData(conn.dataEndpoint + Constants.ZALBACUTANJE_SPARQL_NAMED_GRAPH_URI, new String(out.toByteArray()));
		
		UpdateRequest update = UpdateFactory.create(sparqlUpdate);
	
	    UpdateProcessor processor = UpdateExecutionFactory.createRemote(update, conn.updateEndpoint);
		processor.execute();
	
		model.close();
		
		System.out.println("[INFO] Insertion done");

	}
	
	public void addAppealDecision(com.poverenik.model.appealDecision.Zalba zalba) throws IOException {
		conn = AuthenticationUtilRDF.loadProperties();
		
		Model model = ModelFactory.createDefaultModel();
		model.setNsPrefix("pred", PREDICATE_NAMESPACE);
	
		// Making the changes manually 
		Resource resource = model.createResource(zalba.getAbout());
		
		Property property1 = model.createProperty(PREDICATE_NAMESPACE, "ime_zalioca");
		Literal literal1 = model.createLiteral(zalba.getInformacijeOZaliocu().getFizickoLice().getImeZalioca().getValue());
		
		Property property2 = model.createProperty(PREDICATE_NAMESPACE, "prezime_zalioca");
		Literal literal2 = model.createLiteral(zalba.getInformacijeOZaliocu().getFizickoLice().getPrezimeZalioca().getValue());
		
		Property property3 = model.createProperty(PREDICATE_NAMESPACE, "adresa_zalioca");
		Literal literal3 = model.createLiteral(zalba.getInformacijeOZaliocu().getFizickoLice().getAdresaZalioca().getGrad()+" "+
											   zalba.getInformacijeOZaliocu().getFizickoLice().getAdresaZalioca().getUlica()+" "+
											   zalba.getInformacijeOZaliocu().getFizickoLice().getAdresaZalioca().getBroj());
		
		Property property4 = model.createProperty(PREDICATE_NAMESPACE, "naziv_organa");
		Literal literal4 = model.createLiteral(zalba.getOdlukaKojaSePobija().getNazivOrgana().getValue());
		
		Property property5 = model.createProperty(PREDICATE_NAMESPACE, "broj_resenja");
		Literal literal5 = model.createLiteral(zalba.getOdlukaKojaSePobija().getBrojResenja().getValue());
		
		Property property6 = model.createProperty(PREDICATE_NAMESPACE, "datum_resenja");
		Literal literal6 = model.createLiteral(zalba.getOdlukaKojaSePobija().getDatumResenja().getDan()+"/"+
											   zalba.getOdlukaKojaSePobija().getDatumResenja().getMesec()+"/"+
											   zalba.getOdlukaKojaSePobija().getDatumResenja().getGodina());
		
		Property property7 = model.createProperty(PREDICATE_NAMESPACE, "sporni_dio");
		Literal literal7 = model.createLiteral(zalba.getRazlogZalbe().getSporniDio().getValue());
		
		Property property8 = model.createProperty(PREDICATE_NAMESPACE, "datum_zalbe");
		Literal literal8 = model.createLiteral(zalba.getPodaciOZalbi().getDatumZalbe().getDan()+"/"+
											   zalba.getPodaciOZalbi().getDatumZalbe().getMesec()+"/"+
											   zalba.getPodaciOZalbi().getDatumZalbe().getGodina());
		
		Property property9 = model.createProperty(PREDICATE_NAMESPACE, "ime_i_prezime_podnosioca");
		Literal literal9 = model.createLiteral(zalba.getInformacijePodnosiocuZalbe().getImeIPrezimePodnosioca().getValue());
		

		// Adding the statements to the model
		Statement statement1 = model.createStatement(resource, property1, literal1);
		Statement statement2 = model.createStatement(resource, property2, literal2);
		Statement statement3 = model.createStatement(resource, property3, literal3);
		Statement statement4 = model.createStatement(resource, property4, literal4);
		Statement statement5 = model.createStatement(resource, property5, literal5);
		Statement statement6 = model.createStatement(resource, property6, literal6);
		Statement statement7 = model.createStatement(resource, property7, literal7);
		Statement statement8 = model.createStatement(resource, property8, literal8);
		Statement statement9 = model.createStatement(resource, property9, literal9);
		
		//Statement statement5 = model.createStatement(resource, property5, literal5)		
	
		model.add(statement1);
		model.add(statement2);
		model.add(statement3);
		model.add(statement4);
		model.add(statement5);
		model.add(statement6);
		model.add(statement7);
		model.add(statement8);
		model.add(statement9);
	
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		model.write(out, SparqlUtill.NTRIPLES);
		
		String sparqlUpdate = SparqlUtill.insertData(conn.dataEndpoint + Constants.ZALBAODLUKA_SPARQL_NAMED_GRAPH_URI, new String(out.toByteArray()));
		
		UpdateRequest update = UpdateFactory.create(sparqlUpdate);
	
	    UpdateProcessor processor = UpdateExecutionFactory.createRemote(update, conn.updateEndpoint);
		processor.execute();
	
		model.close();
		
		System.out.println("[INFO] Insertion done");

	}

	public void makeJSONForAppealSilence(String id, String _file) throws IOException {
		conn = AuthenticationUtilRDF.loadProperties();
		String sparqlQuery = SparqlUtill.selectData(conn.dataEndpoint + 
				Constants.ZALBACUTANJE_SPARQL_NAMED_GRAPH_URI,
				String.format("<http://www.euprava.poverenik.gov.rs/rdf/examples/appealSilence/%s> ?p ?o", id));
		QueryExecution query = QueryExecutionFactory.sparqlService(conn.queryEndpoint, sparqlQuery);
		ResultSet results = query.execSelect();
		File rdfFile = new File(_file);
		OutputStream out = new BufferedOutputStream(new FileOutputStream(rdfFile));
		ResultSetFormatter.outputAsJSON(out, results);
		query.close();
		
	}

	public void makeJSONForAppealDecision(String id, String _file) throws IOException {
		conn = AuthenticationUtilRDF.loadProperties();
		String sparqlQuery = SparqlUtill.selectData(conn.dataEndpoint + 
				Constants.ZALBAODLUKA_SPARQL_NAMED_GRAPH_URI,
				String.format("<http://www.euprava.poverenik.gov.rs/rdf/examples/appealDecision/%s> ?p ?o", id));
		QueryExecution query = QueryExecutionFactory.sparqlService(conn.queryEndpoint, sparqlQuery);
		ResultSet results = query.execSelect();
		File rdfFile = new File(_file);
		OutputStream out = new BufferedOutputStream(new FileOutputStream(rdfFile));
		ResultSetFormatter.outputAsJSON(out, results);
		query.close();
		
	}

	public void addDecision(Resenje resenje) throws IOException {
		conn = AuthenticationUtilRDF.loadProperties();
		
		Model model = ModelFactory.createDefaultModel();
		model.setNsPrefix("pred", PREDICATE_NAMESPACE);
	
		// Making the changes manually 
		Resource resource = model.createResource(resenje.getAbout());
		
		Property property1 = model.createProperty(PREDICATE_NAMESPACE, "broj_resenja");
		Literal literal1 = model.createLiteral(resenje.getBrojResenja().getValue());
		
		Property property2 = model.createProperty(PREDICATE_NAMESPACE, "podnosilac_zalbe");
		Literal literal2 = model.createLiteral(resenje.getUvod().getPodnosilacZalbe().getIme() + " "+ resenje.getUvod().getPodnosilacZalbe().getPrezime());
		
		Property property3 = model.createProperty(PREDICATE_NAMESPACE, "poverenik");
		Literal literal3 = model.createLiteral(resenje.getPoverenik().getIme() + " " + resenje.getPoverenik().getPrezime());


		// Adding the statements to the model
		Statement statement1 = model.createStatement(resource, property1, literal1);
		Statement statement2 = model.createStatement(resource, property2, literal2);
		Statement statement3 = model.createStatement(resource, property3, literal3);
		
		//Statement statement5 = model.createStatement(resource, property5, literal5)		
	
		model.add(statement1);
		model.add(statement2);
		model.add(statement3);

	
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		model.write(out, SparqlUtill.NTRIPLES);
		
		String sparqlUpdate = SparqlUtill.insertData(conn.dataEndpoint + Constants.RESENJE_SPARQL_NAMED_GRAPH_URI, new String(out.toByteArray()));
		
		UpdateRequest update = UpdateFactory.create(sparqlUpdate);
	
	    UpdateProcessor processor = UpdateExecutionFactory.createRemote(update, conn.updateEndpoint);
		processor.execute();
	
		model.close();
		
		System.out.println("[INFO] Insertion done");
		
	}

	public void makeJSONForDecision(String id, String _file) throws IOException {
		conn = AuthenticationUtilRDF.loadProperties();
		String sparqlQuery = SparqlUtill.selectData(conn.dataEndpoint + 
		Constants.RESENJE_SPARQL_NAMED_GRAPH_URI,
		String.format("<http://www.euprava.poverenik.gov.rs/rdf/examples/decision/%s> ?p ?o", id));
		QueryExecution query = QueryExecutionFactory.sparqlService(conn.queryEndpoint, sparqlQuery);
		ResultSet results = query.execSelect();
		File rdfFile = new File(_file);
		OutputStream out = new BufferedOutputStream(new FileOutputStream(rdfFile));
		ResultSetFormatter.outputAsJSON(out, results);
		query.close();
		
	}

}
