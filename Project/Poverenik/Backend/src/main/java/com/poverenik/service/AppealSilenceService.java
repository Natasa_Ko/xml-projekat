package com.poverenik.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xmldb.api.base.XMLDBException;
import org.yaml.snakeyaml.reader.StreamReader;


import com.poverenik.database.FusekiManager;
import com.poverenik.model.appealSilence.Zalba;

import com.poverenik.repos.AppealSilenceRepo;
import com.poverenik.util.AppealSilenceTransform;
import com.poverenik.util.Constants;
import com.poverenik.util.MetadataExtractor;
import com.poverenik.util.Person;
import com.ximpleware.AutoPilot;
import com.ximpleware.VTDGen;
import com.ximpleware.VTDNav;
import com.ximpleware.XMLModifier;

@Service
public class AppealSilenceService {
	
	private final String TARGET_NAMESPACE = "http://www.euprava.poverenik.gov.rs/";
	private JAXBContext context;
	private Unmarshaller unmarshaller;
	private SchemaFactory schemaFactory;
	private Schema schema;

	private String newId;
	
	@Autowired
	private AppealSilenceRepo appealSilenceRepo;
	
	@Autowired
	FusekiManager fusekiManager;
	
	public List<Zalba> getAll() throws XMLDBException, JAXBException{
		return appealSilenceRepo.getAll();
	}

	public void create(String zalba) throws FactoryConfigurationError, Exception {
		getNextId();
		zalba = zalba.replace("xml:space='preserve'", "");
		context = JAXBContext.newInstance("com.poverenik.model.appealSilence");
		unmarshaller = context.createUnmarshaller();
		schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		schema = schemaFactory.newSchema(new File(Constants.ZALBACUTANJE_XSD_LOCATION));
		unmarshaller.setSchema(schema);
		
		XMLStreamReader reader = XMLInputFactory.newInstance().createXMLStreamReader(new StringReader(zalba));
		System.out.println(zalba);
		
		
		
		JAXBElement<Zalba> zalbaObj =  unmarshaller.unmarshal(reader, Zalba.class);
		
		Zalba z = zalbaObj.getValue();
		z.setAbout(TARGET_NAMESPACE+"rdf/examples/appealSilence/"+newId);
		z.setVocab("http://www.euprava.poverenik.gov.rs/predicate/");
		z.setId(newId);
		z.setPodnosilac(((Person) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getEmail());
		

		Marshaller marshaller = context.createMarshaller();
		StringWriter stringWriter = new StringWriter();
		marshaller.marshal(z, stringWriter);
		
		String tst = stringWriter.toString().substring(55); //smacinje <?xml.... sa pocetka , damo ze da se upise

		appealSilenceRepo.create(z, tst);
		
		
	}
	
	private void getNextId() throws XMLDBException, JAXBException {
		List<Zalba> zalbe = getAll();
		
		Collections.sort(zalbe, new Comparator<Zalba>() {

			@Override
			public int compare(Zalba o1, Zalba o2) {
				return o1.getId().split("-")[1].compareTo(o2.getId().split("-")[1]);
			}
		});
	
		
		
		newId = "RS-"+ (Integer.parseInt(zalbe.get(zalbe.size()-1).getId().split("-")[1])+1)+"-555";
	}

	public List<Zalba> getAllUnresolved() throws XMLDBException, JAXBException {
		List<Zalba> zalbe = getAll();
		
		zalbe.removeIf(zalba -> zalba.getId().contains("P"));
		return zalbe;
	}

	public List<Zalba> getAllByUserEmail(String email) throws XMLDBException, JAXBException {
		// TODO Auto-generated method stub
		return appealSilenceRepo.findAllByUserEmail(email);
	}
	
	public String findById(String id) throws XMLDBException {
		return appealSilenceRepo.findById(id);
	}

	public File exportAsPDF(String id) throws XMLDBException, SAXException, IOException, TransformerException {
		String zalba = findById(id);
		
		AppealSilenceTransform ast = new AppealSilenceTransform();
		String file = String.format("src/main/resources/static/documents/zalbaNaCutanje-%s.pdf", id);
		ast.makePDF(this.removeNamespace(zalba), file);
		
		return new File(file);
	}
	
	public File exportAsHTML(String id) throws XMLDBException, ParserConfigurationException, SAXException, IOException, TransformerException {
		String zalba = findById(id);
		
		AppealSilenceTransform ast = new AppealSilenceTransform();
		String file = String.format("src/main/resources/static/documents/zalbaNaCutanje-%s.html", id);
		ast.makeHTML(this.removeNamespace(zalba), file);
		
		return new File(file);
	}
	
	public File exportMetadataAsJSON(String id) throws IOException {
		
		String file = String.format("src/main/resources/static/documents/zalbaNaCutanje-%s.JSON", id);
		fusekiManager.makeJSONForAppealSilence(id, file);
		
		return new File(file);
	}
	
	public File exportMetadataAsRDF(String id) throws SAXException, IOException, XMLDBException {
		String file = String.format("src/main/resources/static/documents/zalbaNaCutanje-%s.RDF", id);
		String fileXml = String.format("src/main/resources/static/documents/zalbaNaCutanje-%s.xml", id);
		MetadataExtractor metadataExtractor = new MetadataExtractor();
		
		String zalba = removeNamespace(findById(id));
		
		String output = zalba.substring(0, 44) + 
				" xmlns:pred=\"http://www.euprava.poverenik.gov.rs/rdf/examples/predicate/\" "+
				zalba.substring(45);
				
		FileWriter fw = new FileWriter(fileXml);
		fw.write(output);
		fw.close();
		
		try {
			metadataExtractor.extractMetadata(new FileInputStream(new File(fileXml)),
					new FileOutputStream(new File(file)));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return new File(file);
	}
	
	
	public String removeNamespace(String xml) {
		 try{
	       TransformerFactory factory = TransformerFactory.newInstance();
	        Source xslt = new StreamSource(new File(Constants.NS_REMOVER));
	        Transformer transformer = factory.newTransformer(xslt);
	        StringReader r = new StringReader(xml);
	        StreamSource text = new StreamSource(r);
	        
	        StringWriter writer = new StringWriter();
	        
	        
	        transformer.transform(text, new StreamResult(writer));
	        System.out.println("Done");
	        return writer.toString();
	        } catch (TransformerConfigurationException e) {
	           return "";
	        } catch (TransformerException e) {
	        	return "";
	        }
	
   }

	public List<Zalba> findAllByContains(String text) throws XMLDBException, JAXBException {
		return appealSilenceRepo.findAllByContains(text);
	}



	

	

}
