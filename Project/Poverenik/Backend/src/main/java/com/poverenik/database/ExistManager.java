package com.poverenik.database;


import javax.xml.transform.OutputKeys;

import org.exist.xmldb.DatabaseImpl;
import org.exist.xmldb.EXistResource;
import org.exist.xupdate.XUpdateProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Database;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.CollectionManagementService;
import org.xmldb.api.modules.XMLResource;
import org.xmldb.api.modules.XPathQueryService;
import org.xmldb.api.modules.XUpdateQueryService;



@Component
public class ExistManager {
	
	private final  String TARGET_NAMESPACE = "http://www.euprava.poverenik.gov.rs/user";
	
	private final String UPDATE = "<xu:modifications version=\"1.0\" xmlns:xu=\"" + XUpdateProcessor.XUPDATE_NS
			+ "\" xmlns=\"" + TARGET_NAMESPACE + "\">" + "<xu:update select=\"%1$s\">%2$s</xu:update>"
			+ "</xu:modifications>";
	
	@Autowired
	private AuthenticationManager authenticationMenager;
	
	public void createConnection() throws XMLDBException {
		Database db = new DatabaseImpl();
		
		db.setProperty("create-database", "true");
		
		DatabaseManager.registerDatabase(db);
		
	}
	
	public void closeConnection(Collection col, XMLResource res) throws XMLDBException {
		if(col != null) {
			col.close();
		}
		if(res != null) {
			((EXistResource)res).freeResources();
		}
	}
	
	public void update(String collection_Id, String document_Id, String root, String entity) throws Exception {
		createConnection();
		Collection col = null;
		try {
			col = DatabaseManager.getCollection(this.authenticationMenager.getUri() + collection_Id, this.authenticationMenager.getUser(),
					this.authenticationMenager.getPassword());
			XUpdateQueryService service = (XUpdateQueryService) col.getService("XUpdateQueryService", "1.0");
			service.setProperty("indent", "yes");
			service.updateResource(document_Id, String.format(UPDATE, root, entity));
			
		} finally {
			if (col != null)
				col.close();
		}
	}

	public void add(String collection_Id, String document_Id, String root, String entity, String _APPEND) throws Exception {
		createConnection();
		Collection col = null;
		try {
			col = DatabaseManager.getCollection(this.authenticationMenager.getUri() + collection_Id, this.authenticationMenager.getUser(),
					this.authenticationMenager.getPassword());
			XUpdateQueryService service = (XUpdateQueryService) col.getService("XUpdateQueryService", "1.0");
			service.setProperty("indent", "yes");
			System.out.println(document_Id + ", "+ String.format(_APPEND, root, entity));
			service.updateResource(document_Id, String.format(_APPEND, root, entity));
		} finally {
			if (col != null)
				col.close();
		}
	}
	
	public Collection getOrCreateCollection(String collectionUri, int pathOffset) throws XMLDBException {
		Collection col = DatabaseManager.getCollection(collectionUri, authenticationMenager.getUser(), authenticationMenager.getPassword());
		
		if(col==null) {
			if(collectionUri.startsWith("/")) {
				collectionUri.substring(1);
			}
			
			String pathSegments[] =collectionUri.split("/");
			if(pathSegments.length>0) {
				StringBuilder path = new StringBuilder();
				for (int i = 0; i <= pathOffset; i++) {
					path.append(pathSegments[i]);
					
				}
				Collection startCollection = DatabaseManager.getCollection(authenticationMenager.getUri() + path, authenticationMenager.getUser(), authenticationMenager.getPassword());
				if(startCollection == null) {
					String parentPath = path.substring(0, path.lastIndexOf("/"));
					Collection parentCollection = DatabaseManager.getCollection(authenticationMenager.getUri() + parentPath, authenticationMenager.getUser(), authenticationMenager.getPassword());
					CollectionManagementService service = (CollectionManagementService) parentCollection.getService("CollectionManagementService", "1.0");
					col = service.createCollection(pathSegments[pathOffset]);
					col.close();
					parentCollection.close();
				}else {
					 startCollection.close();
				}
			}
			return getOrCreateCollection(collectionUri, ++pathOffset);
		}
		else {
			return col;
		}
		
	}
	
	public void storeFromText(String collectionId, String documentId, String xmlString) throws XMLDBException {
		createConnection();
		Collection col = null;
		XMLResource res = null;
		try {
			col = getOrCreateCollection(collectionId, 0);
			res = (XMLResource) col.createResource(documentId, XMLResource.RESOURCE_TYPE);
			res.setContent(xmlString);
			col.storeResource(res);
		}
		finally {
			closeConnection(col, res);
		}
	}
	
	public XMLResource load(String collectionUri, String documentId) throws XMLDBException {
		createConnection();
		Collection col = null;
		XMLResource res = null;
		try {
			col = DatabaseManager.getCollection(authenticationMenager.getUri() + collectionUri, authenticationMenager.getUser(), authenticationMenager.getPassword());
			col.setProperty(OutputKeys.INDENT, "yes");
			res = (XMLResource)col.getResource(documentId);
			return res;
		}
		finally {
			
			if(col != null) {
				col.close();
			}
		
		}
		
	}
	
	public ResourceSet retrieve(String collectionUri, String xpathExp, String TARGET_NAMESPACE) throws XMLDBException {
		createConnection();
		Collection col = null;
		ResourceSet result = null;
		System.out.println(authenticationMenager.getUri() + collectionUri+"------"+authenticationMenager.getUser()+"------"+authenticationMenager.getPassword());
		try {
			col = DatabaseManager.getCollection(authenticationMenager.getUri() + collectionUri, authenticationMenager.getUser(), authenticationMenager.getPassword());
			XPathQueryService xpathService = (XPathQueryService) col.getService("XPathQueryService", "1.0");
			xpathService.setProperty("indent", "yes");
			xpathService.setNamespace("", TARGET_NAMESPACE);
			result = xpathService.query(xpathExp);
		}
		finally {
			
			if(col != null) {
				col.close();
			}
		
		}
		
		return result;
	}
	
	
}
