package com.poverenik.controler;

import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.xml.sax.SAXException;
import org.xmldb.api.base.XMLDBException;

import com.poverenik.model.decision.Resenje;
import com.poverenik.service.DecisionService;
import com.poverenik.util.Person;


@RestController


@RequestMapping(value = "/decisions",
//consumes = MediaType.APPLICATION_XML_VALUE,
produces = MediaType.APPLICATION_XML_VALUE)
public class DecisionController {
	
	@Autowired
	private DecisionService decisionService;
	
	@PreAuthorize("hasRole('ROLE_CITIZEN')")
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Resenje>> getAllForUser() throws XMLDBException, JAXBException {
		
		
		List<Resenje> requests = decisionService.findAllByUserEmail(((Person) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getEmail());
		
		if(requests!=null) {
			return new ResponseEntity<List<Resenje>>(requests, HttpStatus.OK);
		}else {
			System.out.println("nema ");
			return new ResponseEntity<List<Resenje>>(requests, HttpStatus.NOT_FOUND);
		}

	}
	
	@PreAuthorize("hasRole('ROLE_COMMISSIONER')")
	@RequestMapping(method = RequestMethod.GET, value = "/all")
	public ResponseEntity<List<Resenje>> getAll() throws XMLDBException, JAXBException {
		
		
		List<Resenje> requests = decisionService.getAll();
		
		if(requests!=null) {
			return new ResponseEntity<List<Resenje>>(requests, HttpStatus.OK);
		}else {
			System.out.println("nema ");
			return new ResponseEntity<List<Resenje>>(requests, HttpStatus.NOT_FOUND);
		}
	

	}
	//@PreAuthorize("hasRole('ROLE_COMMISSIONER')")
	@RequestMapping(method = RequestMethod.GET, value = "/search/{text}")
	public ResponseEntity<List<Resenje>> search(@PathVariable("text") String text) throws XMLDBException, JAXBException {
		
		List<Resenje> decisions = decisionService.findAllByContains(text);
				
		if(decisions!=null) {
			
			return new ResponseEntity<List<Resenje>>(decisions, HttpStatus.OK);
		}else {
			
			return new ResponseEntity<List<Resenje>>(decisions, HttpStatus.NOT_FOUND);
		}
	}
	

	@CrossOrigin(origins = "http://localhost:4200/users")
	@PreAuthorize("hasRole('ROLE_COMMISSIONER')")
	@RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<?> create(@RequestBody String resenje) {
		System.out.println(resenje);
		
		try {
			this.decisionService.create(resenje);
			return new ResponseEntity<>(null, HttpStatus.CREATED);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	
	}
	
	@RequestMapping(path = "/export/{id}/{type}", method = RequestMethod.GET, produces = MediaType.APPLICATION_PDF_VALUE)
	public ResponseEntity<byte[]> exportAs(@PathVariable("id") String id, @PathVariable("type") String type) throws IOException, XMLDBException, SAXException, TransformerException, ParserConfigurationException{
		
		switch (type) {
		case "PDF":
			return new ResponseEntity<byte[]>(Files.readAllBytes(decisionService.exportAsPDF(id).toPath()), HttpStatus.OK);
		case "HTML":
			return new ResponseEntity<byte[]>(Files.readAllBytes(decisionService.exportAsHTML(id).toPath()), HttpStatus.OK);
		case "JSON":
			return new ResponseEntity<byte[]>(Files.readAllBytes(decisionService.exportMetadataAsJSON(id).toPath()), HttpStatus.OK);
		case "RDF":
			return new ResponseEntity<byte[]>(Files.readAllBytes(decisionService.exportMetadataAsRDF(id).toPath()), HttpStatus.OK);
		default:
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
		
	}

}
