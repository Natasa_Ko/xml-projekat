package com.poverenik.repos;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.exist.xupdate.XUpdateProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.xmldb.api.base.ResourceIterator;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.XMLResource;

import com.poverenik.database.ExistManager;
import com.poverenik.database.FusekiManager;
import com.poverenik.model.appealDecision.Zalba;
import com.poverenik.model.decision.Resenja;
import com.poverenik.model.decision.Resenje;
import com.poverenik.model.requests.Zahtev;
import com.poverenik.util.Constants;

@Repository
public class DecisionRepo {
	
	@Autowired
	private ExistManager existManager;
	@Autowired
	private FusekiManager fusekiManager;
	
	private String query;
	private JAXBContext context;
	private Unmarshaller unmarshaller;
	List<Resenje> requests;
	
	private static final String TARGET_NAMESPACE = "http://www.euprava.poverenik.gov.rs/decision";
	
	private final String APPEND = "<xu:modifications version=\"1.0\" xmlns:xu=\"" + XUpdateProcessor.XUPDATE_NS
			+ "\" xmlns=\"" + TARGET_NAMESPACE + "\">" + "<xu:append select=\"%1$s\" child=\"last()\">%2$s</xu:append>"
			+ "</xu:modifications>";

	private final String UPDATE = "<xu:modifications version=\"1.0\" xmlns:xu=\"" + XUpdateProcessor.XUPDATE_NS
			+ "\" xmlns=\"" + TARGET_NAMESPACE + "\">" + "<xu:update select=\"%1$s\">%2$s</xu:update>"
			+ "</xu:modifications>";

	public List<Resenje> findAllByUserEmail(String email) throws XMLDBException, JAXBException{
		
		query = String.format("/resenja/resenje[@podnosilac='%s']", email);
	
									
		
		ResourceSet set = this.existManager.retrieve(Constants.COLLECTION_URI, query, TARGET_NAMESPACE);
		if(set.getSize() != 0) {
			
			requests = new ArrayList<Resenje>();
			context = JAXBContext.newInstance("com.poverenik.model.decision");
			unmarshaller = context.createUnmarshaller();
			Resenje z = null;
			for (int i = 0; i < set.getSize(); i++) {
				z = (Resenje) unmarshaller.unmarshal(((XMLResource)set.getResource(i)).getContentAsDOM());
				requests.add(z);
			}
			 
			return requests;	
		}
		else {
			return null;
		}
		
	}

	public List<Resenje> getAll() throws XMLDBException, JAXBException {
		query = String.format("/resenja/*");
		ResourceSet set = this.existManager.retrieve(Constants.COLLECTION_URI, query, TARGET_NAMESPACE);
	
		if(set.getSize() != 0) {
			
			requests = new ArrayList<Resenje>();
			context = JAXBContext.newInstance("com.poverenik.model.decision");
			unmarshaller = context.createUnmarshaller();
			Resenje z = null;
			for (int i = 0; i < set.getSize(); i++) {
				z = (Resenje) unmarshaller.unmarshal(((XMLResource)set.getResource(i)).getContentAsDOM());
				
				requests.add(z);
			}
			 
			return requests;	
		}
		else {
			return null;
		}
	}
	
	public List<Resenje> findAllByContains(String text) throws XMLDBException, JAXBException{
		query = String.format("/resenja/resenje[uvod/podnosilac_zalbe/ime[contains(., '%1$s')] or "
				                             + "uvod/podnosilac_zalbe/prezime[contains(., '%1$s')] or "
				                             +  "uvod/podnosilac_zalbe/prezime[contains(., '%1$s')] or "
				                             + "uvod/razlog_zalbe[contains(., '%1$s')] or "
				                             + "tekst_resenja/tekst[contains(., '%1$s')] or "
				                             + "tekst_obrazlozenja[contains(., '%1$s')] or "
				                             + "poverenik/ime[contains(., '%1$s')] or "
				                             + "poverenik/prezime[contains(., '%1$s')]]", text);
		
		
		
				
		ResourceSet set = this.existManager.retrieve(Constants.COLLECTION_URI, query, TARGET_NAMESPACE);
	
		if(set.getSize() != 0) {
			
			requests = new ArrayList<Resenje>();
			context = JAXBContext.newInstance("com.poverenik.model.decision");
			unmarshaller = context.createUnmarshaller();
			Resenje z = null;
			for (int i = 0; i < set.getSize(); i++) {
				z = (Resenje) unmarshaller.unmarshal(((XMLResource)set.getResource(i)).getContentAsDOM());
				
				requests.add(z);
			}
			
			return requests;	
		}
		else {
			return null;
		}
			
	}

	public String findById(String id) throws XMLDBException {
		String fullId= "http://www.euprava.poverenik.gov.rs/rdf/examples/decision/"+id;
		query = String.format("/resenja/resenje[@about='%s']", fullId);
		
		ResourceSet set = this.existManager.retrieve(Constants.COLLECTION_URI, query, TARGET_NAMESPACE);
		
		if(set.getSize() !=0) {
			
			return set.getResource(0).getContent().toString();
		}else {
			return null;
		}
	}

	public void create(Resenje resenjeObj, String resenje) throws Exception {
		existManager.add(Constants.COLLECTION_URI,
				 Constants.RESENJE_ID,
				 "/resenja",
				 resenje,
				 APPEND);
	
		fusekiManager.addDecision(resenjeObj);
		
	}
}
