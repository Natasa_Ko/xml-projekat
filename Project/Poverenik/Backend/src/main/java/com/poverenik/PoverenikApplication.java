package com.poverenik;

import java.io.IOException;

import javax.xml.transform.TransformerException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.xml.sax.SAXException;
import org.xmldb.api.base.XMLDBException;

import com.poverenik.util.Initializer;

@SpringBootApplication
public class PoverenikApplication {

	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException, XMLDBException, TransformerException, SAXException {
		Initializer init = new Initializer();
		
		init.initExist();
		
		SpringApplication.run(PoverenikApplication.class, args);
	}

}
