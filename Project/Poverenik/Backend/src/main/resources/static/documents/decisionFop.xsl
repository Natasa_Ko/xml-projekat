<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" version="2.0">
    <xsl:template match="/">
        <fo:root>
            <fo:layout-master-set>
                <fo:simple-page-master
                    master-name="resenje-page">
                    <fo:region-body margin-top="0.75in"
                        margin-bottom="0.75in" margin-left="80pt" margin-right="80pt" />
                </fo:simple-page-master>
            </fo:layout-master-set>
            
            <fo:page-sequence master-reference="resenje-page">
                <fo:flow flow-name="xsl-region-body">
            	<xsl:text>Решење - </xsl:text>
            	<fo:block font-family="Times New Roman" width="60%"
						margin-top="10pt" text-align="left" border="none">
						<xsl:value-of
							select="/resenje/@tip"></xsl:value-of>
				</fo:block>
				<fo:block font-family="Times New Roman" width="60%"
						text-align-last="justify" border="none">
						Број: 
						<fo:inline  font-family="Times New Roman" font-size="10pt"
                            font-weight="bold" text-align="center">
                            <xsl:value-of
							select="/resenje/broj_resenja"></xsl:value-of>
                        </fo:inline>
                        <fo:leader leader-pattern="space" />
                        <fo:inline font-family="Times New Roman" width="60%" text-align="right" border="none">
						Датум: 
						<fo:inline  font-family="Times New Roman" font-size="10pt"
                            font-weight="bold" text-align="center">
                            <xsl:value-of
							select="/resenje/@datum"></xsl:value-of>
                        </fo:inline>
                         године.
				</fo:inline>
				</fo:block>
				<fo:block font-family="Times New Roman" width="60%"
						margin-top="10pt" text-align="left" border="none">Повереник за информације од јавног значаја и заштиту података о личности, у поступку по жалби коју је изјавио 
				<xsl:value-of select="/resenje/uvod/podnosilac_zalbe/ime"></xsl:value-of><xsl:text> </xsl:text><xsl:value-of select="/resenje/uvod/podnosilac_zalbe/prezime"></xsl:value-of> <xsl:value-of select="/resenje/uvod/podnosilac_zalbe/razlog_zalbe"></xsl:value-of> по његовом захтеву од <xsl:value-of select="/resenje/uvod/podnosilac_zalbe/datum_zahteva"></xsl:value-of> за приступ информацијама од јавног значаја, на основу члана 35. став 1. тачка 5. Закона о слободном приступу информацијама од јавног значаја („Сл. гласник РС“, бр. 120/04, 54/07, 104/09 и 36/10), а у вези са чланом 4. тачка 22. Закона о заштити података о личности („Сл. гласник РС“, број 87/18), као и члана 23. и члана 24. став 4. Закона о слободном приступу информацијама од јавног значаја и члана 173. став 2. Закона о општем управном поступку („Сл. гласник РС“, бр. 18/2016 и 95/2018-аутентично тумачење), доноси

				</fo:block>
				<fo:block font-family="Times New Roman" width="60%" font-size="15pt" margin-top="10pt" margin-bottom="10pt" text-align="center" border="none">
						Р Е Ш Е Њ Е
				</fo:block>
				<fo:block font-family="Times New Roman" width="60%"
						margin-top="10pt" text-align="left" border="none">
						<xsl:value-of select="/resenje/tekst_resenja/tekst"></xsl:value-of>
				</fo:block>
				<fo:block font-family="Times New Roman" width="60%" font-size="12pt" margin-top="10pt" margin-bottom="10pt" text-align="center" border="none">
						O б р а з л о ж е њ е
				</fo:block>
				<fo:block font-family="Times New Roman" width="60%"
						margin-top="10pt" text-align="left" border="none">
						<xsl:value-of select="/resenje/tekst_obrazlozenja"></xsl:value-of>
				</fo:block>
				<fo:block font-family="Times New Roman" width="60%"
						margin-top="10pt" text-align="left" border="none">
						<xsl:text xml:space="preserve">		</xsl:text>Против овог решења није допуштена жалба већ се, у складу са Законом о управним
споровима, може покренути управни спор тужбом Управном суду у Београду, у року од 30 дана од
дана пријема решења. Такса на тужбу износи 390,00 динара.
				</fo:block>
				<fo:block font-family="Times New Roman" width="60%"
						margin-top="10pt" text-align="right" border="none">
						ПОВЕРЕНИК
				</fo:block>
				<fo:block font-family="Times New Roman" width="60%"
						 text-align="right" border="none">
						<xsl:value-of select="/resenje/poverenik/ime"></xsl:value-of><xsl:text> </xsl:text><xsl:value-of select="/resenje/poverenik/prezime"></xsl:value-of>
				</fo:block>
            </fo:flow>
            </fo:page-sequence>
            
            </fo:root>
            </xsl:template>
            </xsl:stylesheet>