import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import * as JsonToXml from 'js2xmlparser';

@Injectable({
  providedIn: 'root'
})
export class LogInService {

  private readonly path = 'http://localhost:8082/auth/log-in';

constructor(private http: HttpClient) { }

logIn(user : any) : Observable<any> {

  const headers = new HttpHeaders({
    'Content-Type' : 'application/xml',
    'Accept' : 'application/xml'
  });

  return this.http.post(this.path, user, {headers:headers, responseType:'text'});
  }
}
