import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  private readonly path = 'http://localhost:8082/requests';

  constructor(private http: HttpClient) { }

  getAllRequests(){
    const headers = new HttpHeaders({
      'Content-Type' : 'application/xml',
      'Accept' : 'application/xml'
    });

    return this.http.get(this.path, {headers:headers, responseType:'text'});

  }

  getAllForCitizen(){
    const headers = new HttpHeaders({
      'Content-Type' : 'application/xml',
      'Accept' : 'application/xml'
    });

    return this.http.get(`${this.path}/user`, {headers:headers, responseType:'text'});

  }

  declineRequest(declinedReq : any){
    const headers = new HttpHeaders({
      'Content-Type' : 'application/xml',
      'Accept' : 'application/xml'
    });
    return this.http.put(`${this.path}/decline`,declinedReq, {headers:headers, responseType:'text'});
  }

  createRequest(request : any){
    const headers = new HttpHeaders({
      'Content-Type' : 'application/xml',
      'Accept' : 'application/xml'
    });

    return this.http.put(this.path, request, {headers : headers});
  }

  getOneRequest(findReq : any){
    const headers = new HttpHeaders({
      'Content-Type' : 'application/xml',
      'Accept' : 'application/xml'
    });
    return this.http.put(`${this.path}/find`,findReq, {headers:headers, responseType:'text'});
  }

  searchRequest(value : any){
    const headers = new HttpHeaders({
      'Content-Type' : 'application/xml',
      'Accept' : 'application/xml'
    });
    return this.http.get(`${this.path}/search/${value}`, {headers:headers, responseType:'text'});
  }

  export(id: string, type :string){

    return  this.http.get(`${this.path}/export/${id}/${type}`, {responseType: 'arraybuffer' as 'text' });
  }

}
