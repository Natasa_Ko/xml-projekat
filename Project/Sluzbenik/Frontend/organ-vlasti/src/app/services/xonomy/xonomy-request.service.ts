import { Injectable } from '@angular/core';

declare const Xonomy : any;

@Injectable({
  providedIn: 'root'
})
export class XonomyRequestService {

constructor() { }

public requestSpecification = {
  validate: function (jsElement) {
    //Validate the element:
    let elementSpec = this.elements[jsElement.name];
    if (elementSpec.validate) elementSpec.validate(jsElement);
    //Cycle through the element's attributes:
    for (let i = 0; i < jsElement.attributes.length; i++) {
      let jsAttribute = jsElement.attributes[i];
      let attributeSpec = elementSpec.attributes[jsAttribute.name];
      if (attributeSpec.validate) attributeSpec.validate(jsAttribute);
    }
    //Cycle through the element's children:
    for (let i = 0; i < jsElement.children.length; i++) {
      let jsChild = jsElement.children[i];
      if (jsChild.type == "element") { //if element
        this.validate(jsChild); //recursion
      }
    }
  },
  elements: {
    zahtev : {
      hasText: false,
      validate: function (jsElement) {
        if (jsElement.getText() == "") {
          Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovaj element ne sme biti prazan!"
            }
          );
        }
      },
      menu: [],
      attributes: {
        "xmlns": {
          isInvisible: true,
        },
        "xmlns:pred": {
          isInvisible: true,
        }
      }
    },

    //podaci o organu
    podaci_o_organu : {
      hasText : false,

    },
    naziv_organa : {
      validate: function (jsElement) {
        if (jsElement.getText() == "") {
          Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovaj element ne sme biti prazan!"
            }
          );
        }
      },
      hasText: true,
      asker: Xonomy.askString,
      attributes: {
        "property": {
          isInvisible: true,
        }
      }
    },
    sediste_organa : {
      validate: function (jsElement) {
        if (jsElement.getText() == "") {
          Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovaj element ne sme biti prazan!"
            }
          );
        }
      },
      hasText: true,
      asker: Xonomy.askString,
      attributes: {
        "property": {
          isInvisible: true,
        }
      }
    },
    //Podaci o zakonu
    zahtev_na_osnovu : {
      collapsed: function(jsElement){return true;},
      hasText : false,
    },
    broj_clana : {
      hasText: true,
      isReadOnly : true
    },
    stav : {
      hasText: true,
      isReadOnly : true
    },
    zakon : {
      hasText: true,
      isReadOnly : true
    },

    //Elementi zahteva
    elementi_zahteva : {
      hasText : false,
      oneliner : true,
    },
    uvid_u_dokument : {
      validate: function (jsElement) {
        if (jsElement.getText() == "true") {
          Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Obrišite true ako ne želite ovu opciju"
            }
          );
        }
      },
      collapsed: function(jsElement){return false;},
      hasText: true,
      oneliner: true,
      asker: Xonomy.askString,

    },
    posedovanje_informacije : {
      validate: function (jsElement) {
        if (jsElement.getText() == "true") {
          Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Obrišite true ako ne želite ovu opciju"
            }
          );
        }
      },
      collapsed: function(jsElement){return false;},
      hasText: true,
      oneliner: true,
      asker: Xonomy.askString,
    },
    kopija_dokumenta : {
      validate: function (jsElement) {
        if (jsElement.getText() == "true") {
          Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Obrišite true ako ne želite ovu opciju"
            }
          );
        }
      },
      collapsed: function(jsElement){return false;},
      hasText: true,
      oneliner: true,
      asker: Xonomy.askString,
    },
    dostavljanje_kopije_dokumenta : {
      hasText : false,
    },
    postom : {
      hasText: true,
      oneliner : true,
      asker: Xonomy.askPicklist,
        askerParameter: [
            {value: "true", caption: "Odaberite opciju"}
        ]
    },
    elektronskom_postom : {
      hasText: true,
      oneliner : true,
      asker: Xonomy.askPicklist,
        askerParameter: [
            {value: "true", caption: "Odaberite opciju"}
        ]
    },
    faksom : {
      hasText: true,
      oneliner : true,
        asker: Xonomy.askPicklist,
        askerParameter: [
            {value: "true", caption: "Odaberite opciju"}
        ]
    },
    drugi_nacin : {
      hasText: true,
      asker: Xonomy.askOpenPicklist,
      oneliner : true,
      askerParameter: [
        {value: "Lično", caption: "Unesite nacin! "}
    ]
    },
    informacije_na_koje_se_odnosi_zahtev : {
      validate: function (jsElement) {
        if (jsElement.getText() == "") {
          Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovaj element ne sme biti prazan!"
            }
          );
        }
      },
      hasText : true,
      asker: Xonomy.askString,
    },
    trazioc : {
      hasText : false,
    },
    fizicko_lice : {
      hasText : false,
    },
    ime : {
      hasText: true,
      asker: Xonomy.askString,
      oneliner: true,
      isReadOnly: true,
      attributes: {
        "property": {
          isInvisible: true,
        }
      }

    },
    prezime : {
      hasText: true,
      asker: Xonomy.askString,
      oneliner: true,
      isReadOnly: true,
      attributes: {
        "property": {
          isInvisible: true,
        }
      }

    },
    pravno_lice : {
      validate: function (jsElement) {
        if (jsElement.getText() == "") {
          Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Unesite ime firme."
            }
          );
        }
      },
      hasText: true,
      asker: Xonomy.askString,
      oneliner: true,
    },
    adresa : {
      hasText : false,
    },
    ulica : {
      validate: function (jsElement) {
        if (jsElement.getText() == "") {
          Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Овај елемент не сме бити празан."
            }
          );
        }
      },
      hasText: true,
        asker: Xonomy.askString,
        oneliner: true,
    },
    broj : {
      validate: function (jsElement) {
        if (jsElement.getText() == "") {
          Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Овај елемент не сме бити празан."
            }
          );
        }
      },
      hasText: true,
        asker: Xonomy.askString,
        oneliner: true,
    },
    grad : {
      validate: function (jsElement) {
        if (jsElement.getText() == "") {
          Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Овај елемент не сме бити празан."
            }
          );
        }
      },
      hasText: true,
        asker: Xonomy.askString,
        oneliner: true,
    },
    drugi_podaci_za_kontakt : {
      hasText: true,
      asker: Xonomy.askString,
      oneliner: true,
    }
  }
}
  }


