/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { XonomyRequestService } from './xonomy-request.service';

describe('Service: XonomyRequest', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [XonomyRequestService]
    });
  });

  it('should ...', inject([XonomyRequestService], (service: XonomyRequestService) => {
    expect(service).toBeTruthy();
  }));
});
