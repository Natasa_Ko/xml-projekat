import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  private readonly path = 'http://localhost:8082/auth/sign-up';

constructor(private http: HttpClient) { }

register(user : any) : Observable<any> {

  const headers = new HttpHeaders({
    'Content-Type' : 'application/xml',
    'Accept' : 'application/xml'
  });

  return this.http.post(this.path, user, {headers:headers, responseType:'text'});
  }
}


