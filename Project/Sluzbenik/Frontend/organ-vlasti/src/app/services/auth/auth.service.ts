import { Inject, Injectable } from '@angular/core';
import * as moment from 'moment';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ReplaySubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {


  private helper = new JwtHelperService();


  constructor(@Inject('LOCALSTORAGE') private localStorage: Storage) { }

  setSession(authResult: any) {

    const expiresAt = moment().add(authResult.expiresIn, 'second');
    let decodedToken = this.helper.decodeToken(authResult.accessToken);
    console.log(decodedToken);
    this.localStorage.setItem('currentUser', JSON.stringify({
      accessToken: authResult.accessToken,
      isServant: decodedToken.role === "ROLE_SERVANT",
      isCitizen: decodedToken.role === "ROLE_CITIZEN",
      expiration: expiresAt,
      fullName: decodedToken.fullName,
      logged: true
    }));
    console.log(JSON.parse(this.localStorage.getItem('currentUser')));


  }

  logout() {
    localStorage.removeItem("currentUser");
  }

  getCurrentUser() {
    return JSON.parse(this.localStorage.getItem('currentUser'));
  }

  getToken() {
    let user = JSON.parse(this.localStorage.getItem('currentUser'));
    if(user)
      return user.accessToken;
  }
  public isLoggedIn() {
    return moment().isBefore(this.getExpiration());
  }

  isLoggedOut() {
    return !this.isLoggedIn();
  }

  getExpiration() {
    const expiration = this.getCurrentUser().expiresAt;
    const expiresAt = JSON.parse(expiration || '{}');
    return moment(expiresAt);
  }

  isServant() {
    let currentUser = this.getCurrentUser();
    console.log(currentUser.isServant);
    return currentUser.isServant;
  }

  isCitizen() {
    let currentUser = this.getCurrentUser();
    console.log(currentUser.isCitizen);
    return currentUser.isCitizen;
  }
}
