import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import * as moment from 'moment';
import { AuthService } from '../services/auth/auth.service';

import { NotificationService } from '../services/notification.service';

@Injectable()
export class LoggedInGuard implements CanActivate {

    constructor(private router: Router,
        private notificationService: NotificationService,
        private authService: AuthService) { }

    canActivate() {
        const user = this.authService.getCurrentUser();

        if (user && user.expiration) {
            this.notificationService.openSnackBar('Već ste ulogovani!');
            this.router.navigate(['home']);
            return false;
        } else {
            return true;
        }

    }
}
