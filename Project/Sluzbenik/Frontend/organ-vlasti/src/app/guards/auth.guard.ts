import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import * as moment from 'moment';
import { AuthService } from '../services/auth/auth.service';

import { NotificationService } from '../services/notification.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router,
        private notificationService: NotificationService,
        private authService: AuthService) { }

    canActivate() {
        const user = this.authService.getCurrentUser();

        if (user && user.expiration) {

            if (moment() < moment(user.expiration)) {
                return true;
            } else {
                this.notificationService.openSnackBar('Vaša sesija je istekla!');
                this.router.navigate(['login']);
                return false;
            }
        }
        this.notificationService.openSnackBar('Niste ulogovani!');
        this.router.navigate(['login']);
        return false;
    }
}
