import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthGuard } from 'src/app/guards/auth.guard';
import { AuthService } from 'src/app/services/auth/auth.service';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { timer } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-options',
  templateUrl: './options.component.html',
  styleUrls: ['./options.component.scss']
})
export class OptionsComponent implements OnInit {

  private _mobileQueryListener: () => void;
  mobileQuery: MediaQueryList;
  showSpinner: boolean;
  userName: string;
  isServant: boolean;
  isCitizen: boolean;

  private autoLogoutSubscription: Subscription;


  constructor(private router : Router,private changeDetectorRef: ChangeDetectorRef, private media: MediaMatcher,
        public spinnerService: SpinnerService,
        private authService: AuthService,
        private authGuard: AuthGuard) {

        this.mobileQuery = this.media.matchMedia('(max-width: 1000px)');
        this._mobileQueryListener = () => changeDetectorRef.detectChanges();
        //tslint:disable-next-line: deprecation
        this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit() {

    const user = this.authService.getCurrentUser();

        this.isCitizen = user.isCitizen;
        this.isServant = user.isServant;
        this.userName = user.fullName;

        // Auto log-out subscription
        const timerSub = timer(2000, 5000);
        this.autoLogoutSubscription = timerSub.subscribe(t => {
            this.authGuard.canActivate();
        });
  }

  ngOnDestroy(): void {
    // tslint:disable-next-line: deprecation
    this.mobileQuery.removeListener(this._mobileQueryListener);
    this.autoLogoutSubscription.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.changeDetectorRef.detectChanges();
  }

  logout(){
    this.authService.logout();
    this.router.navigate(['login']);
  }



}
