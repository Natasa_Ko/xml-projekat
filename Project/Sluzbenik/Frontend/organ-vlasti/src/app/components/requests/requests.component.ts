import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth/auth.service';
import { NotificationService } from 'src/app/services/notification.service';
import { RequestService } from 'src/app/services/request/request.service';
import * as txml from 'txml';
import * as js2xml from 'js2xmlparser';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { CreateNoticeComponent } from '../create-notice/create-notice.component';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.scss']
})
export class RequestsComponent implements OnInit {

  currentUser : any;
  requests : any;
  declining: boolean = false;
  pagNumber : number;
  displayedColumns: string[] = [
    'brojZahtjeva',
    'trazilac',
    'mjesto',
    'datumPodnosenja',
    'organ',
    'odobren',
    'odbijanje',
    'obavjestenje',
    'preuzimanje',
  ];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSourceRequests: MatTableDataSource<any>;


  constructor(private dialog: MatDialog, private notification : NotificationService, private requestService : RequestService, private authService : AuthService, private router : Router) {
    this.currentUser = authService.getCurrentUser();
    if(this.currentUser.isCitizen){
      this. displayedColumns = [
        'brojZahtjeva',
        'trazilac',
        'mjesto',
        'datumPodnosenja',
        'organ',
        'odobren',
        'preuzimanje'
      ];
    }
    this.requests = [];
    this.pagNumber = 0;
   }

  ngOnInit() {
    if(this.currentUser.isServant){
    this.requestService.getAllRequests()
    .pipe(
      catchError( err => {
        console.log(err)
        this.notification.openSnackBar("Trenutno nema zahteva!");
        return throwError(err);
      }))
    .subscribe(data => {
      console.log(data);
      console.log(data);
      this.createTable(data);
    });
    }else if(this.currentUser.isCitizen){

      this.requestService.getAllForCitizen()
      .pipe(
        catchError( err => {
          console.log(err)
          this.notification.openSnackBar("Niste podneli ni jedan zahtev!");
          return throwError(err);
        }))
      .subscribe(data =>{
        //console.log(data);
        this.createTable(data);
      });
    }

  }

  createTable(data : any){
    console.log(data);
    let obj : any = txml.parse(data);
    let requests : any = txml.simplify(obj);
    console.log(requests);
    let tableReq : any[] = [];
    let number = 1;
    let stanje = "";
   if(requests.List.item.length > 1){
    this.requests = requests;
    requests.List.item.forEach(req =>{
      if(req.id.includes("ZA_")){
        let requestDate = new Date(req.datum);
        let now = new Date();
        console.log(now.getTime());
        console.log(requestDate.getTime());
        var diffMs = now.getTime() - requestDate.getTime(); // milliseconds between now & Christmas
        var diffDays = Math.floor(diffMs / 86400000); // days
        var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
        var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
      console.log(diffDays + " days, " + diffHrs + " hours, " + diffMins + " minutes");
        if (diffDays > 30 || diffHrs > 24
          //|| diffMins > 5
          ) {
          stanje = "Zastario";
        }
        else
          stanje = "U procesu";
      }
      console.log(req);
      let request = {
        'brojZahtjeva' : req.id,
        'trazilac' : req.informacijeOTraziocu.trazioc.fizickoLice.ime.value +" "+ req.informacijeOTraziocu.trazioc.fizickoLice.prezime.value,
        'mjesto': req.mesto,
        'datumPodnosenja' : req.datum,
        'organ' : req.podaciOOrganu.nazivOrgana.value,
        'odobren' : req.id.includes("ZA_") ? stanje : req.id.includes("ZAO_") ? "Odobren" : "Neodobren"
      }
      tableReq.push(request);
    });
   }else{
      let req = requests.List.item;
      this.requests = requests;
      if(req.id.includes("ZA_")){
        let requestDate = new Date(req.datum);
        let now = new Date();
        var diffMs = (now.getTime() - requestDate.getTime()); // milliseconds between now & Christmas
        var diffDays = Math.floor(diffMs / 86400000); // days
        var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
        var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
      console.log(diffDays + " days, " + diffHrs + " hours, " + diffMins + " minutes");
        if (diffDays > 30 || diffHrs > 24
          //|| diffMins > 5
          ) {
          stanje = "Zastario";
        }
        else
          stanje = "U procesu";
      }
      let request = {
        'brojZahtjeva' : req.id,
        'trazilac' : req.informacijeOTraziocu.trazioc.fizickoLice.ime.value +" "+ req.informacijeOTraziocu.trazioc.fizickoLice.prezime.value,
        'mjesto': req.mesto,
        'datumPodnosenja' : req.datum,
        'organ' : req.podaciOOrganu.nazivOrgana.value,
        'odobren' : req.id.includes("ZA_") ? stanje : req.id.includes("ZAO_") ? "Odobren" : "Neodobren"
      }
      tableReq.push(request);
      number++;
   }
    console.log(tableReq);
    this.pagNumber = tableReq.length;
    this.dataSourceRequests = new MatTableDataSource(tableReq);
    this.dataSourceRequests.sort = this.sort;
    this.dataSourceRequests.paginator = this.paginator;
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceRequests.filter = filterValue.trim().toLowerCase();

    if (this.dataSourceRequests.paginator) {
      this.dataSourceRequests.paginator.firstPage();
    }
  }

  decline(brojZahtjeva : string){
    console.log(brojZahtjeva);

    let _declinedReq : any = {
      "@" : {
        id : brojZahtjeva,
      }
    }

    let declinedReq = js2xml.parse('Request', _declinedReq);
    if(confirm("Želite li odbiti zahtev?")){
      this.declining = true;
    this.requestService.declineRequest(declinedReq).subscribe(() => {
      console.log("Success");
      this.notification.openSnackBar("Zahtev je odbijen!");
      this.declining = false;
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate(['/home/requests']);
    });
    }
  }

  createNotice(element : any){
    let request : any;
    if(this.requests.List.item.length > 1){
      this.requests.List.item.forEach(req => {
        if(req.id === element)
            request = req;
      });
    }else if(this.requests.List.item.id === element){
      request = this.requests.List.item;

    }
    const dialogRef = this.dialog.open(CreateNoticeComponent, {
      data: {
         request
      },
      maxHeight: window.innerHeight + 'px',
    });
    dialogRef.afterClosed().subscribe((result) => {
      console.log(result);
      //this.notification.openSnackBar("Obaveštenje je dodano!");
      this.router.navigate(['/home/notices']);
    });
  }

  exportPDF(elementId : any){
    this.requestService.export(elementId, "PDF").subscribe((response) =>{
      console.log(response);
      let file = new Blob([response], { type: 'application/pdf' });
      var fileURL = URL.createObjectURL(file);
      let a = document.createElement('a');
      document.body.appendChild(a);
      a.setAttribute('style', 'display: none');
      a.href = fileURL;
      a.download = `Zahtev_${elementId}.pdf`;
      a.click();
      window.URL.revokeObjectURL(fileURL);
      a.remove();
    }),(error) => this.notification.openSnackBar("Desila se greška!");
    () => this.notification.openSnackBar("Uspješno ste skinuli dokument u PDF formatu!");

}

exportRDF(elementId : any){
  this.requestService.export(elementId, "RDF").subscribe((response) => {
    let file = new Blob([response], { type: 'application/RDF' });

    var fileURL = URL.createObjectURL(file);

    let a = document.createElement("a");

    document.body.appendChild(a);

    a.setAttribute("style", "display: none");

    a.href = fileURL;

    a.download = `Zahtev_${elementId}.rdf`;
    a.click();
    window.URL.revokeObjectURL(fileURL);
    a.remove();
  }),
    (error) => this.notification.openSnackBar("Desila se greška!");
    () => this.notification.openSnackBar("Uspješno ste izvezli dokument u RDF formatu!");
}

exportJSON(elementId : any){

    this.requestService.export(elementId, "RDF").subscribe((response) => {
      let file = new Blob([response], { type: 'application/JSON' });

      var fileURL = URL.createObjectURL(file);

      let a = document.createElement("a");

      document.body.appendChild(a);

      a.setAttribute("style", "display: none");

      a.href = fileURL;

      a.download = `Zahtev_${elementId}.json`;
      a.click();
      window.URL.revokeObjectURL(fileURL);
      a.remove();
    }),
      (error) => this.notification.openSnackBar("Desila se greška!");
      () => this.notification.openSnackBar("Uspješno ste izvezli dokument u JSON formatu!");
  }

  exportXHTML(elementId : any){
    this.requestService.export(elementId, "HTML").subscribe((response) => {
      let file = new Blob([response], { type: 'text/html' });

      var fileURL = URL.createObjectURL(file);

      let a = document.createElement("a");

      document.body.appendChild(a);

      a.setAttribute("style", "display: none");

      a.href = fileURL;

      a.download = `Zahtev_${elementId}.html`;
      a.click();
      window.URL.revokeObjectURL(fileURL);
      a.remove();
    }),
      (error) => this.notification.openSnackBar("Desila se greška!");
      () => this.notification.openSnackBar("Uspješno ste skinuli dokument u HTML formatu!");
  }



}
