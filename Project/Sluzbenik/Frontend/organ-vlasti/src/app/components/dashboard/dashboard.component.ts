import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AuthService } from 'src/app/services/auth/auth.service';
import { RequestService } from 'src/app/services/request/request.service';
import * as txml from 'txml';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  tableReq : any;
  currentUser : any;
  requests : any;
  declining: boolean = false;
  pagNumber : number;
  displayedColumns: string[] = [
    'brojZahtjeva',
    'trazilac',
    'mjesto',
    'datumPodnosenja',
    'organ',
    'odobren',
    'odbijanje',
    'obavjestenje'
  ];
  value : string;
  dataSourceRequests: MatTableDataSource<any>;

  constructor(private requestService : RequestService, private authService : AuthService) {
    this.currentUser = authService.getCurrentUser();
  }

  ngOnInit() {
  }

  search(value : any){
    this.requestService.searchRequest(value).subscribe((response) => {
      let obj: any = txml.parse(response);
      let requests: any = txml.simplify(obj);
      this.extractDecisions(requests);
      console.log(requests);
    });
    console.log(value);
  }

  extractDecisions(requests : any){
    let tableReq : any[] = [];
    let number = 1;
    let stanje = "";
    if(requests.List.item.length > 1){
      this.requests = requests;
      requests.List.item.forEach(req =>{
        if(req.id.includes("ZA_")){
          let requestDate = new Date(req.datum);
          let now = new Date();
          console.log(now.getTime());
          console.log(requestDate.getTime());
          var diffMs = now.getTime() - requestDate.getTime(); // milliseconds between now & Christmas
          var diffDays = Math.floor(diffMs / 86400000); // days
          var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
          var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
        console.log(diffDays + " days, " + diffHrs + " hours, " + diffMins + " minutes");
          if (diffDays > 30 || diffHrs > 24
            //|| diffMins > 5
            ) {
            stanje = "Zastario";
          }
          else
            stanje = "U procesu";
        }
        console.log(req);
        let request = {
          'brojZahtjeva' : req.id,
          'trazilac' : req.informacijeOTraziocu.trazioc.fizickoLice.ime.value +" "+ req.informacijeOTraziocu.trazioc.fizickoLice.prezime.value,
          'mjesto': req.mesto,
          'datumPodnosenja' : req.datum,
          'organ' : req.podaciOOrganu.nazivOrgana.value,
          'odobren' : req.id.includes("ZA_") ? stanje : req.id.includes("ZAO_") ? "Odobren" : "Neodobren"
        }
        tableReq.push(request);
      });
     }else{
        let req = requests.List.item;
        this.requests = requests;
        if(req.id.includes("ZA_")){
          let requestDate = new Date(req.datum);
          let now = new Date();
          var diffMs = (now.getTime() - requestDate.getTime()); // milliseconds between now & Christmas
          var diffDays = Math.floor(diffMs / 86400000); // days
          var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
          var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
        console.log(diffDays + " days, " + diffHrs + " hours, " + diffMins + " minutes");
          if (diffDays > 30 || diffHrs > 24
            //|| diffMins > 5
            ) {
            stanje = "Zastario";
          }
          else
            stanje = "U procesu";
        }
        let request = {
          'brojZahtjeva' : req.id,
          'trazilac' : req.informacijeOTraziocu.trazioc.fizickoLice.ime.value +" "+ req.informacijeOTraziocu.trazioc.fizickoLice.prezime.value,
          'mjesto': req.mesto,
          'datumPodnosenja' : req.datum,
          'organ' : req.podaciOOrganu.nazivOrgana.value,
          'odobren' : req.id.includes("ZA_") ? stanje : req.id.includes("ZAO_") ? "Odobren" : "Neodobren"
        }
        tableReq.push(request);
        number++;
     }
      console.log(tableReq);
      this.tableReq = tableReq;
      this.pagNumber = tableReq.length;
      this.dataSourceRequests = new MatTableDataSource(tableReq);
  }

}
