import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { NoticeService } from 'src/app/services/notice/notice.service';
import { NotificationService } from 'src/app/services/notification.service';
import { RequestService } from 'src/app/services/request/request.service';
import { XonomyNoticeService } from 'src/app/services/xonomy/xonomy-notice.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { DatePipe } from '@angular/common';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

declare const Xonomy : any;

@Component({
  selector: 'app-create-notice',
  templateUrl: './create-notice.component.html',
  styleUrls: ['./create-notice.component.scss']
})
export class CreateNoticeComponent implements OnInit {

  request : any;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private dialogRef: MatDialogRef<CreateNoticeComponent>,
              private noticeService:NoticeService,
              private notificationService: NotificationService,
              private xonomyService : XonomyNoticeService) {

  }

  ngOnInit() {
    console.log(this.data);
    window.scroll(0, 0);
    this.showXonomy();
  }

  showXonomy() {
    let element = document.getElementById("noticeEditor");
    let datum = new Date(this.data.request.datum);
    let pipe = new DatePipe('en-US');
    const myFormattedDate = pipe.transform(datum, 'dd-MM-yyyy');
    let xmlString = `<obavestenje xmlns= "http://www.euprava.sluzbenik.gov.rs/notice" xmlns:pred= "http://www.euprava.sluzbenik.gov.rs/rdf/examples/predicate/" idZahteva="${this.data.request.id}">
    <podaci_o_primaocu>
  <naziv property = "pred:naziv_ustanove" >`+this.data.request.podaciOOrganu.nazivOrgana.value+ `</naziv>
  <sediste property = "pred:sediste_ustanove" >`+this.data.request.podaciOOrganu.sedisteOrgana.value+ `</sediste>
        <broj_predmeta></broj_predmeta>
  <datum_podnosenja property = "pred:datum_podnosenja"></datum_podnosenja>
</podaci_o_primaocu>
<podaci_o_podnosiocu>
  <ime_podnosioca property = "pred:ime_podnosioca" >`+this.data.request.informacijeOTraziocu.trazioc.fizickoLice.ime.value+ `</ime_podnosioca>
  <prezime_podnosioca property = "pred:prezime_podnosioca" >`+this.data.request.informacijeOTraziocu.trazioc.fizickoLice.prezime.value+ `</prezime_podnosioca>
  <naziv_podnosioca property = "pred:naziv_podnosioca" >`+this.data.request.informacijeOTraziocu.trazioc.pravnoLice+ `</naziv_podnosioca>
  <adresa_podnosioca property = "pred:adresa_podnosioca" >`+this.data.request.informacijeOTraziocu.adresa.ulica + ` `+this.data.request.informacijeOTraziocu.adresa.broj + `</adresa_podnosioca>
</podaci_o_podnosiocu>
  <trazena_informacija>
  <datum_zahteva property="pred:datum_zahteva" >`+myFormattedDate+`</datum_zahteva>
  <opis_trazene_informacije property="pred:opis_informacije" >`+this.data.request.informacijeNaKojeSeOdnosiZahtev+ `</opis_trazene_informacije>
  </trazena_informacija>
  <obavestenje>
  <datum_uvida></datum_uvida>
      <od_vreme></od_vreme>
  <do_vreme></do_vreme>
  <grad></grad>
  <ulica></ulica>
  <broj_ulice></broj_ulice>
  <broj_kancelarije></broj_kancelarije>
  </obavestenje>
  <troskovi>
     <format tip = "A4" cena = "3">3.00</format>
  </troskovi>
  <uplata>
     <iznos_za_uplatu></iznos_za_uplatu>
     <racun broj = "" model = ""></racun>
  </uplata>
  <dostavljeno>
      <opcija broj = "1">1. Именованом (М.П.)</opcija>
      <opcija broj = "2">2. Архиви</opcija>
  </dostavljeno>
  <potpis_ovlascenog_lica/>
</obavestenje>`;
    console.log(xmlString);
    let specification = this.xonomyService.noticeSpecification;
    Xonomy.setMode("laic");
    Xonomy.render(xmlString, element, specification);
  }

  createNotice(){
    let data : any = Xonomy.harvest();
    console.log(data);
    this.noticeService.createNotice(data)
    .pipe(
      catchError( err => {
        console.log(err)
        this.notificationService.openSnackBar("Neuspešno kreiranje. Pokušajte ponovo!");
        this.dialogRef.close();
        return throwError(err);
      }))
    .subscribe(() =>{
      this.notificationService.openSnackBar("Uspešno ste kreirali obaveštenje!");
      this.dialogRef.close();
    });
  }


}
