import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LogInService } from 'src/app/services/log-in/log-in.service';
import * as js2xml from 'js2xmlparser';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { NotificationService } from 'src/app/services/notification.service';


@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss']
})
export class LogInComponent implements OnInit {

  hide : Boolean;

  loginForm: FormGroup;
  loading: boolean;
  domParser : DOMParser;

  constructor(private notification : NotificationService, private loginService : LogInService, private router : Router, private authService : AuthService) {
    this.hide = true;
    this.domParser = new DOMParser();
   }

  ngOnInit() {
    this.createForm();
  }


  private createForm() {

    this.loginForm = new FormGroup({
        email: new FormControl('', [Validators.required, Validators.email]),
        password: new FormControl('', Validators.required),
    });
  }

  login() {
    const email = this.loginForm.get('email').value;
    const password = this.loginForm.get('password').value;

    this.loading = true;

    console.log("Email ", email);
    console.log("Password ", password);

    let _user : any = {
      "@" : {
        username : email,
        password : password
      }
    }

    let user = js2xml.parse('Request', _user);

    console.log(user);

    this.loginService.logIn(user).pipe(
      catchError( err => {
        console.log(err)
        this.notification.openSnackBar("Pogrešno uneseni podaci!");
        this.loading = false;
        return throwError(err);
      }))
    .subscribe(data =>{
      console.log(data);
      let xmlData = this.domParser.parseFromString(data, 'text/xml');
      let accessToken = xmlData.querySelector('accessToken').innerHTML;
      let expiresIn = xmlData.querySelector('expiresIn').innerHTML;
      let authResult = {
        accessToken : accessToken,
        expiresIn : expiresIn
      };
      this.authService.setSession(authResult);
      this.router.navigate(['home']);
    });

  }

  showPassword(){
    console.log(this.hide, "WAAS");
    if(this.hide)
        this.hide = false;
    else
        this.hide = true;
  }

}
