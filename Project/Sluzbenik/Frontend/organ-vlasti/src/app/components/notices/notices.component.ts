import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth/auth.service';
import { NoticeService } from 'src/app/services/notice/notice.service';
import { NotificationService } from 'src/app/services/notification.service';
import { RequestService } from 'src/app/services/request/request.service';
import * as txml from 'txml';

@Component({
  selector: 'app-notices',
  templateUrl: './notices.component.html',
  styleUrls: ['./notices.component.scss']
})
export class NoticesComponent implements OnInit {

  currentUser : any;
  requests : any;

  pagNumber : number;
  displayedColumns: string[] = [
    'brojObavjestenja',
    'brojZahteva',
    'nazivOrgana',
    'trazilac',
    'trazenaInformacija',
    'datumUvida',
    'vrijemeUvida',
    'iznosZaUplatu',
    'preuzimanje'
  ];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSourceNotices: MatTableDataSource<any>;


  constructor(private notification : NotificationService, private noticeService : NoticeService, private authService : AuthService) {
    this.currentUser = authService.getCurrentUser();
    this.requests = [];
    this.pagNumber = 0;
   }

  ngOnInit() {

    if(this.currentUser.isServant){
      this.noticeService.getAllNotices()
      .pipe(
        catchError( err => {
          console.log(err)
          this.notification.openSnackBar("Niste napisali ni jedno obaveštenje!");
          return throwError(err);
        }))
      .subscribe(data => {
        this.createTable(data);
      });
      }else if(this.currentUser.isCitizen){

        this.noticeService.getAllNoticesForCitizen()
        .pipe(
          catchError( err => {
            console.log(err)
            this.notification.openSnackBar("Trenutno nema obaveštenja!");
            return throwError(err);
          }))
        .subscribe(data =>{
          console.log(data);
          this.createTable(data);
        });
      }

  }

  createTable(data : any){
    let obj : any = txml.parse(data);
    let notices : any = txml.simplify(obj);
    let tableNotice : any[] = [];
    let number = 1;
    if(notices.List.item.length > 1){
    notices.List.item.forEach(n =>{
      console.log(n);
      let request = {
        'brojObavjestenja' : n.id,
        'brojZahteva' : n.idZahteva,
        'nazivOrgana':  n.podaciOPrimaocu.naziv.value,
        'trazilac': n.podaciOPodnosiocu.imePodnosioca.value +" " + n.podaciOPodnosiocu.prezimePodnosioca.value,
        'trazenaInformacija': n.trazenaInformacija.opisTrazeneInformacije.value,
        'datumUvida' : n.obavestenje.datumUvida,
        'vrijemeUvida': n.obavestenje.odVreme +"-"+ n.obavestenje.doVreme,
        'iznosZaUplatu': n.uplata.iznosZaUplatu,
      }
      tableNotice.push(request);
      number++;
    });
  }else{
    let n = notices.List.item;
    let notice = {
      'brojObavjestenja' : n.id,
      'brojZahteva': n.idZahteva,
        'nazivOrgana':  n.podaciOPrimaocu.naziv.value,
        'trazilac': n.podaciOPodnosiocu.imePodnosioca.value +" " + n.podaciOPodnosiocu.prezimePodnosioca.value,
        'trazenaInformacija': n.trazenaInformacija.opisTrazeneInformacije.value,
        'datumUvida' : n.obavestenje.datumUvida,
        'vrijemeUvida': n.obavestenje.odVreme +"-"+ n.obavestenje.doVreme,
        'iznosZaUplatu': n.uplata.iznosZaUplatu,
    }
    tableNotice.push(notice);
    number++;
  }
    console.log(tableNotice);
    this.pagNumber = tableNotice.length;
    this.dataSourceNotices = new MatTableDataSource(tableNotice);
    this.dataSourceNotices.sort = this.sort;
    this.dataSourceNotices.paginator = this.paginator;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceNotices.filter = filterValue.trim().toLowerCase();

    if (this.dataSourceNotices.paginator) {
      this.dataSourceNotices.paginator.firstPage();
    }
  }

  exportPDF(elementId : any){
      this.noticeService.export(elementId, "PDF").subscribe((response) =>{
        console.log(response);
        let file = new Blob([response], { type: 'application/pdf' });
        var fileURL = URL.createObjectURL(file);
        let a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = fileURL;
        a.download = `Obavestenje_${elementId}.pdf`;
        a.click();
        window.URL.revokeObjectURL(fileURL);
        a.remove();
      }),(error) => this.notification.openSnackBar("Desila se greška!");
      () => this.notification.openSnackBar("Uspješno ste skinuli dokument u PDF formatu!");

  }

  exportXHTML(elementId : any){
    this.noticeService.export(elementId, "HTML").subscribe((response) => {
      let file = new Blob([response], { type: 'text/html' });

      var fileURL = URL.createObjectURL(file);

      let a = document.createElement("a");

      document.body.appendChild(a);

      a.setAttribute("style", "display: none");

      a.href = fileURL;

      a.download = `Obavestenje_${elementId}.html`;
      a.click();
      window.URL.revokeObjectURL(fileURL);
      a.remove();
    }),
      (error) => this.notification.openSnackBar("Desila se greška!");
      () => this.notification.openSnackBar("Uspješno ste skinuli dokument u HTML formatu!");
  }

  exportRDF(elementId : any){
    this.noticeService.export(elementId, "RDF").subscribe((response) => {
      let file = new Blob([response], { type: 'application/RDF' });

      var fileURL = URL.createObjectURL(file);

      let a = document.createElement("a");

      document.body.appendChild(a);

      a.setAttribute("style", "display: none");

      a.href = fileURL;

      a.download = `Obavestenje_${elementId}.rdf`;
      a.click();
      window.URL.revokeObjectURL(fileURL);
      a.remove();
    }),
      (error) => this.notification.openSnackBar("Desila se greška!");
      () => this.notification.openSnackBar("Uspješno ste izvezli dokument u RDF formatu!");
  }

  exportJSON(elementId : any){

      this.noticeService.export(elementId, "RDF").subscribe((response) => {
        let file = new Blob([response], { type: 'application/JSON' });

        var fileURL = URL.createObjectURL(file);

        let a = document.createElement("a");

        document.body.appendChild(a);

        a.setAttribute("style", "display: none");

        a.href = fileURL;

        a.download = `Obavestenje_${elementId}.json`;
        a.click();
        window.URL.revokeObjectURL(fileURL);
        a.remove();
      }),
        (error) => this.notification.openSnackBar("Desila se greška!");
        () => this.notification.openSnackBar("Uspješno ste izvezli dokument u JSON formatu!");
    }

}
