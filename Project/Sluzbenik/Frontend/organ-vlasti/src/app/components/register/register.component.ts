import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { RouteConfigLoadEnd, Router } from '@angular/router';
import * as js2xml from 'js2xmlparser';
import { throwError } from 'rxjs';
import { RegisterService } from 'src/app/services/register/register.service';
import { catchError } from 'rxjs/operators';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  loading: boolean;
  notSame : boolean;
  constructor(private registerService : RegisterService, private router : Router, private notification : NotificationService) {
    this.notSame = false;
  }

  ngOnInit() {
    this.createForm();
  }


  private createForm() {
    this.registerForm = new FormGroup({
        firstName: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(24)]),
        lastName: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(24)]),
        email: new FormControl('', [Validators.required, Validators.email]),
        password: new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(18)]),
        confirmPassword : new FormControl('', Validators.required)
    });


  }

  checkPasswords(event : any) {
   const password = this.registerForm.get('password').value;
   const repeatPassword = this.registerForm.get('confirmPassword').value;
   if(password === repeatPassword)
    {
      this.notSame = false;
    }
   else{
      this.notSame = true;
   }

  }



  register() {
    const firstName = this.registerForm.get('firstName').value;
    const lastName = this.registerForm.get('lastName').value;
    const email = this.registerForm.get('email').value;
    const password = this.registerForm.get('password').value;


    this.loading = true;

    console.log("Email ", email);
    console.log("Password ", password);
    console.log("First name ", firstName);
    console.log("Last name ", lastName);

    let _user : any = {
      "@" : {
        first_name: firstName,
        last_name : lastName,
        email : email,
        password : password
      }
    }

    let user = js2xml.parse('Request', _user);

    console.log(user);

    this.registerService.register(user)
    .pipe(
      catchError( err => {
        console.log(err);
        this.notification.openSnackBar("Nalog sa tom e-adresom već postoji!");
        this.loading = false;
        return throwError(err);
      }))
      .subscribe(() =>{
      this.router.navigate(['login']);
    });

  }

}
