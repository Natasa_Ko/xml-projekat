import {Component, Inject, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { NotificationService } from 'src/app/services/notification.service';
import { RequestService } from 'src/app/services/request/request.service';
import { XonomyRequestService } from 'src/app/services/xonomy/xonomy-request.service';


declare const Xonomy: any;

@Component({
  selector: 'app-create-request',
  templateUrl: './create-request.component.html',
  styleUrls: ['./create-request.component.scss']
})
export class CreateRequestComponent implements OnInit {

  currentUser : any;
  name : any;
  last_name : any;
  constructor(private authService: AuthService, private router : Router,private notificationService: NotificationService, private xonomyService : XonomyRequestService, private requestService : RequestService) {
    this.currentUser = authService.getCurrentUser();
    let splitted = this.currentUser.fullName.split(" ", 2);
    this.name = splitted[0];
    this.last_name = splitted[1];
   }

  ngOnInit() {
    console.log("onInit");
    this.showXonomy();
  }

  showXonomy() {
    let element = document.getElementById("requestEditor");
    let xmlString = `<zahtev xmlns= "http://www.euprava.sluzbenik.gov.rs/request" xmlns:pred= "http://www.euprava.sluzbenik.gov.rs/rdf/examples/predicate/">
    <podaci_o_organu>
        <naziv_organa property="pred:naziv_organa"></naziv_organa>
        <sediste_organa property="pred:sediste_organa"></sediste_organa>
    </podaci_o_organu>
    <zahtev_na_osnovu>
        <broj_clana>15</broj_clana>
        <stav>1</stav>
        <zakon> Закона о слободном приступу информацијама од јавног значаја („Службени гласник РС“, бр. 120/04, 54/07, 104/09 и 36/10)</zakon>
    </zahtev_na_osnovu>
    <elementiZahteva>
        <uvid_u_dokument>true</uvid_u_dokument>
        <posedovanje_informacije>true</posedovanje_informacije>
        <kopija_dokumenta>true</kopija_dokumenta>
        <dostavljanje_kopije_dokumenta>
            <postom>

            </postom>
            <elektronskom_postom>

            </elektronskom_postom>
            <faksom>

            </faksom>
            <drugi_nacin>

            </drugi_nacin>
        </dostavljanje_kopije_dokumenta>
    </elementiZahteva>
    <informacije_na_koje_se_odnosi_zahtev></informacije_na_koje_se_odnosi_zahtev>
    <informacije_o_traziocu>
        <trazioc>
            <fizicko_lice>
                <ime property="pred:podnosilac_ime">${this.name}</ime>
                <prezime property="pred:podnosilac_prezime">${this.last_name}</prezime>
            </fizicko_lice>
            <pravno_lice/>
        </trazioc>
        <adresa>
            <ulica></ulica>
            <broj></broj>
            <grad></grad>
        </adresa>
        <drugi_podaci_za_kontakt></drugi_podaci_za_kontakt>
    </informacije_o_traziocu>
    </zahtev>`;
    console.log(xmlString);
    let specification = this.xonomyService.requestSpecification;
    Xonomy.setMode("laic");
    Xonomy.render(xmlString, element, specification);
  }

  createRequest(){
    let data : any = Xonomy.harvest();
    this.requestService.createRequest(data).subscribe(() => {
      this.notificationService.openSnackBar("Uspešno ste poslali zahtev!");
      this.router.navigate(['home/requests']);
    })
  }

}
