import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './components/register/register.component';
import { LogInComponent } from './components/log-in/log-in.component';
import { HeaderComponent } from './components/header/header.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './angular-material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LogInService } from './services/log-in/log-in.service';
import { AuthService } from './services/auth/auth.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { InterceptorService } from './services/interceptor.service';
import { AuthGuard } from './guards/auth.guard';
import { LoggedInGuard } from './guards/logged-in.guard';
import { OptionsComponent } from './components/options/options.component';
import { RequestsComponent } from './components/requests/requests.component';
import { NoticesComponent } from './components/notices/notices.component';
import { CreateRequestComponent } from './components/create-request/create-request.component';
import { CreateNoticeComponent } from './components/create-notice/create-notice.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { Display_searchComponent } from './components/display_search/display_search.component';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LogInComponent,
    HeaderComponent,
    RequestsComponent,
    OptionsComponent,
    NoticesComponent,
    CreateRequestComponent,
    CreateNoticeComponent,
    DashboardComponent,
    Display_searchComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,

  ],
  providers: [
    {
      provide:HTTP_INTERCEPTORS,
      useClass:InterceptorService,
      multi:true
    },
    { provide: 'LOCALSTORAGE', useValue: window.localStorage },
    AuthGuard, LoggedInGuard
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
