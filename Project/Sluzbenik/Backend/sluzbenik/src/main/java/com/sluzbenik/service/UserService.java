package com.sluzbenik.service;

import javax.xml.bind.JAXBException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xmldb.api.base.XMLDBException;

import com.sluzbenik.model.user.User;
import com.sluzbenik.repos.UserRepo;

@Service
public class UserService {
	
	@Autowired
	private UserRepo userRepo;
	
	public void create(String user, String email) throws Exception {
		userRepo.create(user);
	}
	
	public User findByEmail(String email) throws JAXBException, XMLDBException {
		return userRepo.findByEmail(email);	
	}
	

}