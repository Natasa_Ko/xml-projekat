package com.sluzbenik.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;
import org.xmldb.api.base.XMLDBException;

import com.sluzbenik.database.FusekiManager;
import com.sluzbenik.model.notice.BrojPredmeta;
import com.sluzbenik.model.notice.Obavestenje;
import com.sluzbenik.model.notice.PodaciOPrimaocu;
import com.sluzbenik.model.request.Zahtev;
import com.sluzbenik.repos.NoticeRepo;
import com.sluzbenik.util.Constants;
import com.sluzbenik.util.MetadataExtractor;
import com.sluzbenik.util.NoticeTransform;
import com.sluzbenik.util.Person;

@Service
public class NoticeService {
	@Autowired
	private NoticeRepo noticeRepo;
	
	@Autowired
	private RequestService requestService;
	
	@Autowired
	FusekiManager fusekiManager;
	
	private final String TARGET_NAMESPACE = "http://www.euprava.sluzbenik.gov.rs/notice";
	private JAXBContext context;
	private Unmarshaller unmarshaller;
	private SchemaFactory schemaFactory;
	private Schema schema;
	private String newId;
	
	public List<Obavestenje> findAll() throws XMLDBException, JAXBException{
		return noticeRepo.findAll();
	}
	
	public List<Obavestenje> findAllByUserEmail(String email) throws XMLDBException, JAXBException{
		return noticeRepo.findAllByUserEmail(email);
	}
	
	public void create(String obavestenje) throws Exception {
		getNextId();
		
		obavestenje = obavestenje.replace("xml:space='preserve'", "");
		context = JAXBContext.newInstance("com.sluzbenik.model.notice");
		unmarshaller = context.createUnmarshaller();
		schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		schema = schemaFactory.newSchema(new File(Constants.OBAVESTENJE_XSD_LOCATION));
		unmarshaller.setSchema(schema);
		

		XMLStreamReader reader = XMLInputFactory.newInstance().createXMLStreamReader(new StringReader(obavestenje));
		
		JAXBElement<Obavestenje> obavestenjeObj =  unmarshaller.unmarshal(reader, Obavestenje.class);
		
		Obavestenje o = obavestenjeObj.getValue();
		//update-ujemo id zahteva i postavljamo ga da je odobren
		String updatedIdZahteva = requestService.updateId(o.getIdZahteva(), "ZAO_");
		Zahtev request = requestService.findById(updatedIdZahteva);
		o.setAbout(TARGET_NAMESPACE+"/"+newId);
		o.setVocab("http://www.euprava.sluzbenik.gov.rs/rdf/examples/predicate/");
		System.err.println("NOTICE SERVICE: NEW NOTICE ID -> "+ newId);
		o.setId(newId);
		
		Date date = new Date();
		DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		
		o.getPodaciOPrimaocu().getDatumPodnosenja().setValue(formatter.format(date).toString());
		PodaciOPrimaocu p = o.getPodaciOPrimaocu();
		BrojPredmeta br = new BrojPredmeta();
		br.setValue(newId);
		p.setBrojPredmeta(br);
	//	o.setPodaciOPrimaocu(BrojPredmeta::setValue(newId));
		System.err.println("NOTICE SERVICE: NEW REQUEST ID -> "+ updatedIdZahteva);
		o.setIdZahteva(updatedIdZahteva);
		o.setPodnosilac(request.getPodnosilac());
	//	z.setPodnosilac(((Person) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getEmail());
		
		Marshaller marshaller = context.createMarshaller();
		StringWriter stringWriter = new StringWriter();
		marshaller.marshal(o, stringWriter);
		
		String tst = stringWriter.toString().substring(55); //smacinje <?xml.... sa pocetka , damo ze da se upise

		System.err.println("NOTICE SERVICE: Obavestenje -> "+ o.toString());
		noticeRepo.create(o, tst);
			
	}
	
	private void getNextId() throws XMLDBException, JAXBException {
		List<Obavestenje> obavestenja = findAll();
		obavestenja.sort(Comparator.comparing(Obavestenje::getId));
		newId = "OB_"+ (Integer.parseInt(obavestenja.get(obavestenja.size()-1).getId().split("_")[1])+1);
	}

	public File exportAsPDF(String id) throws XMLDBException, SAXException, IOException, TransformerException {
		String zalba = findById(id);
		
		NoticeTransform nt = new NoticeTransform();
		String file = String.format("src/main/resources/static/documents/obavestenje-%s.pdf", id);
		nt.makePDF(this.removeNamespace(zalba), file);
		
		return new File(file);
	}
	
	public File exportAsHTML(String id) throws XMLDBException, ParserConfigurationException, SAXException, IOException, TransformerException {
		String zalba = findById(id);
		
		NoticeTransform ast = new NoticeTransform();
		String file = String.format("src/main/resources/static/documents/obavestenje-%s.html", id);
		ast.makeHTML(this.removeNamespace(zalba), file);
		
		return new File(file);
	}
	
	public File exportMetadataAsJSON(String id) throws IOException {
		
		String file = String.format("src/main/resources/static/documents/obavestenje-%s.JSON", id);
		fusekiManager.makeJSONForNotice(id, file);
		
		return new File(file);
	}
	
	public File exportMetadataAsRDF(String id) throws SAXException, IOException, XMLDBException {
		String file = String.format("src/main/resources/static/documents/obavestenje-%s.RDF", id);
		String fileXml = String.format("src/main/resources/static/documents/obavestenje-%s.xml", id);
		MetadataExtractor metadataExtractor = new MetadataExtractor();
		
		String zalba = removeNamespace(findById(id));
	//	System.err.println("NOTICE SERVICE: RDF zalba -> " + zalba);
		
		String output = zalba.substring(0, 50) + 
				" xmlns:pred=\"http://www.euprava.poverenik.gov.rs/rdf/examples/predicate/\" "+
				zalba.substring(51);
				
		FileWriter fw = new FileWriter(fileXml);
		fw.write(output);
		fw.close();
		
		try {
			metadataExtractor.extractMetadata(new FileInputStream(new File(fileXml)),
					new FileOutputStream(new File(file)));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return new File(file);
	}
	
	public String findById(String id) throws XMLDBException {
		return noticeRepo.findById(id);
	}
	
	public String removeNamespace(String xml) {
		 try{
	       TransformerFactory factory = TransformerFactory.newInstance();
	        Source xslt = new StreamSource(new File(Constants.NS_REMOVER));
	        Transformer transformer = factory.newTransformer(xslt);
	        StringReader r = new StringReader(xml);
	        StreamSource text = new StreamSource(r);
	        
	        StringWriter writer = new StringWriter();
	        
	        
	        transformer.transform(text, new StreamResult(writer));
	        System.out.println("Done");
	        return writer.toString();
	        } catch (TransformerConfigurationException e) {
	           return "";
	        } catch (TransformerException e) {
	        	return "";
	        }
	
  }
}
