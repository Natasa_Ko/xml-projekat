package com.sluzbenik.repos;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.exist.xupdate.XUpdateProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.xmldb.api.base.ResourceIterator;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.XMLResource;

import com.sluzbenik.database.ExistManager;
import com.sluzbenik.database.FusekiManager;
import com.sluzbenik.model.notice.Obavestenje;
import com.sluzbenik.model.request.Zahtev;
import com.sluzbenik.util.Constants;

@Repository
public class NoticeRepo {
	
	@Autowired
	private ExistManager existManager;
	@Autowired
	private FusekiManager fusekiManager;
	
	private String query;
	private JAXBContext context;
	private Unmarshaller unmarshaller;
	private ResourceIterator it;
	List<Obavestenje> notices;
	
	private final String TARGET_NAMESPACE = "http://www.euprava.sluzbenik.gov.rs/notice";
	private final String APPEND = "<xu:modifications version=\"1.0\" xmlns:xu=\"" + XUpdateProcessor.XUPDATE_NS
			+ "\" xmlns=\"" + TARGET_NAMESPACE + "\">" + "<xu:append select=\"%1$s\" child=\"last()\">%2$s</xu:append>"
			+ "</xu:modifications>";

	
	
	public List<Obavestenje> findAll() throws XMLDBException, JAXBException{
		
//		query = String.format("/zahtevi/zahtev[@podnosilac='%s']", email);
		query = String.format("/obavestenja/*");

									
		
		ResourceSet set = this.existManager.retrieve(Constants.COLLECTION_URI, query, TARGET_NAMESPACE);
		
		if(set.getSize() != 0) {
			
			notices = new ArrayList<Obavestenje>();
			it = set.getIterator();
			context = JAXBContext.newInstance("com.sluzbenik.model.notice");
			unmarshaller = context.createUnmarshaller();
			Obavestenje z = null;
			for (int i = 0; i < set.getSize(); i++) {
				z = (Obavestenje) unmarshaller.unmarshal(((XMLResource)set.getResource(i)).getContentAsDOM());
				notices.add(z);
			}
			 
			return notices;	
		}
		else {
			return null;
		}
		
	}
	
public List<Obavestenje> findAllByUserEmail(String email) throws XMLDBException, JAXBException{
		
		query = String.format("/obavestenja/obavestenje[@podnosilac='%s']", email);
	
					
		
		ResourceSet set = this.existManager.retrieve(Constants.COLLECTION_URI, query, TARGET_NAMESPACE);
		
		if(set.getSize() != 0) {
			
			notices = new ArrayList<Obavestenje>();
			context = JAXBContext.newInstance("com.sluzbenik.model.notice");
			unmarshaller = context.createUnmarshaller();
			Obavestenje o = null;
			for (int i = 0; i < set.getSize(); i++) {
				o = (Obavestenje) unmarshaller.unmarshal(((XMLResource)set.getResource(i)).getContentAsDOM());
				notices.add(o);
			}
			 
			return notices;	
		}
		else {
			return null;
		}
		
	}

		public void create(Obavestenje obavestenjeObj, String obavestenje) throws Exception {
			existManager.add(Constants.COLLECTION_URI,
							 Constants.OBAVESTENJE_ID,
							 "/obavestenja",
							 obavestenje,
							 APPEND);
			fusekiManager.addNotice(obavestenjeObj);
			
		}
		
		public String findById(String id) throws XMLDBException {
			query = String.format("/obavestenja/obavestenje[@id='%s']", id);
			
			ResourceSet set = this.existManager.retrieve(Constants.COLLECTION_URI, query, TARGET_NAMESPACE);
			
			if(set.getSize() !=0) {
				
				return set.getResource(0).getContent().toString();
			}else {
				return null;
			}
			
			
		}

}
