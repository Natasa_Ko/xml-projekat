package com.sluzbenik.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.xml.sax.SAXException;
import org.xmldb.api.base.XMLDBException;

import com.sluzbenik.model.notice.Obavestenje;
import com.sluzbenik.model.request.Zahtev;
import com.sluzbenik.service.NoticeService;
import com.sluzbenik.util.Person;


@RestController
@CrossOrigin(origins= "http://localhost:4200")
@RequestMapping(value = "/notices",
				produces = MediaType.APPLICATION_XML_VALUE)
public class NoticeController {
	
	@Autowired
	NoticeService noticeService;
	
	@PreAuthorize("hasRole('ROLE_SERVANT')")
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Obavestenje>> getAll() throws XMLDBException, JAXBException {
		
		System.out.println("NOTICE CONTROLLER: getAll");
		System.out.println(SecurityContextHolder.getContext().getAuthentication().getPrincipal());
		
		List<Obavestenje> requests = noticeService.findAll();
		
		if(requests!=null) {
			return new ResponseEntity<List<Obavestenje>>(requests, HttpStatus.OK);
		}else {
			System.out.println("Nema obavestenja ");
			return new ResponseEntity<List<Obavestenje>>(requests, HttpStatus.NOT_FOUND);
		}

	}
	
	@PreAuthorize("hasRole('ROLE_CITIZEN')")
	@RequestMapping(value = "/user",method = RequestMethod.GET)
	public ResponseEntity<List<Obavestenje>> getAllForUser() throws XMLDBException, JAXBException {
		
		List<Obavestenje> requests = noticeService.findAllByUserEmail(((Person) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getEmail());
		
		if(requests!=null) {
			return new ResponseEntity<List<Obavestenje>>(requests, HttpStatus.OK);
		}else {
			System.out.println("Nema obavestenja ");
			return new ResponseEntity<List<Obavestenje>>(requests, HttpStatus.NOT_FOUND);
		}

	}
	
	@PreAuthorize("hasRole('ROLE_SERVANT')")
	@RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<?> create(@RequestBody String obavestenje) {
		
		
		try {
			this.noticeService.create(obavestenje);
			return new ResponseEntity<>(null, HttpStatus.CREATED);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
				
	}

//	@PreAuthorize("hasRole('ROLE_SERVANT')")
	@RequestMapping(path = "/export/{id}/{type}", method = RequestMethod.GET, produces = MediaType.APPLICATION_PDF_VALUE)
	public ResponseEntity<byte[]> exportAs(@PathVariable("id") String id, @PathVariable("type") String type) throws IOException, XMLDBException, SAXException, TransformerException, ParserConfigurationException{
		
		switch (type) {
		case "PDF":

			System.out.println("id" + id + type);
			return new ResponseEntity<byte[]>(Files.readAllBytes(noticeService.exportAsPDF(id).toPath()), HttpStatus.OK);
		case "HTML":
			return new ResponseEntity<byte[]>(Files.readAllBytes(noticeService.exportAsHTML(id).toPath()), HttpStatus.OK);
		case "JSON":
			return new ResponseEntity<byte[]>(Files.readAllBytes(noticeService.exportMetadataAsJSON(id).toPath()), HttpStatus.OK);
		case "RDF":
			return new ResponseEntity<byte[]>(Files.readAllBytes(noticeService.exportMetadataAsRDF(id).toPath()), HttpStatus.OK);
		default:
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
		
	}
}
