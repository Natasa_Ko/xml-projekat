package com.sluzbenik.util;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class RequestTransform {
	private FopFactory fopFactory;
	private TransformerFactory transformerFactory;
	private DocumentBuilderFactory documentFactory;

	public void makePDF(String xml, String _file) throws SAXException, IOException, TransformerException {

		transformerFactory = TransformerFactory.newInstance();
		fopFactory = FopFactory.newInstance(new File("src/main/java/fop.xconf"));
		
		StreamSource streamSourceFile = new StreamSource(new File(Constants.ZAHTEVCIR_XSL_LOCATION));
		StreamSource streamSourceXML = new StreamSource(new StringReader(xml));
	
		FOUserAgent userAgent = fopFactory.newFOUserAgent();
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();

		Transformer xslFoTransformer = transformerFactory.newTransformer(streamSourceFile);

		

		Result res = new SAXResult(this.fopFactory.newFop(MimeConstants.MIME_PDF, userAgent, outStream).getDefaultHandler());
		xslFoTransformer.transform(streamSourceXML, res);
		OutputStream out = new BufferedOutputStream(new FileOutputStream(new File(_file)));
		out.write(outStream.toByteArray());
		out.close();
	}


	public void makeHTML(String xml, String _file) throws ParserConfigurationException, TransformerException, SAXException, IOException {
			StreamSource transformSource = new StreamSource(new File(Constants.ZAHTEVCIR_XHTML_LOCATION));
			Transformer transformer = transformerFactory.newTransformer(transformSource);
			transformer.setOutputProperty("{http://xml.apache.org/xalan}indent-amount", "2");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.METHOD, "xhtml");

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			DOMSource source = new DOMSource(builder.parse(new InputSource(new StringReader(xml))));
			StreamResult result = new StreamResult(new FileOutputStream(_file));
			transformer.transform(source, result);
	}
	
	

	public RequestTransform() throws SAXException, IOException {
		documentFactory = DocumentBuilderFactory.newInstance();
		documentFactory.setNamespaceAware(true);
		documentFactory.setIgnoringComments(true);
		documentFactory.setIgnoringElementContentWhitespace(true);
		transformerFactory = TransformerFactory.newInstance();
		fopFactory = FopFactory.newInstance(new File("src/main/java/fop.xconf"));
	}

}
