package com.sluzbenik.service;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;
import org.xmldb.api.base.XMLDBException;

import com.sluzbenik.database.FusekiManager;
import com.sluzbenik.model.request.Zahtev;
import com.sluzbenik.repos.RequestRepo;
import com.sluzbenik.util.Constants;
import com.sluzbenik.util.MetadataExtractor;
import com.sluzbenik.util.NoticeTransform;
import com.sluzbenik.util.Person;
import com.sluzbenik.util.RequestTransform;

@Service
public class RequestService {
	
	private final String TARGET_NAMESPACE = "http://www.euprava.sluzbenik.gov.rs/request";
	private JAXBContext context;
	private Unmarshaller unmarshaller;
	private StringReader stringReader;
	private SchemaFactory schemaFactory;
	private Schema schema;
	
	
	private String newId;


	@Autowired
	private RequestRepo requestRepo;
	
	@Autowired
	FusekiManager fusekiManager;
	
	public List<Zahtev> findAll() throws XMLDBException, JAXBException{
		return requestRepo.findAll();
	}
	
	public List<Zahtev> findAllByUserEmail(String email) throws XMLDBException, JAXBException{
		return requestRepo.findAllByUserEmail(email);
	}
	
	public List<Zahtev> findAllByIdContains(String id) throws XMLDBException, JAXBException{
		return requestRepo.findAllByContainsId(id);
	}
	
	public Zahtev findById(String id) throws XMLDBException, JAXBException{
		return requestRepo.findById(id);
	}
	
	public void create(String zahtev) throws Exception {
		getNextId();
		zahtev = zahtev.replace("xml:space='preserve'", "");
		
		context = JAXBContext.newInstance("com.sluzbenik.model.request");
		unmarshaller = context.createUnmarshaller();
		schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		schema = schemaFactory.newSchema(new File(Constants.ZAHTEVCIR_XSD_LOCATION));
		unmarshaller.setSchema(schema);
		

		XMLStreamReader reader = XMLInputFactory.newInstance().createXMLStreamReader(new StringReader(zahtev));
			
		JAXBElement<Zahtev> zahtevObj =  unmarshaller.unmarshal(reader, Zahtev.class);
		
		Zahtev z = zahtevObj.getValue();
		z.setAbout(TARGET_NAMESPACE+"/"+newId);
		z.setVocab("http://www.euprava.sluzbenik.gov.rs/rdf/examples/predicate/");
		z.setId(newId);
		
		Date date = new Date();
		GregorianCalendar c = new GregorianCalendar();
		c.setTime(date);
		XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
		z.setDatum(date2);
		z.setMesto(z.getInformacijeOTraziocu().getAdresa().getGrad());
		z.setPodnosilac(((Person) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getEmail());
		
		Marshaller marshaller = context.createMarshaller();
		StringWriter stringWriter = new StringWriter();
		marshaller.marshal(z, stringWriter);
		
		String tst = stringWriter.toString().substring(55); //smacinje <?xml.... sa pocetka , damo ze da se upise

		
		requestRepo.create(z, tst);
			
	}
	
	private void getNextId() throws XMLDBException, JAXBException {
		List<Zahtev> zahtevi = findAll();
		zahtevi.sort(Comparator.comparing(Zahtev::getId));
		//newId = "ZA_"+ (Integer.parseInt(zahtevi.get(zahtevi.size()-1).getId().split("_")[1])+1);
		int number = zahtevi.size() + 1;
		newId = "ZA_"+ number;
	}
	
//	private String getNextIdDeclined() throws XMLDBException, JAXBException {
//		List<Zahtev> zahtevi = findAllByIdContains("ZAN_");
//		if(zahtevi != null) {
//			zahtevi.sort(Comparator.comparing(Zahtev::getId));
//			return "ZAN_"+ (Integer.parseInt(zahtevi.get(zahtevi.size()-1).getId().split("_")[1])+1);
//		}else {
//			return "ZAN_1";
//		}
//		
//	}
//	
//	private String getNextIdAccepted() throws XMLDBException, JAXBException {
//		List<Zahtev> zahtevi = findAllByIdContains("ZAO_");
//		if(zahtevi != null) {
//			zahtevi.sort(Comparator.comparing(Zahtev::getId));
//			return "ZAO_"+ (Integer.parseInt(zahtevi.get(zahtevi.size()-1).getId().split("_")[1])+1);
//		}else {
//			return "ZAO_1";
//		}
//		
//	}
	
	public String updateId(String id, String change) throws Exception {
		
		String oldId = id;
		String updatedId = oldId.replace("ZA_", change);
		
		Zahtev request = requestRepo.findById(id);
		
		List<Zahtev> requests = requestRepo.findAll();
		int index = 0;
		for(Zahtev req : requests) {
			if(req.getId().equals(request.getId())) {
				index = requests.indexOf(req) + 1;
			}
		}
		
		String url = request.getAbout().substring(0, request.getAbout().lastIndexOf("/")) + "/" + updatedId;
		System.out.println("URL " + url);
		
		request.setId(updatedId);
		request.setAbout(url);
		
		String root = String.format("/zahtevi/zahtev[%d]/@id", index);
		System.out.println(root);
		requestRepo.update(request, updatedId, root);
		
		root = String.format("/zahtevi/zahtev[%d]/@about", index);
		System.out.println(root);
		requestRepo.update(request, url, root);
		return updatedId;
			
	}
	
	
	public File exportAsPDF(String id) throws XMLDBException, SAXException, IOException, TransformerException {
		String zalba = findByIdString(id);
		
		RequestTransform nt = new RequestTransform();
		String file = String.format("src/main/resources/static/documents/zahtev-%s.pdf", id);
		nt.makePDF(this.removeNamespace(zalba), file);
		
		return new File(file);
	}
	
	public String findByIdString(String id) throws XMLDBException {
		return requestRepo.findByIdString(id);
	}
	
	public File exportMetadataAsJSON(String id) throws IOException {
		
		String file = String.format("src/main/resources/static/documents/zahtev-%s.JSON", id);
		fusekiManager.makeJSONForRequest(id, file);
		
		return new File(file);
	}
	
	public File exportMetadataAsRDF(String id) throws SAXException, IOException, XMLDBException {
		String file = String.format("src/main/resources/static/documents/zahtev-%s.RDF", id);
		String fileXml = String.format("src/main/resources/static/documents/zahtev-%s.xml", id);
		MetadataExtractor metadataExtractor = new MetadataExtractor();
		
		String zalba = removeNamespace(findByIdString(id));
		System.err.println("NOTICE SERVICE: RDF zalba -> " + zalba);
		
		String output = zalba.substring(0, 46) + 
				" xmlns:pred=\"http://www.euprava.poverenik.gov.rs/rdf/examples/predicate/\" "+
				zalba.substring(47);
				
		FileWriter fw = new FileWriter(fileXml);
		fw.write(output);
		fw.close();
		
		try {
			metadataExtractor.extractMetadata(new FileInputStream(new File(fileXml)),
					new FileOutputStream(new File(file)));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return new File(file);
	}
	public String removeNamespace(String xml) {
		 try{
	       TransformerFactory factory = TransformerFactory.newInstance();
	        Source xslt = new StreamSource(new File(Constants.NS_REMOVER));
	        Transformer transformer = factory.newTransformer(xslt);
	        StringReader r = new StringReader(xml);
	        StreamSource text = new StreamSource(r);
	        
	        StringWriter writer = new StringWriter();
	        
	        
	        transformer.transform(text, new StreamResult(writer));
	        System.out.println("Done");
	        return writer.toString();
	        } catch (TransformerConfigurationException e) {
	           return "";
	        } catch (TransformerException e) {
	        	return "";
	        }
	
 }
	public List<Zahtev> findAllByContains(String text) throws XMLDBException, JAXBException {
		
		return requestRepo.findAllByContains(text);
	}
	
	public File exportAsHTML(String id) throws XMLDBException, ParserConfigurationException, SAXException, IOException, TransformerException {
		String zalba = findByIdString(id);
		
		RequestTransform ast = new RequestTransform();
		String file = String.format("src/main/resources/static/documents/zahtev-%s.html", id);
		ast.makeHTML(this.removeNamespace(zalba), file);
		
		return new File(file);
	}
	
	
}