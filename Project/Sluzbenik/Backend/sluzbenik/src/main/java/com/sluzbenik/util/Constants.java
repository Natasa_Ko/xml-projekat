package com.sluzbenik.util;

public class Constants {

	public static final String COLLECTION_URI= "/db/documentsSluzbenik";
	public static final String PREDICATE_NAMESPACE = "http://localhost:8080/rdf/examples/predicate/";										   
	public static final String NS_REMOVER = "src/main/resources/static/documents/removeNs.xslt"; 

	
	//korisnici
	
	public static final String KORISNICI_ID = "users.xml";
	public static final String KORISNICI_LOCATION = "src/main/resources/static/documents/user.xml";
	
	//obavestenje
	public static final String OBAVESTENJE_ID = "notice.xml";
	public static final String OBAVESTENJE_LOCATION = "src/main/resources/static/documents/notice.xml";
	public static final String OBAVESTENJE_RDF_LOCATION = "src/main/resources/static/documents/notice.rdf"; //tu ce napraviti rdf
	public static final String OBAVESTENJE_XSD_LOCATION = "src/main/resources/static/documents/notice.xsd";
	public static final String OBAVESTENJE_SPARQL_NAMED_GRAPH_URI = "/notice";
	public static final String OBAVESTENJE_XSL_LOCATION = "src/main/resources/static/documents/notice_fo.xsl";
	public static final String OBAVESTENJE_XHTML_LOCATION= "src/main/resources/static/documents/notice.xsl";

	
	//zalba na odluku
	public static final String ZALBAODLUKA_ID = "zalbanaodlukucir.xml";
	public static final String ZALBAODLUKA_LOCATION = "documents/zalbanaodlukucir.xml";
	public static final String ZALBAODLUKA_RDF_LOCATION = "documents/zalbanaodlukucir.rdf"; //tu ce napraviti rdf
	public static final String ZALBAODLUKA_SPARQL_NAMED_GRAPH_URI = "/zalbanaodluku";
	
	
	//zahtjev za informacije od javnog znacaja
	public static final String ZAHTEVCIR_ID = "requests.xml";
	public static final String ZAHTEVCIR_LOCATION = "src/main/resources/static/documents/request.xml";
	public static final String ZAHTEVCIR_RDF_LOCATION = "src/main/resources/static/documents/request.rdf"; //tu ce napraviti rdf
	public static final String ZAHTEVCIR_XSD_LOCATION = "src/main/resources/static/documents/request.xsd";
	public static final String ZAHTEVCIR_SPARQL_NAMED_GRAPH_URI = "/request";

	public static final String ZAHTEVCIR_XSL_LOCATION = "src/main/resources/static/documents/request_fop.xsl";
	public static final String ZAHTEVCIR_XHTML_LOCATION= "src/main/resources/static/documents/request.xsl";

	

	//zalba na cutanje
	public static final String ZALBACUTANJE_ID = "zalbacutanjecir.xml";
	public static final String ZALBACUTANJE_LOCATION = "documents/zalbacutanjecir.xml";
	public static final String ZALBACUTANJE_RDF_LOCATION = "documents/zalbacutanjecir.rdf"; //tu ce napraviti rdf
	public static final String ZALBACUTANJE_SPARQL_NAMED_GRAPH_URI = "/zalbacutanjecir";
	
	//resenje
	public static final String RESENJE_ID = "resenje.xml";
	public static final String RESENJE_LOCATION = "documents/resenje.xml";
	public static final String RESENJE_RDF_LOCATION = "documents/resenje.rdf"; //tu ce napraviti rdf
	public static final String RESENJE_SPARQL_NAMED_GRAPH_URI = "/resenje";
	
	
	
}