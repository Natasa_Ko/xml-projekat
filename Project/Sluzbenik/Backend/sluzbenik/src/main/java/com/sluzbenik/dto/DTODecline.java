package com.sluzbenik.dto;

public class DTODecline {
	
	private String id;
	
	public DTODecline() {
		super();
	}

	public DTODecline(String id) {
		super();
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
