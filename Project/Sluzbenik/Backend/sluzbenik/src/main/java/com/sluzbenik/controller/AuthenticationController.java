package com.sluzbenik.controller;

import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.xmldb.api.base.XMLDBException;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.sluzbenik.dto.DTOUser;
import com.sluzbenik.dto.DTOUserLogin;
import com.sluzbenik.dto.DTOUserSignUp;
import com.sluzbenik.dto.DTOUserTokenState;
import com.sluzbenik.model.user.User;
import com.sluzbenik.sec.TokenUtils;
import com.sluzbenik.service.UserService;
import com.sluzbenik.util.Person;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "/auth",
				produces = MediaType.APPLICATION_XML_VALUE,
				consumes = MediaType.APPLICATION_XML_VALUE)
public class AuthenticationController {
	
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private TokenUtils tokenUtils;
	 
	
	@Autowired
    private AuthenticationManager authenticationManager;
	
	private BCryptPasswordEncoder encoder;
	private XmlMapper mapper;
	
	
	@PostMapping("/sign-up")
    public ResponseEntity<?> addUser(@RequestBody DTOUserSignUp userRequest) throws Exception {
		System.out.println("USOOOo");
        User existUser = this.userService.findByEmail(userRequest.getEmail());
        
        if (existUser != null) {
        	 return new ResponseEntity<>("Email already exists", HttpStatus.NOT_ACCEPTABLE);
        }

        try {
        	encoder = new BCryptPasswordEncoder();
        	mapper = new XmlMapper();
        	System.out.println(userRequest.getPassword() + " ==PASSWORD");
        	DTOUser dtoUser = new DTOUser(userRequest.getFirst_name(),
        								  userRequest.getLast_name(),
        								  userRequest.getEmail(),
        								  encoder.encode(userRequest.getPassword()),
        								  "ROLE_CITIZEN");
        	
        	this.userService.create(mapper.writeValueAsString(dtoUser).replace("DTOUser", "user"), userRequest.getEmail());
        	
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
        
        
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
	
	@PostMapping("/log-in")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody DTOUserLogin authenticationRequest,
                                                                    HttpServletResponse response) throws JAXBException, XMLDBException {
		
		Authentication authentication = null;
		try {
			authentication = authenticationManager
	                .authenticate(new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(),
	                        authenticationRequest.getPassword()));
		}catch (BadCredentialsException e) {
			Object o = "Sorry, user doesn't exist :(";
			return new ResponseEntity<>(o,HttpStatus.BAD_REQUEST);
		}
        
		
  
        // Ubaci korisnika u trenutni security kontekst
        SecurityContextHolder.getContext().setAuthentication(authentication);
        // Kreiraj token za tog korisnika
        System.err.println("LOG IN "+authentication.getPrincipal().toString());
        	
        User user = userService.findByEmail( ((Person)authentication.getPrincipal()).getEmail() );
        
        String fullName = user.getFirstName()+" "+user.getLastName();


        // Kreiraj token za tog korisnika
        
        System.out.println(authentication.getAuthorities());
     
        String jwt = tokenUtils.generateToken(user.getEmail(), user.getRole(), fullName); // prijavljujemo se na sistem sa email adresom
        int expiresIn = tokenUtils.getExpiredIn();
            
        System.out.println("USER = "+SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    		
        // Vrati token kao odgovor na uspesnu autentifikaciju
		return ResponseEntity.ok(new DTOUserTokenState(jwt, expiresIn));
 
    }

}
