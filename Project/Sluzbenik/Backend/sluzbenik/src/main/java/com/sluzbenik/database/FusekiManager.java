package com.sluzbenik.database;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.update.UpdateExecutionFactory;
import org.apache.jena.update.UpdateFactory;
import org.apache.jena.update.UpdateProcessor;
import org.apache.jena.update.UpdateRequest;
import org.springframework.stereotype.Component;

import com.sluzbenik.util.AuthenticationUtilRDF;
import com.sluzbenik.util.AuthenticationUtilRDF.ConnectionProperties;
import com.sluzbenik.util.Constants;
import com.sluzbenik.util.SparqlUtill;

@Component
public class FusekiManager {
	private final String PREDICATE_NAMESPACE = "http://www.euprava.poverenik.gov.rs/rdf/examples/predicate/";
	private ConnectionProperties conn;
	
	public void addRequest(com.sluzbenik.model.request.Zahtev zahtev) throws IOException {
		conn = AuthenticationUtilRDF.loadProperties();
		
		Model model = ModelFactory.createDefaultModel();
		model.setNsPrefix("pred", PREDICATE_NAMESPACE);
	
		// Making the changes manually 
				Resource resource = model.createResource(zahtev.getAbout());
				
				Property property1 = model.createProperty(PREDICATE_NAMESPACE, "naziv_organa");
				Literal literal1 = model.createLiteral(zahtev.getPodaciOOrganu().getNazivOrgana().getValue());
				
				Property property2 = model.createProperty(PREDICATE_NAMESPACE, "sediste_organa");
				Literal literal2 = model.createLiteral(zahtev.getPodaciOOrganu().getSedisteOrgana().getValue());
				
				Property property3 = model.createProperty(PREDICATE_NAMESPACE, "podnosialc_ime");
				Literal literal3 = model.createLiteral(zahtev.getInformacijeOTraziocu().getTrazioc().getFizickoLice().getIme().getValue());
				
				Property property4 = model.createProperty(PREDICATE_NAMESPACE, "podnosialc_prezime");
				Literal literal4 = model.createLiteral(zahtev.getInformacijeOTraziocu().getTrazioc().getFizickoLice().getPrezime().getValue());
				
				//Property property5 = model.createProperty(PREDICATE_NAMESPACE, "adresa");
				//Literal literal5 = model.createLiteral("Novi Sad Fruskogorska 12");
				
				

				// Adding the statements to the model
				Statement statement1 = model.createStatement(resource, property1, literal1);
				Statement statement2 = model.createStatement(resource, property2, literal2);
				Statement statement3 = model.createStatement(resource, property3, literal3);
				Statement statement4 = model.createStatement(resource, property4, literal4);
				//Statement statement5 = model.createStatement(resource, property5, literal5);
				
			
				model.add(statement1);
				model.add(statement2);
				model.add(statement3);
				model.add(statement4);
				//model.add(statement5);
	
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		model.write(out, SparqlUtill.NTRIPLES);
		
		String sparqlUpdate = SparqlUtill.insertData(conn.dataEndpoint + Constants.ZAHTEVCIR_SPARQL_NAMED_GRAPH_URI, new String(out.toByteArray()));
		
		UpdateRequest update = UpdateFactory.create(sparqlUpdate);
	
	    UpdateProcessor processor = UpdateExecutionFactory.createRemote(update, conn.updateEndpoint);
		processor.execute();
	
		model.close();
		
		System.out.println("[INFO] Insertion done");

	}
	
	public void addNotice(com.sluzbenik.model.notice.Obavestenje obavestenje) throws IOException {
		conn = AuthenticationUtilRDF.loadProperties();
		
		Model model = ModelFactory.createDefaultModel();
		model.setNsPrefix("pred", PREDICATE_NAMESPACE);
	
		// Making the changes manually 
				Resource resource = model.createResource(obavestenje.getAbout());
				
				Property property1 = model.createProperty(PREDICATE_NAMESPACE, "naziv_ustanove");
				Literal literal1 = model.createLiteral(obavestenje.getPodaciOPrimaocu().getNaziv().getValue());
				
				Property property2 = model.createProperty(PREDICATE_NAMESPACE, "sediste_ustanove");
				Literal literal2 = model.createLiteral(obavestenje.getPodaciOPrimaocu().getSediste().getValue());
				
				Property property3 = model.createProperty(PREDICATE_NAMESPACE, "brojpredmeta");
				Literal literal3 = model.createLiteral(obavestenje.getPodaciOPrimaocu().getBrojPredmeta().getValue());
				
				Property property4 = model.createProperty(PREDICATE_NAMESPACE, "datum_podnosenja");
				Literal literal4 = model.createLiteral(obavestenje.getPodaciOPrimaocu().getDatumPodnosenja().getValue());
			
				Property property5 = model.createProperty(PREDICATE_NAMESPACE, "ime_podnosioca");
				Literal literal5 = model.createLiteral(obavestenje.getPodaciOPodnosiocu().getImePodnosioca().getValue());
				
				Property property6 = model.createProperty(PREDICATE_NAMESPACE, "prezime_podnosioca");
				Literal literal6 = model.createLiteral(obavestenje.getPodaciOPodnosiocu().getPrezimePodnosioca().getValue());

				Property property7 = model.createProperty(PREDICATE_NAMESPACE, "naziv_podnosioca");
				Literal literal7 = model.createLiteral(obavestenje.getPodaciOPodnosiocu().getNazivPodnosioca().getValue());
		
				Property property8 = model.createProperty(PREDICATE_NAMESPACE, "adresa_podnosioca");
				Literal literal8 = model.createLiteral(obavestenje.getPodaciOPodnosiocu().getAdresaPodnosioca().getValue());
		
				Property property9 = model.createProperty(PREDICATE_NAMESPACE, "datum_zahteva");
				Literal literal9 = model.createLiteral(obavestenje.getTrazenaInformacija().getDatumZahteva().getValue());
		
				Property property10 = model.createProperty(PREDICATE_NAMESPACE, "opis_informacije");
				Literal literal10 = model.createLiteral(obavestenje.getTrazenaInformacija().getOpisTrazeneInformacije().getValue());	
				

				// Adding the statements to the model
				Statement statement1 = model.createStatement(resource, property1, literal1);
				Statement statement2 = model.createStatement(resource, property2, literal2);
				Statement statement3 = model.createStatement(resource, property3, literal3);
				Statement statement4 = model.createStatement(resource, property4, literal4);
				Statement statement5 = model.createStatement(resource, property5, literal5);
				Statement statement6 = model.createStatement(resource, property6, literal6);
				Statement statement7 = model.createStatement(resource, property7, literal7);
				Statement statement8 = model.createStatement(resource, property8, literal8);
				Statement statement9 = model.createStatement(resource, property9, literal9);
				Statement statement10 = model.createStatement(resource, property10, literal10);
				
			
				model.add(statement1);
				model.add(statement2);
				model.add(statement3);
				model.add(statement4);
				model.add(statement5);
				model.add(statement6);
				model.add(statement7);
				model.add(statement8);
				model.add(statement9);
				model.add(statement10);
	
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		model.write(out, SparqlUtill.NTRIPLES);
		
		String sparqlUpdate = SparqlUtill.insertData(conn.dataEndpoint + Constants.OBAVESTENJE_SPARQL_NAMED_GRAPH_URI, new String(out.toByteArray()));
		
		UpdateRequest update = UpdateFactory.create(sparqlUpdate);
	
	    UpdateProcessor processor = UpdateExecutionFactory.createRemote(update, conn.updateEndpoint);
		processor.execute();
	
		model.close();
		
		System.out.println("[INFO] Insertion done");

	}
	
	public void makeJSONForNotice(String id, String _file) throws IOException {
		conn = AuthenticationUtilRDF.loadProperties();
		String sparqlQuery = SparqlUtill.selectData(conn.dataEndpoint + 
				Constants.OBAVESTENJE_SPARQL_NAMED_GRAPH_URI,
				String.format("<http://www.euprava.poverenik.gov.rs/rdf/examples/notice/%s> ?p ?o", id));
		QueryExecution query = QueryExecutionFactory.sparqlService(conn.queryEndpoint, sparqlQuery);
		ResultSet results = query.execSelect();
		File rdfFile = new File(_file);
		OutputStream out = new BufferedOutputStream(new FileOutputStream(rdfFile));
		ResultSetFormatter.outputAsJSON(out, results);
		query.close();
		
	}
	
	public void makeJSONForRequest(String id, String _file) throws IOException {
		conn = AuthenticationUtilRDF.loadProperties();
		String sparqlQuery = SparqlUtill.selectData(conn.dataEndpoint + 
				Constants.ZAHTEVCIR_SPARQL_NAMED_GRAPH_URI,
				String.format("<http://www.euprava.poverenik.gov.rs/rdf/examples/request/%s> ?p ?o", id));
		QueryExecution query = QueryExecutionFactory.sparqlService(conn.queryEndpoint, sparqlQuery);
		ResultSet results = query.execSelect();
		File rdfFile = new File(_file);
		OutputStream out = new BufferedOutputStream(new FileOutputStream(rdfFile));
		ResultSetFormatter.outputAsJSON(out, results);
		query.close();
		
	}
	

}
