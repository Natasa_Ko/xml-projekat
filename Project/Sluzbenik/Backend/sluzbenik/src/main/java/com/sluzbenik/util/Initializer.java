package com.sluzbenik.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.xml.transform.TransformerException;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.update.UpdateExecutionFactory;
import org.apache.jena.update.UpdateFactory;
import org.apache.jena.update.UpdateProcessor;
import org.apache.jena.update.UpdateRequest;
import org.exist.xmldb.EXistResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.xml.sax.SAXException;
import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Database;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.CollectionManagementService;
import org.xmldb.api.modules.XMLResource;

import com.sluzbenik.util.AuthenticationUtilRDF.ConnectionProperties;

public class Initializer {
	
	private static AuthenticationUtil.ConnectionProperties conn;
	
	public void initExist() throws IOException, ClassNotFoundException, XMLDBException, InstantiationException, IllegalAccessException, SAXException, TransformerException {
		
		
		this.conn = AuthenticationUtil.loadProperties();
    	
    	Class<?> cl = Class.forName(conn.driver);
    	
    	Database database = (Database) cl.newInstance();
        database.setProperty("create-database", "true");
        
        // entry point for the API which enables you to get the Collection reference
        DatabaseManager.registerDatabase(database);
        
        // a collection of Resources stored within an XML database
        Collection col = null;
        XMLResource res = null;
        try { 
        	
        	System.out.println("[INFO] Retrieving the collection: " + Constants.COLLECTION_URI);
            col = getOrCreateCollection(Constants.COLLECTION_URI);
            
            /*
             *  create new XMLResource with a given id
             *  an id is assigned to the new resource if left empty (null)
             */
            System.out.println("[INFO] Inserting the document: " + Constants.KORISNICI_ID);
            res = (XMLResource) col.createResource(Constants.KORISNICI_ID, XMLResource.RESOURCE_TYPE);
            
            File f = new File(Constants.KORISNICI_LOCATION);
            
            if(!f.canRead()) {
                System.out.println("[ERROR] Cannot read the file: " + Constants.KORISNICI_LOCATION);
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            
            res.setContent(f);
            System.out.println("[INFO] Storing the document: " + res.getId());
            
            col.storeResource(res);
            System.out.println("[INFO] Done.");
            
            System.out.println("[INFO] Inserting the document: " + Constants.ZAHTEVCIR_ID);
            res = (XMLResource) col.createResource(Constants.ZAHTEVCIR_ID, XMLResource.RESOURCE_TYPE);
            
            File f1 = new File(Constants.ZAHTEVCIR_LOCATION);
            
            if(!f1.canRead()) {
                System.out.println("[ERROR] Cannot read the file: " + Constants.ZAHTEVCIR_LOCATION);
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            
            res.setContent(f1);
            System.out.println("[INFO] Storing the document: " + res.getId());
            
            col.storeResource(res);
            System.out.println("[INFO] Done.");
            
            //-----------------------------------

            System.out.println("[INFO] Inserting the document: " + Constants.OBAVESTENJE_ID);
            res = (XMLResource) col.createResource(Constants.OBAVESTENJE_ID, XMLResource.RESOURCE_TYPE);
            
            File f2 = new File(Constants.OBAVESTENJE_LOCATION);
            
            if(!f1.canRead()) {
                System.out.println("[ERROR] Cannot read the file: " + Constants.OBAVESTENJE_LOCATION);
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            
            res.setContent(f2);
            System.out.println("[INFO] Storing the document: " + res.getId());
            
            col.storeResource(res);
            System.out.println("[INFO] Done.");
            //-----------------------------------
     ConnectionProperties conn = AuthenticationUtilRDF.loadProperties();
            
            System.out.println("[INFO] " + Initializer.class.getSimpleName());
    		
    		// Referencing XML file with RDF data in attributes

    		
    		// Automatic extraction of RDF triples from XML file
    		MetadataExtractor metadataExtractor = new MetadataExtractor();
    		
    		System.out.println("[INFO] Extracting metadata from RDFa attributes...");
    		metadataExtractor.extractMetadata(
    				new FileInputStream(new File(Constants.ZAHTEVCIR_LOCATION)), 
    				new FileOutputStream(new File(Constants.ZAHTEVCIR_RDF_LOCATION)));
    				
    		
    		// Loading a default model with extracted metadata
    		Model model = ModelFactory.createDefaultModel();
    		model.read(Constants.ZAHTEVCIR_RDF_LOCATION);
    		
    		ByteArrayOutputStream out = new ByteArrayOutputStream();
    		
    		model.write(out, SparqlUtill.NTRIPLES);
    		
    		System.out.println("[INFO] Extracted metadata as RDF/XML...");
    		model.write(System.out, SparqlUtill.RDF_XML);

    		
    		// Writing the named graph
    		System.out.println("[INFO] Populating named graph \"" + Constants.ZAHTEVCIR_SPARQL_NAMED_GRAPH_URI + "\" with extracted metadata.");
    		String sparqlUpdate = SparqlUtill.insertData(conn.dataEndpoint + Constants.ZAHTEVCIR_SPARQL_NAMED_GRAPH_URI, new String(out.toByteArray()));
    		System.out.println(sparqlUpdate);
    		
    		// UpdateRequest represents a unit of execution
    		UpdateRequest update = UpdateFactory.create(sparqlUpdate);

    		UpdateProcessor processor = UpdateExecutionFactory.createRemote(update, conn.updateEndpoint);
    		processor.execute();
    		System.out.println("[INFO] Done.");
    		
   //-------------------- RDF OBAVESTENJA
                  		
     		// Referencing XML file with RDF data in attributes

     		
     		// Automatic extraction of RDF triples from XML file
     		
     		System.out.println("[INFO] Extracting metadata from RDFa attributes...");
     		metadataExtractor.extractMetadata(
     				new FileInputStream(new File(Constants.OBAVESTENJE_LOCATION)), 
     				new FileOutputStream(new File(Constants.OBAVESTENJE_RDF_LOCATION)));
     				
     		
     		// Loading a default model with extracted metadata
     		model = ModelFactory.createDefaultModel();
     		model.read(Constants.OBAVESTENJE_RDF_LOCATION);
     		
     		out = new ByteArrayOutputStream();
     		
     		model.write(out, SparqlUtill.NTRIPLES);
     		
     		System.out.println("[INFO] Extracted metadata as RDF/XML...");
     		model.write(System.out, SparqlUtill.RDF_XML);

     		
     		// Writing the named graph
     		System.out.println("[INFO] Populating named graph \"" + Constants.OBAVESTENJE_SPARQL_NAMED_GRAPH_URI + "\" with extracted metadata.");
     		sparqlUpdate = SparqlUtill.insertData(conn.dataEndpoint + Constants.OBAVESTENJE_SPARQL_NAMED_GRAPH_URI, new String(out.toByteArray()));
     		System.out.println(sparqlUpdate);
     		
     		// UpdateRequest represents a unit of execution
     		update = UpdateFactory.create(sparqlUpdate);

     		processor = UpdateExecutionFactory.createRemote(update, conn.updateEndpoint);
     		processor.execute();
     		System.out.println("[INFO] Done.");

                 
        }
        finally {
        	//don't forget to cleanup
            if(res != null) {
                try { 
                	((EXistResource)res).freeResources(); 
                } catch (XMLDBException xe) {
                	xe.printStackTrace();
                }
            }
            
            if(col != null) {
                try { 
                	col.close(); 
                } catch (XMLDBException xe) {
                	xe.printStackTrace();
                }
            }

		}

	}
	
	private static Collection getOrCreateCollection(String collectionUri) throws XMLDBException {
        return getOrCreateCollection(collectionUri, 0);
    }

	
	private static Collection getOrCreateCollection(String collectionUri, int pathSegmentOffset) throws XMLDBException {
        
        Collection col = DatabaseManager.getCollection(conn.uri + collectionUri, conn.user, conn.password);
        
        // create the collection if it does not exist
        if(col == null) {
        
         	if(collectionUri.startsWith("/")) {
                collectionUri = collectionUri.substring(1);
            }
            
        	String pathSegments[] = collectionUri.split("/");
            
        	if(pathSegments.length > 0) {
                StringBuilder path = new StringBuilder();
            
                for(int i = 0; i <= pathSegmentOffset; i++) {
                    path.append("/" + pathSegments[i]);
                }
                
                Collection startCol = DatabaseManager.getCollection(conn.uri + path, conn.user, conn.password);
                
                if (startCol == null) {
                	
                	// child collection does not exist
                    
                	String parentPath = path.substring(0, path.lastIndexOf("/"));
                    Collection parentCol = DatabaseManager.getCollection(conn.uri + parentPath, conn.user, conn.password);
                    
                    CollectionManagementService mgt = (CollectionManagementService) parentCol.getService("CollectionManagementService", "1.0");
                    
                    System.out.println("[INFO] Creating the collection: " + pathSegments[pathSegmentOffset]);
                    col = mgt.createCollection(pathSegments[pathSegmentOffset]);
                    
                    col.close();
                    parentCol.close();
                    
                } else {
                    startCol.close();
                }
            }
            return getOrCreateCollection(collectionUri, ++pathSegmentOffset);
        } else {
            return col;
        }
    }

}
