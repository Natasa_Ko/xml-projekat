package com.team.xml;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.team.xml.test.ZahtevCirUnmarshalling;

import com.team.xml.test.ZahtevCirMarshalling;

@SpringBootApplication
public class XmlApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(XmlApplication.class, args);
		
//		ZahtevCirUnmarshalling unmarshallingZahtevCir = new ZahtevCirUnmarshalling();
//		unmarshallingZahtevCir.unmarhsalling();
//		
//		ZahtevCirMarshalling marshallingZahtevCir = new ZahtevCirMarshalling();
//		marshallingZahtevCir.marshalZahtevCir();
	}

}
