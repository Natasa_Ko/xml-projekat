package com.team.xml.controllers;

import static com.team.xml.util.Constants.COLLECTION_URI;
import static com.team.xml.util.Constants.PREDICATE_NAMESPACE;
import static com.team.xml.util.Constants.RESENJE_ID;
import static com.team.xml.util.Constants.RESENJE_SPARQL_NAMED_GRAPH_URI;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.update.UpdateExecutionFactory;
import org.apache.jena.update.UpdateFactory;
import org.apache.jena.update.UpdateProcessor;
import org.apache.jena.update.UpdateRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Database;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.XUpdateQueryService;

import com.team.xml.templates.ResenjeUpdate;
import com.team.xml.util.AuthenticationUtil;
import com.team.xml.util.AuthenticationUtilRDF;
import com.team.xml.util.ControllerInterface;
import com.team.xml.util.SparqlUtil;
import com.team.xml.util.AuthenticationUtilRDF.ConnectionProperties;

@Controller
@RequestMapping("/resenje")
public class ResenjeController implements ControllerInterface {
	
	private AuthenticationUtil.ConnectionProperties conn;

	@Override
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<HttpStatus> insert() throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, XMLDBException {
		conn = AuthenticationUtil.loadProperties();
	
	    Class<?> cl = Class.forName(conn.driver);
	    
	    Database database = (Database) cl.newInstance();
	    database.setProperty("create-database", "true");
	    
	    DatabaseManager.registerDatabase(database);
	    
	    Collection col = null;
	    String xmlData ="<resenja xmlns=\"http://localhost:8080/resenje\"\r\n" + 
	    		"	xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\r\n" + 
	    		"	xsi:schemaLocation=\"http://localhost:8080/resenje resenje.xsd\"\r\n" + 
	    		"	xmlns:addr=\"http://localhost:8080/rdf/example\"\r\n" + 
	    		"	xmlns:pred=\"http://localhost:8080/rdf/examples/predicate/\">\r\n" + 
	    		"	<resenje tip=\"када је жалба основана\"\r\n" + 
	    		"		datum=\"09.06.2020. године.\"\r\n" + 
	    		"		vocab=\"http://localhost:8080/rdf/examples/predicate/\"\r\n" + 
	    		"		about=\"http://localhost:8080/rdf/examples/resenje/071-01-1114-2020-03\">\r\n" + 
	    		"		<broj_resenja property=\"pred:broj_resenja\"\r\n" + 
	    		"			datatype=\"xs:string\">071-01-1114-2020-03</broj_resenja>\r\n" + 
	    		"		<uvod>\r\n" + 
	    		"			<podnosilac_zalbe property=\"pred:podnosilac_zalbe\"\r\n" + 
	    		"				datatype=\"TOsoba\">\r\n" + 
	    		"				<ime>Pera</ime>\r\n" + 
	    		"				<prezime>Peric</prezime>\r\n" + 
	    		"			</podnosilac_zalbe>\r\n" + 
	    		"			<razlog_zalbe></razlog_zalbe>\r\n" + 
	    		"			<datum_zahteva></datum_zahteva>\r\n" + 
	    		"			<zakon></zakon>\r\n" + 
	    		"		</uvod>\r\n" + 
	    		"		<tekst_resenja>\r\n" + 
	    		"			<primalac_resenja property=\"pred:primalac_resenja\"\r\n" + 
	    		"				datatype=\"xs:string\">Univerzitet u Novom Sadu</primalac_resenja>\r\n" + 
	    		"		</tekst_resenja>\r\n" + 
	    		"\r\n" + 
	    		"		<tekst_obrazlozenja>\r\n" + 
	    		"			<razlog_zalbe></razlog_zalbe>\r\n" + 
	    		"			<postupanje_po_zalbi></postupanje_po_zalbi>\r\n" + 
	    		"			<sta_je_utvrdjeno></sta_je_utvrdjeno>\r\n" + 
	    		"			<sta_je_naredjeno></sta_je_naredjeno>\r\n" + 
	    		"			<uputstvo_o_pravnom_sredstvu></uputstvo_o_pravnom_sredstvu>\r\n" + 
	    		"		</tekst_obrazlozenja>\r\n" + 
	    		"\r\n" + 
	    		"		<poverenik property=\"pred:poverenik\" datatype=\"TOsoba\">\r\n" + 
	    		"			<ime>Marko</ime>\r\n" + 
	    		"			<prezime>Markovic</prezime>\r\n" + 
	    		"		</poverenik>\r\n" + 
	    		"\r\n" + 
	    		"	</resenje>\r\n" + 
	    		"</resenja>";
	    			    
	    try { 
	        col = DatabaseManager.getCollection(conn.uri + COLLECTION_URI, conn.user, conn.password);
	        col.setProperty("indent", "yes");
	    	
	        XUpdateQueryService xupdateService = (XUpdateQueryService) col.getService("XUpdateQueryService", "1.0");
	        xupdateService.setProperty("indent", "yes");
	        xupdateService.updateResource(RESENJE_ID, String.format(ResenjeUpdate.APPEND, RESENJE_SPARQL_NAMED_GRAPH_URI, xmlData));
	        
	    } finally {
	    	
	        if(col != null) {
	            try { 
	            	col.close();
	            } catch (XMLDBException xe) {
	            	xe.printStackTrace();
	            }
	        }
	    }
	    //rdf
	    ConnectionProperties conn = AuthenticationUtilRDF.loadProperties();
		
		Model model = ModelFactory.createDefaultModel();
		model.setNsPrefix("pred", PREDICATE_NAMESPACE);
	
		// Making the changes manually 
		Resource resource = model.createResource("http://localhost:8080/rdf/examples/resenje/071-01-1115-2020-03");
		
		Property property1 = model.createProperty(PREDICATE_NAMESPACE, "broj_resenja");
		Literal literal1 = model.createLiteral("071-01-1114-2020-03");
		
		Property property2 = model.createProperty(PREDICATE_NAMESPACE, "podnosilac_zalbe");
		Literal literal2 = model.createLiteral("Pera Peric");
		
		Property property3 = model.createProperty(PREDICATE_NAMESPACE, "poverenik");
		Literal literal3 = model.createLiteral("Marko Markovic");
		
		Property property4 = model.createProperty(PREDICATE_NAMESPACE, "primalac_resenja");
		Literal literal4 = model.createLiteral("Univerzitet u Novom Sadu");
		
		
		// Adding the statements to the model
		Statement statement1 = model.createStatement(resource, property1, literal1);
		Statement statement2 = model.createStatement(resource, property2, literal2);
		Statement statement3 = model.createStatement(resource, property3, literal3);
		Statement statement4 = model.createStatement(resource, property4, literal4);
		
		
	
		model.add(statement1);
		model.add(statement2);
		model.add(statement3);
		model.add(statement4);
		
	
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		model.write(out, SparqlUtil.NTRIPLES);
		
		String sparqlUpdate = SparqlUtil.insertData(conn.dataEndpoint + RESENJE_SPARQL_NAMED_GRAPH_URI, new String(out.toByteArray()));
		
		UpdateRequest update = UpdateFactory.create(sparqlUpdate);
	
	    UpdateProcessor processor = UpdateExecutionFactory.createRemote(update, conn.updateEndpoint);
		processor.execute();
	
		model.close();
		
		System.out.println("[INFO] Insertion done");
	    
	    return new ResponseEntity<>(HttpStatus.OK);
	}

}
