package com.team.xml.test;

import java.io.File;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import com.team.xml.model.*;

//pisanje u XML
public class ZahtevCirMarshalling {
	// DefiniÅ¡e se JAXB kontekst (putanja do paketa sa JAXB bean-ovima)
	public static String JAXBContentPath = "com.team.xml.model";
	
	public String marshalZahtevCir() throws Exception {
	
	StringWriter sw = new StringWriter();
	JAXBContext context = JAXBContext.newInstance(JAXBContentPath);
	
	// Unmarshaller je objekat zaduÅ¾en za konverziju iz XML-a u objektni model
	Unmarshaller unmarshaller = context.createUnmarshaller();

	Zahtev zahtev = (Zahtev) unmarshaller.unmarshal(new File("./data/zahtevcir.xml"));
				
//	// Izmena nad objektnim modelom dodavanjem novog odseka
//	zahtev.getMesto().set("BANJALUKA");
				
	// Marshaller je objekat zaduÅ¾en za konverziju iz objektnog u XML model
	Marshaller marshaller = context.createMarshaller();
	
	File file = new File("./data/zahteviCirUpdated.xml");
	//String constant = XMLConstants.W3C_XML_SCHEMA_NS_URI;
	//SchemaFactory xsdFactory = SchemaFactory.newInstance(Zahtev.class);
	//Schema schema = null;
	//schema = xsdFactory.newSchema(file);
				
	//marshaller.setSchema(schema);
	// PodeÅ¡avanje marshaller-a
	marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
				
	// Umesto System.out-a, moÅ¾e se koristiti FileOutputStream
	marshaller.marshal(zahtev, file);
	marshaller.marshal(zahtev, System.out);
	
	return "ok";
	}
}
