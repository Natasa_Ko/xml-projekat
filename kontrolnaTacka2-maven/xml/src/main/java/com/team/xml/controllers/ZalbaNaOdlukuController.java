package com.team.xml.controllers;

import static com.team.xml.util.Constants.COLLECTION_URI;
import static com.team.xml.util.Constants.PREDICATE_NAMESPACE;
import static com.team.xml.util.Constants.ZALBAODLUKA_ID;
import static com.team.xml.util.Constants.ZALBAODLUKA_SPARQL_NAMED_GRAPH_URI;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.update.UpdateExecutionFactory;
import org.apache.jena.update.UpdateFactory;
import org.apache.jena.update.UpdateProcessor;
import org.apache.jena.update.UpdateRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Database;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.XUpdateQueryService;

import com.team.xml.templates.ZalbaNaOdlukuUpdate;
import com.team.xml.util.AuthenticationUtil;
import com.team.xml.util.AuthenticationUtilRDF;
import com.team.xml.util.AuthenticationUtilRDF.ConnectionProperties;
import com.team.xml.util.ControllerInterface;
import com.team.xml.util.SparqlUtil;

@Controller
@RequestMapping("/zalbanaodluku")
public class ZalbaNaOdlukuController implements ControllerInterface {

	private AuthenticationUtil.ConnectionProperties conn;

	@Override
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<HttpStatus> insert() throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, XMLDBException {
		conn = AuthenticationUtil.loadProperties();
	
	    Class<?> cl = Class.forName(conn.driver);
	    
	    Database database = (Database) cl.newInstance();
	    database.setProperty("create-database", "true");
	    
	    DatabaseManager.registerDatabase(database);
	    
	    Collection col = null;
	    String xmlData ="<zalbe_na_odluku xmlns=\"http://localhost:8080/zalbanaodluku\"\r\n" + 
	    		"	xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\r\n" + 
	    		"	xsi:schemaLocation=\"http://localhost:8080/xml zalbanaodlukucir.xsd\"\r\n" + 
	    		"	xmlns:addr=\"http://localhost:8080/rdf/example\"\r\n" + 
	    		"	xmlns:pred=\"http://localhost:8080/rdf/examples/predicate/\">\r\n" + 
	    		"	<zalba vocab=\"http://localhost:8080/rdf/examples/predicate/\"\r\n" + 
	    		"		about=\"http://localhost:8080/rdf/examples/zalbanaodluku/RJ-1-555\">\r\n" + 
	    		"		<vrsta_zalbe>\r\n" + 
	    		"		</vrsta_zalbe>\r\n" + 
	    		"		<poverenik>\r\n" + 
	    		"		</poverenik>\r\n" + 
	    		"		<adresa_za_postu>\r\n" + 
	    		"			<grad> </grad>\r\n" + 
	    		"			<ulica> </ulica>\r\n" + 
	    		"			<broj>1</broj>\r\n" + 
	    		"		</adresa_za_postu>\r\n" + 
	    		"		<naslov>\r\n" + 
	    		"		</naslov>\r\n" + 
	    		"		<informacije_o_zaliocu>\r\n" + 
	    		"			<fizicko_lice>\r\n" + 
	    		"				<ime_zalioca property=\"pred:ime_zalioca\"\r\n" + 
	    		"					datatype=\"xs:string\"> Pera\r\n" + 
	    		"				</ime_zalioca>\r\n" + 
	    		"				<prezime_zalioca property=\"pred:prezime_zalioca\"\r\n" + 
	    		"					datatype=\"xs:string\">\r\n" + 
	    		"				</prezime_zalioca> Peric\r\n" + 
	    		"				<adresa_zalioca>\r\n" + 
	    		"					<grad>Novi Sad</grad>\r\n" + 
	    		"					<ulica>Fruskogorska</ulica>\r\n" + 
	    		"					<broj>1</broj>\r\n" + 
	    		"				</adresa_zalioca>\r\n" + 
	    		"			</fizicko_lice>\r\n" + 
	    		"		</informacije_o_zaliocu>\r\n" + 
	    		"		<odluka_koja_se_pobija>\r\n" + 
	    		"			<naziv_organa property=\"pred:naziv_organa\"\r\n" + 
	    		"				datatype=\"xs:string\">Visi sud u Novom Sadu\r\n" + 
	    		"			</naziv_organa>\r\n" + 
	    		"			<broj_resenja property=\"pred:broj_resenja\"\r\n" + 
	    		"				datatype=\"xs:string\"> RJ-1-555\r\n" + 
	    		"			</broj_resenja>\r\n" + 
	    		"			<datum_resenja property=\"pred:datum_resenja\"\r\n" + 
	    		"				datatype=\"ns1:TDatum\">\r\n" + 
	    		"				<dan>25</dan>\r\n" + 
	    		"				<mesec>11</mesec>\r\n" + 
	    		"				<godina>2019</godina>\r\n" + 
	    		"			</datum_resenja>\r\n" + 
	    		"		</odluka_koja_se_pobija>\r\n" + 
	    		"		<razlog_zalbe>\r\n" + 
	    		"			<datum_odbijenog_zahteva\r\n" + 
	    		"				property=\"pred:datum_odbijenog_zahteva\" datatype=\"ns1:TDatum\">\r\n" + 
	    		"				<dan>25</dan>\r\n" + 
	    		"				<mesec>11</mesec>\r\n" + 
	    		"				<godina>2019</godina>\r\n" + 
	    		"			</datum_odbijenog_zahteva>\r\n" + 
	    		"			<sporni_dio property=\"pred:sporni_dio\" datatype=\"xs:string\">\r\n" + 
	    		"			Odbijeni zahtev na osnovu...\r\n" + 
	    		"			</sporni_dio>\r\n" + 
	    		"		</razlog_zalbe>\r\n" + 
	    		"		<podaci_o_zalbi>\r\n" + 
	    		"			<mesto property=\"pred:mesto\" datatype=\"xs:string\">\r\n" + 
	    		"			</mesto>Novi Sad\r\n" + 
	    		"			<datum_zalbe>\r\n" + 
	    		"				<dan>27</dan>\r\n" + 
	    		"				<mesec>11</mesec>\r\n" + 
	    		"				<godina>2019</godina>\r\n" + 
	    		"			</datum_zalbe>\r\n" + 
	    		"		</podaci_o_zalbi>\r\n" + 
	    		"		<informacije_podnosiocu_zalbe>\r\n" + 
	    		"			<ime_i_prezime_podnosioca\r\n" + 
	    		"				property=\"pred:ime_i_prezime_podnosioca\" datatype=\"xs:string\">Pera Peric</ime_i_prezime_podnosioca>\r\n" + 
	    		"			<adresa_podnosioca>\r\n" + 
	    		"				<grad>Novi Sad</grad>\r\n" + 
	    		"				<ulica>Fruskogorska</ulica>\r\n" + 
	    		"				<broj>1</broj>\r\n" + 
	    		"			</adresa_podnosioca>\r\n" + 
	    		"			<podaci_za_kontakt></podaci_za_kontakt>\r\n" + 
	    		"			<potpis_podnosioca></potpis_podnosioca>\r\n" + 
	    		"		</informacije_podnosiocu_zalbe>\r\n" + 
	    		"		<napomena>\r\n" + 
	    		"			<tacka>\r\n" + 
	    		"			</tacka>\r\n" + 
	    		"			<tacka>\r\n" + 
	    		"			</tacka>\r\n" + 
	    		"		</napomena>\r\n" + 
	    		"	</zalba>\r\n" + 
	    		"</zalbe_na_odluku>\r\n" ;
	    			    
	    try { 
	        col = DatabaseManager.getCollection(conn.uri + COLLECTION_URI, conn.user, conn.password);
	        col.setProperty("indent", "yes");
	    	
	        XUpdateQueryService xupdateService = (XUpdateQueryService) col.getService("XUpdateQueryService", "1.0");
	        xupdateService.setProperty("indent", "yes");
	        xupdateService.updateResource(ZALBAODLUKA_ID, String.format(ZalbaNaOdlukuUpdate.APPEND, ZALBAODLUKA_SPARQL_NAMED_GRAPH_URI, xmlData));
	        
	    } finally {
	    	
	        if(col != null) {
	            try { 
	            	col.close();
	            } catch (XMLDBException xe) {
	            	xe.printStackTrace();
	            }
	        }
	    }
	    //rdf
	    ConnectionProperties conn = AuthenticationUtilRDF.loadProperties();
		
		Model model = ModelFactory.createDefaultModel();
		model.setNsPrefix("pred", PREDICATE_NAMESPACE);
	
		// Making the changes manually 
		Resource resource = model.createResource("http://localhost:8080/rdf/examples/zalbanaodluku/RJ-1-556");
		
		Property property1 = model.createProperty(PREDICATE_NAMESPACE, "ime_zalioca");
		Literal literal1 = model.createLiteral("Pera");
		
		Property property2 = model.createProperty(PREDICATE_NAMESPACE, "prezime_zalioca");
		Literal literal2 = model.createLiteral("Peric");
		
		Property property3 = model.createProperty(PREDICATE_NAMESPACE, "adresa_zalioca");
		Literal literal3 = model.createLiteral("Novi Sad Fruskogorska 1");
		
		Property property4 = model.createProperty(PREDICATE_NAMESPACE, "naziv_organa");
		Literal literal4 = model.createLiteral("Visi sud u Novom Sadu");
		
		Property property5 = model.createProperty(PREDICATE_NAMESPACE, "broj_resenja");
		Literal literal5 = model.createLiteral("RJ-1-556");
		
		Property property6 = model.createProperty(PREDICATE_NAMESPACE, "datum_resenja");
		Literal literal6 = model.createLiteral("2019-11-25");
		
		Property property8 = model.createProperty(PREDICATE_NAMESPACE, "sporni_dio");
		Literal literal8 = model.createLiteral("Odbijeni zahtev na osnovu...");
		
		Property property9 = model.createProperty(PREDICATE_NAMESPACE, "datum_zalbe");
		Literal literal9 = model.createLiteral("2019-11-27");
		
		Property property11 = model.createProperty(PREDICATE_NAMESPACE, "ime_i_prezime_podnosioca");
		Literal literal11 = model.createLiteral("Pera Peric");

		// Adding the statements to the model
		Statement statement1 = model.createStatement(resource, property1, literal1);
		Statement statement2 = model.createStatement(resource, property2, literal2);
		Statement statement3 = model.createStatement(resource, property3, literal3);
		Statement statement4 = model.createStatement(resource, property4, literal4);
		Statement statement5 = model.createStatement(resource, property5, literal5);
		Statement statement6 = model.createStatement(resource, property6, literal6);
		Statement statement8 = model.createStatement(resource, property8, literal8);
		Statement statement9 = model.createStatement(resource, property9, literal9);
		Statement statement11 = model.createStatement(resource, property11, literal11);
		
	
		model.add(statement1);
		model.add(statement2);
		model.add(statement3);
		model.add(statement4);
		model.add(statement5);
		model.add(statement6);
		model.add(statement8);
		model.add(statement9);
		model.add(statement11);
	
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		model.write(out, SparqlUtil.NTRIPLES);
		
		String sparqlUpdate = SparqlUtil.insertData(conn.dataEndpoint + ZALBAODLUKA_SPARQL_NAMED_GRAPH_URI, new String(out.toByteArray()));
		
		UpdateRequest update = UpdateFactory.create(sparqlUpdate);
	
	    UpdateProcessor processor = UpdateExecutionFactory.createRemote(update, conn.updateEndpoint);
		processor.execute();
	
		model.close();
		
		System.out.println("[INFO] Insertion done");
	    
	    return new ResponseEntity<>(HttpStatus.OK);
	}
}
