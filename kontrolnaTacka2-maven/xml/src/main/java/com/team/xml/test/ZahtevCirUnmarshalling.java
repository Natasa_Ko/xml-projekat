package com.team.xml.test;

import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.team.xml.model.Zahtev;


// from XML file to Java object
public class ZahtevCirUnmarshalling {

	public static String JAXBContentPath = "com.team.xml.model";
	
	public Zahtev unmarhsalling() throws Exception {
		// Definise se JAXB kontekst (putanja do paketa sa JAXB bean-ovima)
		JAXBContext context = JAXBContext.newInstance(JAXBContentPath);

		// Unmarshaller je objekat zaduzen za konverziju iz XML-a u objektni model
		Unmarshaller unmarshaller = context.createUnmarshaller();
		
		// Unmarshalling generise objektni model na osnovu XML fajla 
		Zahtev zahtev = (Zahtev) unmarshaller.unmarshal(new File("./data/zahtevcir.xml"));
		
		//prikazuje unmarshallovan objekat
		
		printZahtevCir(zahtev);
		return zahtev;
		
	}

	private static void printZahtevCir(Zahtev zahtev) {
//		System.out.println("назив и седиште органа коме се захтев упућује : " + zahtev.getPodaciOOrganu().getNazivOrgana() + " " + zahtev.getPodaciOOrganu().getSedisteOrgana());
//		System.out.println("З А Х Т Е В \r\n" + 
//				"\r\n" + 
//				"за приступ информацији од јавног значаја ");
//		System.out.println("На основу члана " + zahtev.getZahtevNaOsnovu().getBrojClana() + " . ст. "  + zahtev.getZahtevNaOsnovu().getStav() + " " + zahtev.getZahtevNaOsnovu().getZakon());
//		System.out.println(" , од горе наведеног органа захтевам:  ");
//		System.out.println(" обавештење да ли поседује тражену информацију; ");
//		if(zahtev.getContent().isPosedovanjeInformacije()) {System.out.println(" ZAHTEVA");}else {System.out.println("NE ZAHTEVA");}
//		System.out.println(" увид у документ који садржи тражену информацију;   "); 
//		if(zahtev.getElementiZahteva().isUvidUDokument()) {System.out.println(" ZAHTEVA");} else { System.out.println("NE ZAHTEVA");}
//		System.out.println(" копију документа који садржи тражену информацију;  " );
//		if( zahtev.getElementiZahteva().isKopijaDokumenta()) {System.out.println(" ZAHTEVA");} else {System.out.println("NE ZAHTEVA");}
//		System.out.println(" достављање копије документа који садржи тражену информацију:**  ");
//		if(zahtev.getElementiZahteva().getDostavljanjeKopijeDokumenta().isElektronskomPostom() || zahtev.getElementiZahteva().getDostavljanjeKopijeDokumenta().isFaksom() || !zahtev.getElementiZahteva().getDostavljanjeKopijeDokumenta().getDrugiNacin().isEmpty())
//		{System.out.println(" ZAHTEVA");}else{System.out.println("NE ZAHTEVA");}
//		if(zahtev.getElementiZahteva().getDostavljanjeKopijeDokumenta() != null) {
//			System.out.println(" поштом " + zahtev.getElementiZahteva().getDostavljanjeKopijeDokumenta().isPostom());
//			System.out.println(" електронском поштом " + zahtev.getElementiZahteva().getDostavljanjeKopijeDokumenta().isElektronskomPostom());
//
//			System.out.println(" факсом " + zahtev.getElementiZahteva().getDostavljanjeKopijeDokumenta().isFaksom());
//			System.out.println("  на други начин: " + zahtev.getElementiZahteva().getDostavljanjeKopijeDokumenta().getDrugiNacin());
//			
//		}
//		System.out.println("Овај захтев се односи на следеће информације:  " + zahtev.getInformacijeNaKojeSeOdnosiZahtev());
//		System.out.println("У " + zahtev.getMestoIDatum().getMesto());
//		System.out.println(", дана " + zahtev.getMestoIDatum().getDan() + "." + zahtev.getMestoIDatum().getMesec() + " " + zahtev.getMestoIDatum().getGodina() + "године/n");
//
//		System.out.println("Тражилац информације/Име и презиме" + zahtev.getInformacijeOTraziocu().getTrazioc().getFizickoLice() != null ? zahtev.getInformacijeOTraziocu().getTrazioc().getFizickoLice().getIme() +zahtev.getInformacijeOTraziocu().getTrazioc().getFizickoLice().getPrezime() :zahtev.getInformacijeOTraziocu().getTrazioc().getPravnoLice() );
//		System.out.println(" адреса " + zahtev.getInformacijeOTraziocu().getAdresa().getGrad() + " " +zahtev.getInformacijeOTraziocu().getAdresa().getUlica() + " "  + zahtev.getInformacijeOTraziocu().getAdresa().getBroj());
//		System.out.println(" други подаци за контакт " + zahtev.getInformacijeOTraziocu().getDrugiPodaciZaKontakt());
//		
	}
}
