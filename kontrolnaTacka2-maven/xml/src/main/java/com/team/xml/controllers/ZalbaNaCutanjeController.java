package com.team.xml.controllers;

import static com.team.xml.util.Constants.COLLECTION_URI;
import static com.team.xml.util.Constants.PREDICATE_NAMESPACE;
import static com.team.xml.util.Constants.ZALBACUTANJE_ID;
import static com.team.xml.util.Constants.ZALBACUTANJE_SPARQL_NAMED_GRAPH_URI;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.update.UpdateExecutionFactory;
import org.apache.jena.update.UpdateFactory;
import org.apache.jena.update.UpdateProcessor;
import org.apache.jena.update.UpdateRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Database;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.XUpdateQueryService;

import com.team.xml.templates.ZalbaNaOdlukuUpdate;
import com.team.xml.util.AuthenticationUtil;
import com.team.xml.util.AuthenticationUtilRDF;
import com.team.xml.util.AuthenticationUtilRDF.ConnectionProperties;
import com.team.xml.util.ControllerInterface;
import com.team.xml.util.SparqlUtil;

@Controller
@RequestMapping("/zalbanacutanje")
public class ZalbaNaCutanjeController implements ControllerInterface {

	private AuthenticationUtil.ConnectionProperties conn;

	@Override
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<HttpStatus> insert() throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, XMLDBException {
		conn = AuthenticationUtil.loadProperties();
	
	    Class<?> cl = Class.forName(conn.driver);
	    
	    Database database = (Database) cl.newInstance();
	    database.setProperty("create-database", "true");
	    
	    DatabaseManager.registerDatabase(database);
	    
	    Collection col = null;
	    String xmlData ="<zalbe xmlns=\"http://localhost:8080/zalbacutanjecir\"\r\n" + 
	    		"    xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\r\n" + 
	    		"    xsi:schemaLocation=\"http://localhost:8080/zalbacutanjecir zalbacutanjecir.xsd\"\r\n" + 
	    		"    xmlns:addr=\"http://localhost:8080/rdf/example\"\r\n" + 
	    		"    xmlns:pred=\"http://localhost:8080/rdf/examples/predicate/\">\r\n" + 
	    		"    \r\n" + 
	    		"    <zalba vocab=\"http://localhost:8080/rdf/examples/predicate/\">\r\n" + 
	    		"    <naslov>\r\n" + 
	    		"    </naslov>\r\n" + 
	    		"    <podaci_o_primaocu>\r\n" + 
	    		"        <kome>\r\n" + 
	    		"        </kome>\r\n" + 
	    		"        <adresa_za_postu>\r\n" + 
	    		"            <grad>\r\n" + 
	    		"            </grad>\r\n" + 
	    		"            <ulica>\r\n" + 
	    		"            </ulica>\r\n" + 
	    		"            <broj>\r\n" + 
	    		"                12\r\n" + 
	    		"            </broj>\r\n" + 
	    		"        </adresa_za_postu>    \r\n" + 
	    		"    </podaci_o_primaocu>\r\n" + 
	    		"        \r\n" + 
	    		"    <sadrzaj>\r\n" + 
	    		"        \r\n" + 
	    		"        <naziv_organa property=\"pred:naziv_organa\" datatype=\"xs:string\"/>\r\n" + 
	    		"        \r\n" + 
	    		"        због тога што орган власти: \r\n" + 
	    		"        <razlog_zalbe property=\"pred:razlog_zalbe\" datatype=\"xs:string\">\r\n" + 
	    		"            <opcija izabrano =\"\">није поступио</opcija>\r\n" + 
	    		"            <opcija izabrano =\"\">није поступио у целости</opcija>\r\n" + 
	    		"            <opcija izabrano =\"\"> у законском року</opcija>\r\n" + 
	    		"        </razlog_zalbe>\r\n" + 
	    		"        \r\n" + 
	    		"        \r\n" + 
	    		"        <datum_podnosenja_zahteva property=\"pred:datum_podnosenja_zahteva\" datatype=\"ns1:TDatum\">\r\n" + 
	    		"            <dan>25</dan>\r\n" + 
	    		"            <mesec>11</mesec>\r\n" + 
	    		"            <godina>2019</godina>\r\n" + 
	    		"        </datum_podnosenja_zahteva> \r\n" + 
	    		"        <podaci_o_zahtevu_i_informacijama/>\r\n" + 
	    		"   \r\n" + 
	    		"    </sadrzaj>\r\n" + 
	    		"    <podnosilac_zalbe >\r\n" + 
	    		"        <ime_i_prezime property=\"pred:ime_i_prezime\" datatype=\"xs:string\" >Petar</ime_i_prezime>\r\n" + 
	    		"        <potpis/>\r\n" + 
	    		"        <adresa property=\"pred:adresa\"\r\n" + 
	    		"            datatype=\"ns1:TAdresa\">\r\n" + 
	    		"            <grad>             </grad>\r\n" + 
	    		"            <ulica>             </ulica>\r\n" + 
	    		"            <broj>         12    </broj>\r\n" + 
	    		"        </adresa>\r\n" + 
	    		"        <drugi_podaci_za_kontakt/>      \r\n" + 
	    		"    </podnosilac_zalbe>\r\n" + 
	    		"    \r\n" + 
	    		"    <podaci_o_zalbi>\r\n" + 
	    		"        У <mesto/> , дана <dan>06</dan><mesec>10</mesec> <godina>2020</godina>године\r\n" + 
	    		"    </podaci_o_zalbi>\r\n" + 
	    		"    \r\n" + 
	    		"    \r\n" + 
	    		"</zalba>\r\n" + 
	    		"</zalbe>" ;
	    			    
	    try { 
	        col = DatabaseManager.getCollection(conn.uri + COLLECTION_URI, conn.user, conn.password);
	        col.setProperty("indent", "yes");
	    	
	        XUpdateQueryService xupdateService = (XUpdateQueryService) col.getService("XUpdateQueryService", "1.0");
	        xupdateService.setProperty("indent", "yes");
	        xupdateService.updateResource(ZALBACUTANJE_ID, String.format(ZalbaNaOdlukuUpdate.APPEND, ZALBACUTANJE_SPARQL_NAMED_GRAPH_URI, xmlData));
	        
	    } finally {
	    	
	        if(col != null) {
	            try { 
	            	col.close();
	            } catch (XMLDBException xe) {
	            	xe.printStackTrace();
	            }
	        }
	    }
	    //rdf
	    ConnectionProperties conn = AuthenticationUtilRDF.loadProperties();
		
		Model model = ModelFactory.createDefaultModel();
		model.setNsPrefix("pred", PREDICATE_NAMESPACE);
	
		// Making the changes manually 
		Resource resource = model.createResource("http://localhost:8080/rdf/examples/zalbacutanje");
		
		Property property1 = model.createProperty(PREDICATE_NAMESPACE, "naziv_organa");
		Literal literal1 = model.createLiteral("Visi sud u Beogradu");
		
		Property property2 = model.createProperty(PREDICATE_NAMESPACE, "razlog_zalbe");
		Literal literal2 = model.createLiteral("nije postupio");
		
		Property property3 = model.createProperty(PREDICATE_NAMESPACE, "datum_podnosenja_zahteva");
		Literal literal3 = model.createLiteral("2019-11-25");
		
		Property property4 = model.createProperty(PREDICATE_NAMESPACE, "ime_i_prezime");
		Literal literal4 = model.createLiteral("Petar Petrovic");
		
		Property property5 = model.createProperty(PREDICATE_NAMESPACE, "adresa");
		Literal literal5 = model.createLiteral("Novi Sad Fruskogorska 12");
		
		

		// Adding the statements to the model
		Statement statement1 = model.createStatement(resource, property1, literal1);
		Statement statement2 = model.createStatement(resource, property2, literal2);
		Statement statement3 = model.createStatement(resource, property3, literal3);
		Statement statement4 = model.createStatement(resource, property4, literal4);
		Statement statement5 = model.createStatement(resource, property5, literal5);
		
	
		model.add(statement1);
		model.add(statement2);
		model.add(statement3);
		model.add(statement4);
		model.add(statement5);
	
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		model.write(out, SparqlUtil.NTRIPLES);
		
		String sparqlUpdate = SparqlUtil.insertData(conn.dataEndpoint + ZALBACUTANJE_SPARQL_NAMED_GRAPH_URI, new String(out.toByteArray()));
		
		UpdateRequest update = UpdateFactory.create(sparqlUpdate);
	
	    UpdateProcessor processor = UpdateExecutionFactory.createRemote(update, conn.updateEndpoint);
		processor.execute();
	
		model.close();
		
		System.out.println("[INFO] Insertion done");
	    
	    return new ResponseEntity<>(HttpStatus.OK);
	}
}
