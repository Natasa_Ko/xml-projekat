package com.team.xml.util;

public class Constants {

	public static final String COLLECTION_URI= "/db/documents";
	public static final String PREDICATE_NAMESPACE = "http://localhost:8080/rdf/examples/predicate/";										   
	
	//obavestenje
	public static final String OBAVESTENJE_ID = "obavestenje.xml";
	public static final String OBAVESTENJE_LOCATION = "documents/obavestenje.xml";
	public static final String OBAVESTENJE_RDF_LOCATION = "documents/obavestenje.rdf"; //tu ce napraviti rdf
	public static final String OBAVESTENJE_SPARQL_NAMED_GRAPH_URI = "/obavestenja";
	
	//zalba na odluku
	public static final String ZALBAODLUKA_ID = "zalbanaodlukucir.xml";
	public static final String ZALBAODLUKA_LOCATION = "documents/zalbanaodlukucir.xml";
	public static final String ZALBAODLUKA_RDF_LOCATION = "documents/zalbanaodlukucir.rdf"; //tu ce napraviti rdf
	public static final String ZALBAODLUKA_SPARQL_NAMED_GRAPH_URI = "/zalbanaodluku";
	
	
	//zahtjev za informacije od javnog znacaja
	public static final String ZAHTEVCIR_ID = "zahtevcir.xml";
	public static final String ZAHTEVCIR_LOCATION = "documents/zahtevcir.xml";
	public static final String ZAHTEVCIR_RDF_LOCATION = "documents/zahtevcir.rdf"; //tu ce napraviti rdf
	public static final String ZAHTEVCIR_SPARQL_NAMED_GRAPH_URI = "/zahtevcir";
	

	//zalba na cutanje
	public static final String ZALBACUTANJE_ID = "zalbacutanjecir.xml";
	public static final String ZALBACUTANJE_LOCATION = "documents/zalbacutanjecir.xml";
	public static final String ZALBACUTANJE_RDF_LOCATION = "documents/zalbacutanjecir.rdf"; //tu ce napraviti rdf
	public static final String ZALBACUTANJE_SPARQL_NAMED_GRAPH_URI = "/zalbacutanjecir";
	
	//resenje
	public static final String RESENJE_ID = "resenje.xml";
	public static final String RESENJE_LOCATION = "documents/resenje.xml";
	public static final String RESENJE_RDF_LOCATION = "documents/resenje.rdf"; //tu ce napraviti rdf
	public static final String RESENJE_SPARQL_NAMED_GRAPH_URI = "/resenje";
	
	
	
}
