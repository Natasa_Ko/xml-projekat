package com.team.xml.controllers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.update.UpdateExecutionFactory;
import org.apache.jena.update.UpdateFactory;
import org.apache.jena.update.UpdateProcessor;
import org.apache.jena.update.UpdateRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Database;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.XUpdateQueryService;


import com.team.xml.templates.ObavestenjeUpdate;
import com.team.xml.util.AuthenticationUtil;
import com.team.xml.util.AuthenticationUtilRDF;
import com.team.xml.util.AuthenticationUtilRDF.ConnectionProperties;
import com.team.xml.util.ControllerInterface;
import com.team.xml.util.SparqlUtil;

import static com.team.xml.util.Constants.*;

@Controller
@RequestMapping("/obavestenje")
public class ObavestenjeController implements ControllerInterface{
	
	private AuthenticationUtil.ConnectionProperties conn;

	@Override
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<HttpStatus> insert() throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, XMLDBException {
		conn = AuthenticationUtil.loadProperties();

        Class<?> cl = Class.forName(conn.driver);
        
        Database database = (Database) cl.newInstance();
        database.setProperty("create-database", "true");
        
        DatabaseManager.registerDatabase(database);
        
        Collection col = null;
        String xmlData = "<obavestenje vocab=\"http://localhost:8080/rdf/examples/predicate/\" \r\n"
        		+ "        		 about=\"http://localhost:8080/rdf/examples/obavestenje/OB_5\">\r\n"
        		+ "		<podaci_o_primaocu>\r\n"
        		+ "			<naziv property = \"pred:naziv_ustanove\" datatype = \"xs:string\">Visi sud u Novom Sadu</naziv>\r\n"
        		+ "			<sediste property = \"pred:sediste_ustanove\" datatype = \"xs:string\">Novi Sad</sediste>\r\n"
        		+ "			<broj_predmeta property = \"pred:brojpredmeta\" datatype = \"xs:string\">OB_5</broj_predmeta>\r\n"
        		+ "			<datum_podnosenja property = \"pred:datum_podnosenja\" datatype = \"xs:date\">12-12-2012</datum_podnosenja>\r\n"
        		+ "		</podaci_o_primaocu>\r\n"
        		+ "		<podaci_o_podnosiocu>\r\n"
        		+ "			<ime_podnosioca property = \"pred:ime_podnosioca\" datatype = \"xs:string\">Petar</ime_podnosioca>\r\n"
        		+ "			<prezime_podnosioca property = \"pred:prezime_podnosioca\" datatype = \"xs:string\">Petrovic</prezime_podnosioca>\r\n"
        		+ "			<naziv_podnosioca property = \"pred:naziv_podnosioca\" datatype = \"xs:string\">NIS Petrol</naziv_podnosioca>\r\n"
        		+ "			<adresa_podnosioca property = \"pred:adresa_podnosioca\" datatype = \"xs:string\">Boska Buhe 9</adresa_podnosioca>\r\n"
        		+ "		</podaci_o_podnosiocu>\r\n"
        		+ "		\r\n"
        		+ "	    <trazena_informacija>\r\n"
        		+ "			<datum_zahteva property=\"pred:datum_zahteva\" datatype=\"xs:date\">12-12-2012</datum_zahteva>\r\n"
        		+ "			<opis_trazene_informacije property=\"pred:opis_informacije\" datatype=\"xs:string\">Uvid u budzet za 2020. godinu</opis_trazene_informacije>\r\n"
        		+ "	    </trazena_informacija>\r\n"
        		+ "	    <obavestenje>\r\n"
        		+ "			<datum_uvida>29-12-2012</datum_uvida>\r\n"
        		+ "	      	<od_vreme>12:00</od_vreme>\r\n"
        		+ "			<do_vreme>15:00</do_vreme>\r\n"
        		+ "			<grad>Novi Sad</grad>\r\n"
        		+ "			<ulica>Ulica</ulica>\r\n"
        		+ "			<broj_ulice>5</broj_ulice>\r\n"
        		+ "			<broj_kancelarije>105</broj_kancelarije>\r\n"
        		+ "	    </obavestenje>\r\n"
        		+ "	    <troskovi>\r\n"
        		+ "	       <format tip = \"A4\" cena = \"3\">3.00</format>\r\n"
        		+ "	    </troskovi>\r\n"
        		+ "	    <uplata>\r\n"
        		+ "	       <iznos_za_uplatu>840.00</iznos_za_uplatu>  \r\n"
        		+ "	       <racun broj = \"840-742328-843-30\" model = \"97\">840-742328-843-30</racun>\r\n"
        		+ "	    </uplata>\r\n"
        		+ "	    \r\n"
        		+ "	    <dostavljeno>\r\n"
        		+ "	        <opcija broj = \"1\">1. Именованом (М.П.)</opcija>\r\n"
        		+ "	        <opcija broj = \"2\">2. Архиви</opcija>\r\n"
        		+ "	    </dostavljeno>\r\n"
        		+ "	    \r\n"
        		+ "	    <potpis_ovlascenog_lica/>\r\n"
        		+ "	</obavestenje>";
        
        try { 
            col = DatabaseManager.getCollection(conn.uri + COLLECTION_URI, conn.user, conn.password);
            col.setProperty("indent", "yes");
        	
            XUpdateQueryService xupdateService = (XUpdateQueryService) col.getService("XUpdateQueryService", "1.0");
            xupdateService.setProperty("indent", "yes");
            xupdateService.updateResource(OBAVESTENJE_ID, String.format(ObavestenjeUpdate.APPEND, OBAVESTENJE_SPARQL_NAMED_GRAPH_URI, xmlData));
            
        } finally {
        	
            if(col != null) {
                try { 
                	col.close();
                } catch (XMLDBException xe) {
                	xe.printStackTrace();
                }
            }
        }
        //rdf
        ConnectionProperties conn = AuthenticationUtilRDF.loadProperties();
		
		Model model = ModelFactory.createDefaultModel();
		model.setNsPrefix("pred", PREDICATE_NAMESPACE);

		// Making the changes manually 
		Resource resource = model.createResource("http://localhost:8080/rdf/examples/obavestenje/OB_5");
		
		Property property1 = model.createProperty(PREDICATE_NAMESPACE, "naziv_ustanove");
		Literal literal1 = model.createLiteral("Visi sud u Novom Sadu");
		
		Property property2 = model.createProperty(PREDICATE_NAMESPACE, "sediste_ustanove");
		Literal literal2 = model.createLiteral("Novi Sad");
		
		Property property3 = model.createProperty(PREDICATE_NAMESPACE, "brojpredmeta");
		Literal literal3 = model.createLiteral("OB_1");
		
		Property property4 = model.createProperty(PREDICATE_NAMESPACE, "datum_podnosenja");
		Literal literal4 = model.createLiteral("12-12-2012");
		
		Property property5 = model.createProperty(PREDICATE_NAMESPACE, "ime_podnosioca");
		Literal literal5 = model.createLiteral("Petar");
		
		Property property6 = model.createProperty(PREDICATE_NAMESPACE, "prezime_podnosioca");
		Literal literal6 = model.createLiteral("Petrovic");
		
		Property property7 = model.createProperty(PREDICATE_NAMESPACE, "naziv_podnosioca");
		Literal literal7 = model.createLiteral("NIS Petrol");
		
		Property property8 = model.createProperty(PREDICATE_NAMESPACE, "adresa_podnosioca");
		Literal literal8 = model.createLiteral("Boska Buhe 9");
		
		Property property9 = model.createProperty(PREDICATE_NAMESPACE, "datum_zahteva");
		Literal literal9 = model.createLiteral("12-12-2012");
		
		Property property10 = model.createProperty(PREDICATE_NAMESPACE, "opis_informacije");
		Literal literal10 = model.createLiteral("Uvid u budzet za 2010. godinu");
		
		// Adding the statements to the model
		Statement statement1 = model.createStatement(resource, property1, literal1);
		Statement statement2 = model.createStatement(resource, property2, literal2);
		Statement statement3 = model.createStatement(resource, property3, literal3);
		Statement statement4 = model.createStatement(resource, property4, literal4);
		Statement statement5 = model.createStatement(resource, property5, literal5);
		Statement statement6 = model.createStatement(resource, property6, literal6);
		Statement statement7 = model.createStatement(resource, property7, literal7);
		Statement statement8 = model.createStatement(resource, property8, literal8);
		Statement statement9 = model.createStatement(resource, property9, literal9);
		Statement statement10 = model.createStatement(resource, property10, literal10);

		model.add(statement1);
		model.add(statement2);
		model.add(statement3);
		model.add(statement4);
		model.add(statement5);
		model.add(statement6);
		model.add(statement7);
		model.add(statement8);
		model.add(statement9);
		model.add(statement10);

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		model.write(out, SparqlUtil.NTRIPLES);
		
		String sparqlUpdate = SparqlUtil.insertData(conn.dataEndpoint + OBAVESTENJE_SPARQL_NAMED_GRAPH_URI, new String(out.toByteArray()));
		
		UpdateRequest update = UpdateFactory.create(sparqlUpdate);

        UpdateProcessor processor = UpdateExecutionFactory.createRemote(update, conn.updateEndpoint);
		processor.execute();

		model.close();
		
		System.out.println("[INFO] Insertion done");
        
        return new ResponseEntity<>(HttpStatus.OK);
	}

}
