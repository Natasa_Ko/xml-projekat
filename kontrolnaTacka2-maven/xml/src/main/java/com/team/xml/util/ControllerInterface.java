package com.team.xml.util;

import java.io.IOException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.XMLDBException;



public interface ControllerInterface {
	
	public ResponseEntity<HttpStatus> insert() throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, XMLDBException;
	
	

}
