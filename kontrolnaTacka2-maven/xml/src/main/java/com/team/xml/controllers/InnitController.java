package com.team.xml.controllers;



import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.xml.transform.TransformerException;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.update.UpdateExecutionFactory;
import org.apache.jena.update.UpdateFactory;
import org.apache.jena.update.UpdateProcessor;
import org.apache.jena.update.UpdateRequest;
import org.exist.xmldb.EXistResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.xml.sax.SAXException;
import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Database;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.CollectionManagementService;
import org.xmldb.api.modules.XMLResource;


import com.team.xml.util.AuthenticationUtil;
import com.team.xml.util.AuthenticationUtilRDF;
import com.team.xml.util.MetadataExtractor;
import com.team.xml.util.SparqlUtil;

import static com.team.xml.util.Constants.*;
import static com.team.xml.util.AuthenticationUtilRDF.ConnectionProperties;



@RestController

public class InnitController {

	private static AuthenticationUtil.ConnectionProperties conn;
	
	@RequestMapping(value = "/initialization", method = RequestMethod.GET)
	public ResponseEntity<HttpStatus> innitExist() throws XMLDBException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, SAXException, TransformerException {
		//inicijalizuje EXIST bazu i upisuje dokumente u nju
		
		this.conn = AuthenticationUtil.loadProperties();
        	
    	Class<?> cl = Class.forName(conn.driver);
    	
    	Database database = (Database) cl.newInstance();
        database.setProperty("create-database", "true");
        
        // entry point for the API which enables you to get the Collection reference
        DatabaseManager.registerDatabase(database);
        
        // a collection of Resources stored within an XML database
        Collection col = null;
        XMLResource res = null;
        try { 
        	
        	System.out.println("[INFO] Retrieving the collection: " + COLLECTION_URI);
            col = getOrCreateCollection(COLLECTION_URI);
            
            /*
             *  create new XMLResource with a given id
             *  an id is assigned to the new resource if left empty (null)
             */
            System.out.println("[INFO] Inserting the document: " + OBAVESTENJE_ID);
            res = (XMLResource) col.createResource(OBAVESTENJE_ID, XMLResource.RESOURCE_TYPE);
            
            File f = new File(OBAVESTENJE_LOCATION);
            
            if(!f.canRead()) {
                System.out.println("[ERROR] Cannot read the file: " + OBAVESTENJE_LOCATION);
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            
            res.setContent(f);
            System.out.println("[INFO] Storing the document: " + res.getId());
            
            col.storeResource(res);
            System.out.println("[INFO] Done.");
            
            //------------------------------------------------------------------------------------------------------
            
            /*
             *  create new XMLResource with a given id
             *  an id is assigned to the new resource if left empty (null)
             */
            System.out.println("[INFO] Inserting the document: " + ZAHTEVCIR_ID);
            res = (XMLResource) col.createResource(ZAHTEVCIR_ID, XMLResource.RESOURCE_TYPE);
            
            File f1 = new File(ZAHTEVCIR_LOCATION);
            
            if(!f1.canRead()) {
                System.out.println("[ERROR] Cannot read the file: " + ZAHTEVCIR_LOCATION);
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            
            res.setContent(f1);
            System.out.println("[INFO] Storing the document: " + res.getId());
            
            col.storeResource(res);
            System.out.println("[INFO] Done.");
            
//            System.out.println("[INFO] Inserting the document: " + ZAHTEVCIR_ID);
//            res = (XMLResource) col.createResource(ZAHTEVCIR_ID, XMLResource.RESOURCE_TYPE);
            
 //------------------------------------------------------------------------------------------------------
            
            /*
             *  create new XMLResource with a given id
             *  an id is assigned to the new resource if left empty (null)
             */
            System.out.println("[INFO] Inserting the document: " + ZALBAODLUKA_ID);
            res = (XMLResource) col.createResource(ZALBAODLUKA_ID, XMLResource.RESOURCE_TYPE);
            
            File f2 = new File(ZALBAODLUKA_LOCATION);
            
            if(!f2.canRead()) {
                System.out.println("[ERROR] Cannot read the file: " + ZALBAODLUKA_LOCATION);
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            
            res.setContent(f2);
            System.out.println("[INFO] Storing the document: " + res.getId());
            
            col.storeResource(res);
            System.out.println("[INFO] Done.");
            
 //------------------------------------------------------------------------------------------------------
            
            /*
             *  create new XMLResource with a given id
             *  an id is assigned to the new resource if left empty (null)
             */
            System.out.println("[INFO] Inserting the document: " + ZALBACUTANJE_ID);
            res = (XMLResource) col.createResource(ZALBACUTANJE_ID, XMLResource.RESOURCE_TYPE);
            
            File f3 = new File(ZALBACUTANJE_LOCATION);
            
            if(!f3.canRead()) {
                System.out.println("[ERROR] Cannot read the file: " + ZALBACUTANJE_LOCATION);
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            
            res.setContent(f3);
            System.out.println("[INFO] Storing the document: " + res.getId());
            
            col.storeResource(res);
            System.out.println("[INFO] Done.");
            
//------------------------------------------------------------------------------------------------------
            
            /*
             *  create new XMLResource with a given id
             *  an id is assigned to the new resource if left empty (null)
             */
            System.out.println("[INFO] Inserting the document: " + RESENJE_ID);
            res = (XMLResource) col.createResource(RESENJE_ID, XMLResource.RESOURCE_TYPE);
            
            File f4 = new File(RESENJE_LOCATION);
            
            if(!f4.canRead()) {
                System.out.println("[ERROR] Cannot read the file: " + RESENJE_LOCATION);
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            
            res.setContent(f4);
            System.out.println("[INFO] Storing the document: " + res.getId());
            
            col.storeResource(res);
            System.out.println("[INFO] Done.");


            
        } finally {
            
        	//don't forget to cleanup
            if(res != null) {
                try { 
                	((EXistResource)res).freeResources(); 
                } catch (XMLDBException xe) {
                	xe.printStackTrace();
                }
            }
            
            if(col != null) {
                try { 
                	col.close(); 
                } catch (XMLDBException xe) {
                	xe.printStackTrace();
                }
            }
        }
        
        //inicijalizuje RDF bazi i upisuje u nju-------------------------------------------
        
        ConnectionProperties conn = AuthenticationUtilRDF.loadProperties();
        
        System.out.println("[INFO] " + InnitController.class.getSimpleName());
		
		// Referencing XML file with RDF data in attributes

		
		// Automatic extraction of RDF triples from XML file
		MetadataExtractor metadataExtractor = new MetadataExtractor();
		
		System.out.println("[INFO] Extracting metadata from RDFa attributes...");
		metadataExtractor.extractMetadata(
				new FileInputStream(new File(OBAVESTENJE_LOCATION)), 
				new FileOutputStream(new File(OBAVESTENJE_RDF_LOCATION)));
				
		
		// Loading a default model with extracted metadata
		Model model = ModelFactory.createDefaultModel();
		model.read(OBAVESTENJE_RDF_LOCATION);
		
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		
		model.write(out, SparqlUtil.NTRIPLES);
		
		System.out.println("[INFO] Extracted metadata as RDF/XML...");
		model.write(System.out, SparqlUtil.RDF_XML);

		
		// Writing the named graph
		System.out.println("[INFO] Populating named graph \"" + OBAVESTENJE_SPARQL_NAMED_GRAPH_URI + "\" with extracted metadata.");
		String sparqlUpdate = SparqlUtil.insertData(conn.dataEndpoint + OBAVESTENJE_SPARQL_NAMED_GRAPH_URI, new String(out.toByteArray()));
		System.out.println(sparqlUpdate);
		
		// UpdateRequest represents a unit of execution
		UpdateRequest update = UpdateFactory.create(sparqlUpdate);

		UpdateProcessor processor = UpdateExecutionFactory.createRemote(update, conn.updateEndpoint);
		processor.execute();
		System.out.println("[INFO] Done.");
		
		System.out.println("[INFO] Extracting metadata from RDFa attributes...");
		metadataExtractor.extractMetadata(
				new FileInputStream(new File(OBAVESTENJE_LOCATION)), 
				new FileOutputStream(new File(OBAVESTENJE_RDF_LOCATION)));
				
		//------------------------------------------------------------------------------------------------------------
		// Automatic extraction of RDF triples from XML file
		MetadataExtractor metadataExtractorZalbaOdluka = new MetadataExtractor();
		
		System.out.println("[INFO] Extracting metadata from RDFa attributes...");
		metadataExtractorZalbaOdluka.extractMetadata(
				new FileInputStream(new File(ZALBAODLUKA_LOCATION)), 
				new FileOutputStream(new File(ZALBAODLUKA_RDF_LOCATION)));
		// Loading a default model with extracted metadata
		Model modelZalbaOdluka = ModelFactory.createDefaultModel();
		modelZalbaOdluka.read(ZALBAODLUKA_RDF_LOCATION);
		
		ByteArrayOutputStream outZalbaOdluka = new ByteArrayOutputStream();
		
		modelZalbaOdluka.write(outZalbaOdluka, SparqlUtil.NTRIPLES);
		
		System.out.println("[INFO] Extracted metadata as RDF/XML...");
		modelZalbaOdluka.write(System.out, SparqlUtil.RDF_XML);

		
		// Writing the named graph
		System.out.println("[INFO] Populating named graph \"" + ZALBAODLUKA_SPARQL_NAMED_GRAPH_URI + "\" with extracted metadata.");
		String sparqlUpdateZalbaOdluka = SparqlUtil.insertData(conn.dataEndpoint + ZALBAODLUKA_SPARQL_NAMED_GRAPH_URI, new String(outZalbaOdluka.toByteArray()));
		System.out.println(sparqlUpdateZalbaOdluka);
		
		// UpdateRequest represents a unit of execution
		UpdateRequest updateZalbaOdluka = UpdateFactory.create(sparqlUpdateZalbaOdluka);

		UpdateProcessor processorZalbaOdluka = UpdateExecutionFactory.createRemote(updateZalbaOdluka, conn.updateEndpoint);
		processorZalbaOdluka.execute();
		System.out.println("[INFO] Done.");
		
		//------------------------------------------------------------------------------------------------------------
		// Automatic extraction of RDF triples from XML file
		MetadataExtractor metadataExtractorZahtevcir = new MetadataExtractor();
		
		System.out.println("[INFO] Extracting metadata from RDFa attributes...");
		metadataExtractorZahtevcir.extractMetadata(
				new FileInputStream(new File(ZAHTEVCIR_LOCATION)), 
				new FileOutputStream(new File(ZAHTEVCIR_RDF_LOCATION)));
		// Loading a default model with extracted metadata
		Model modelZahtevcir = ModelFactory.createDefaultModel();
		modelZahtevcir.read(ZAHTEVCIR_RDF_LOCATION);
		
		ByteArrayOutputStream outZahtevcir = new ByteArrayOutputStream();
		
		modelZahtevcir.write(outZahtevcir, SparqlUtil.NTRIPLES);
		
		System.out.println("[INFO] Extracted metadata as RDF/XML...");
		modelZahtevcir.write(System.out, SparqlUtil.RDF_XML);

		
		// Writing the named graph
		System.out.println("[INFO] Populating named graph \"" + ZAHTEVCIR_SPARQL_NAMED_GRAPH_URI + "\" with extracted metadata.");
		String sparqlUpdatZahtevcir = SparqlUtil.insertData(conn.dataEndpoint + ZAHTEVCIR_SPARQL_NAMED_GRAPH_URI, new String(outZahtevcir.toByteArray()));
		System.out.println(sparqlUpdatZahtevcir);
		
		// UpdateRequest represents a unit of execution
		UpdateRequest updateZahtevcir = UpdateFactory.create(sparqlUpdatZahtevcir);

		UpdateProcessor processorZahtevcir = UpdateExecutionFactory.createRemote(updateZahtevcir, conn.updateEndpoint);
		processorZahtevcir.execute();
		System.out.println("[INFO] Done.");
		
		//------------------------------------------------------------------------------------------------------------
		// Automatic extraction of RDF triples from XML file
		MetadataExtractor metadataExtractor2 = new MetadataExtractor();
		
		System.out.println("[INFO] Extracting metadata from RDFa attributes...");
		metadataExtractor2.extractMetadata(
				new FileInputStream(new File(ZALBACUTANJE_LOCATION)), 
				new FileOutputStream(new File(ZALBACUTANJE_RDF_LOCATION)));
		// Loading a default model with extracted metadata
		Model model2 = ModelFactory.createDefaultModel();
		model2.read(ZALBACUTANJE_RDF_LOCATION);
		
		ByteArrayOutputStream out3 = new ByteArrayOutputStream();
		
		model2.write(out3, SparqlUtil.NTRIPLES);
		
		System.out.println("[INFO] Extracted metadata as RDF/XML...");
		model2.write(System.out, SparqlUtil.RDF_XML);

		
		// Writing the named graph
		System.out.println("[INFO] Populating named graph \"" + ZALBACUTANJE_SPARQL_NAMED_GRAPH_URI + "\" with extracted metadata.");
		String sparqlUpdate2 = SparqlUtil.insertData(conn.dataEndpoint + ZALBACUTANJE_SPARQL_NAMED_GRAPH_URI, new String(out3.toByteArray()));
		System.out.println(sparqlUpdate2);
		
		// UpdateRequest represents a unit of execution
		UpdateRequest update2 = UpdateFactory.create(sparqlUpdate2);

		UpdateProcessor processor2 = UpdateExecutionFactory.createRemote(update2, conn.updateEndpoint);
		processor2.execute();
		System.out.println("[INFO] Done.");
		
		//------------------------------------------------------------------------------------------------------------
		// Automatic extraction of RDF triples from XML file
		MetadataExtractor metadataExtractorResenje = new MetadataExtractor();
		
		System.out.println("[INFO] Extracting metadata from RDFa attributes...");
		metadataExtractorResenje.extractMetadata(
				new FileInputStream(new File(RESENJE_LOCATION)), 
				new FileOutputStream(new File(RESENJE_RDF_LOCATION)));
		// Loading a default model with extracted metadata
		Model modelResenje = ModelFactory.createDefaultModel();
		modelResenje.read(RESENJE_RDF_LOCATION);
		
		ByteArrayOutputStream outResenje = new ByteArrayOutputStream();
		
		modelResenje.write(outResenje, SparqlUtil.NTRIPLES);
		
		System.out.println("[INFO] Extracted metadata as RDF/XML...");
		modelResenje.write(System.out, SparqlUtil.RDF_XML);

		
		// Writing the named graph
		System.out.println("[INFO] Populating named graph \"" + RESENJE_SPARQL_NAMED_GRAPH_URI + "\" with extracted metadata.");
		String sparqlUpdateResenje = SparqlUtil.insertData(conn.dataEndpoint + RESENJE_SPARQL_NAMED_GRAPH_URI, new String(outResenje.toByteArray()));
		System.out.println(sparqlUpdateResenje);
		
		// UpdateRequest represents a unit of execution
		UpdateRequest updateResenje = UpdateFactory.create(sparqlUpdateResenje);

		UpdateProcessor processorResenje = UpdateExecutionFactory.createRemote(updateResenje, conn.updateEndpoint);
		processorResenje.execute();
		System.out.println("[INFO] Done.");		
		
		

        
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	
	
	private static Collection getOrCreateCollection(String collectionUri) throws XMLDBException {
        return getOrCreateCollection(collectionUri, 0);
    }
    
    private static Collection getOrCreateCollection(String collectionUri, int pathSegmentOffset) throws XMLDBException {
        
        Collection col = DatabaseManager.getCollection(conn.uri + collectionUri, conn.user, conn.password);
        
        // create the collection if it does not exist
        if(col == null) {
        
         	if(collectionUri.startsWith("/")) {
                collectionUri = collectionUri.substring(1);
            }
            
        	String pathSegments[] = collectionUri.split("/");
            
        	if(pathSegments.length > 0) {
                StringBuilder path = new StringBuilder();
            
                for(int i = 0; i <= pathSegmentOffset; i++) {
                    path.append("/" + pathSegments[i]);
                }
                
                Collection startCol = DatabaseManager.getCollection(conn.uri + path, conn.user, conn.password);
                
                if (startCol == null) {
                	
                	// child collection does not exist
                    
                	String parentPath = path.substring(0, path.lastIndexOf("/"));
                    Collection parentCol = DatabaseManager.getCollection(conn.uri + parentPath, conn.user, conn.password);
                    
                    CollectionManagementService mgt = (CollectionManagementService) parentCol.getService("CollectionManagementService", "1.0");
                    
                    System.out.println("[INFO] Creating the collection: " + pathSegments[pathSegmentOffset]);
                    col = mgt.createCollection(pathSegments[pathSegmentOffset]);
                    
                    col.close();
                    parentCol.close();
                    
                } else {
                    startCol.close();
                }
            }
            return getOrCreateCollection(collectionUri, ++pathSegmentOffset);
        } else {
            return col;
        }
    }
}
