
package com.team.xml.model;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlMixed;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="podaci_o_organu"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="naziv_organa"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;simpleContent&gt;
 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
 *                           &lt;attribute name="property" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/simpleContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="sediste_organa"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;simpleContent&gt;
 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
 *                           &lt;attribute name="property" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/simpleContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="zahtev_na_osnovu"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="broj_clana" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/&gt;
 *                   &lt;element name="stav" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/&gt;
 *                   &lt;element name="zakon" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="mesto_i_datum"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="mesto" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="dan" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/&gt;
 *                   &lt;element name="mesec" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/&gt;
 *                   &lt;element name="godina" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="elementiZahteva"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="uvid_u_dokument" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *                   &lt;element name="posedovanje_informacije" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *                   &lt;element name="kopija_dokumenta" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *                   &lt;element name="dostavljanje_kopije_dokumenta"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="postom" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *                             &lt;element name="elektronskom_postom" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *                             &lt;element name="faksom" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *                             &lt;element name="drugi_nacin" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="informacije_na_koje_se_odnosi_zahtev" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="informacije_o_traziocu"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="trazioc"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="fizicko_lice"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="ime"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;simpleContent&gt;
 *                                             &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
 *                                               &lt;attribute name="property" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                             &lt;/extension&gt;
 *                                           &lt;/simpleContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                       &lt;element name="prezime"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;simpleContent&gt;
 *                                             &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
 *                                               &lt;attribute name="property" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                             &lt;/extension&gt;
 *                                           &lt;/simpleContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="pravno_lice" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="adresa"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="ulica" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="broj" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/&gt;
 *                             &lt;element name="grad" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="drugi_podaci_za_kontakt" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="mesto" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="datum" use="required" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "content"
})
@XmlRootElement(name = "zahtev")
public class Zahtev {

    @XmlElementRefs({
        @XmlElementRef(name = "podaci_o_organu", type = JAXBElement.class),
        @XmlElementRef(name = "zahtev_na_osnovu", type = JAXBElement.class),
        @XmlElementRef(name = "mesto_i_datum", type = JAXBElement.class),
        @XmlElementRef(name = "elementiZahteva", type = JAXBElement.class),
        @XmlElementRef(name = "informacije_na_koje_se_odnosi_zahtev", type = JAXBElement.class),
        @XmlElementRef(name = "informacije_o_traziocu", type = JAXBElement.class)
    })
    @XmlMixed
    protected List<Serializable> content;
    @XmlAttribute(name = "mesto", required = true)
    protected String mesto;
    @XmlAttribute(name = "datum", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar datum;
    @XmlAttribute(name = "id", required = true)
    protected String id;

    /**
     * Gets the value of the content property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the content property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link Zahtev.PodaciOOrganu }{@code >}
     * {@link JAXBElement }{@code <}{@link Zahtev.ZahtevNaOsnovu }{@code >}
     * {@link JAXBElement }{@code <}{@link Zahtev.MestoIDatum }{@code >}
     * {@link JAXBElement }{@code <}{@link Zahtev.ElementiZahteva }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link Zahtev.InformacijeOTraziocu }{@code >}
     * {@link String }
     * 
     * 
     */
    public List<Serializable> getContent() {
        if (content == null) {
            content = new ArrayList<Serializable>();
        }
        return this.content;
    }

    /**
     * Gets the value of the mesto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMesto() {
        return mesto;
    }

    /**
     * Sets the value of the mesto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMesto(String value) {
        this.mesto = value;
    }

    /**
     * Gets the value of the datum property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDatum() {
        return datum;
    }

    /**
     * Sets the value of the datum property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDatum(XMLGregorianCalendar value) {
        this.datum = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="uvid_u_dokument" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
     *         &lt;element name="posedovanje_informacije" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
     *         &lt;element name="kopija_dokumenta" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
     *         &lt;element name="dostavljanje_kopije_dokumenta"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="postom" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
     *                   &lt;element name="elektronskom_postom" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
     *                   &lt;element name="faksom" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
     *                   &lt;element name="drugi_nacin" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "uvidUDokument",
        "posedovanjeInformacije",
        "kopijaDokumenta",
        "dostavljanjeKopijeDokumenta"
    })
    public static class ElementiZahteva {

        @XmlElement(name = "uvid_u_dokument")
        protected boolean uvidUDokument;
        @XmlElement(name = "posedovanje_informacije")
        protected boolean posedovanjeInformacije;
        @XmlElement(name = "kopija_dokumenta")
        protected boolean kopijaDokumenta;
        @XmlElement(name = "dostavljanje_kopije_dokumenta", required = true)
        protected Zahtev.ElementiZahteva.DostavljanjeKopijeDokumenta dostavljanjeKopijeDokumenta;

        /**
         * Gets the value of the uvidUDokument property.
         * 
         */
        public boolean isUvidUDokument() {
            return uvidUDokument;
        }

        /**
         * Sets the value of the uvidUDokument property.
         * 
         */
        public void setUvidUDokument(boolean value) {
            this.uvidUDokument = value;
        }

        /**
         * Gets the value of the posedovanjeInformacije property.
         * 
         */
        public boolean isPosedovanjeInformacije() {
            return posedovanjeInformacije;
        }

        /**
         * Sets the value of the posedovanjeInformacije property.
         * 
         */
        public void setPosedovanjeInformacije(boolean value) {
            this.posedovanjeInformacije = value;
        }

        /**
         * Gets the value of the kopijaDokumenta property.
         * 
         */
        public boolean isKopijaDokumenta() {
            return kopijaDokumenta;
        }

        /**
         * Sets the value of the kopijaDokumenta property.
         * 
         */
        public void setKopijaDokumenta(boolean value) {
            this.kopijaDokumenta = value;
        }

        /**
         * Gets the value of the dostavljanjeKopijeDokumenta property.
         * 
         * @return
         *     possible object is
         *     {@link Zahtev.ElementiZahteva.DostavljanjeKopijeDokumenta }
         *     
         */
        public Zahtev.ElementiZahteva.DostavljanjeKopijeDokumenta getDostavljanjeKopijeDokumenta() {
            return dostavljanjeKopijeDokumenta;
        }

        /**
         * Sets the value of the dostavljanjeKopijeDokumenta property.
         * 
         * @param value
         *     allowed object is
         *     {@link Zahtev.ElementiZahteva.DostavljanjeKopijeDokumenta }
         *     
         */
        public void setDostavljanjeKopijeDokumenta(Zahtev.ElementiZahteva.DostavljanjeKopijeDokumenta value) {
            this.dostavljanjeKopijeDokumenta = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="postom" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
         *         &lt;element name="elektronskom_postom" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
         *         &lt;element name="faksom" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
         *         &lt;element name="drugi_nacin" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "postom",
            "elektronskomPostom",
            "faksom",
            "drugiNacin"
        })
        public static class DostavljanjeKopijeDokumenta {

            protected boolean postom;
            @XmlElement(name = "elektronskom_postom")
            protected boolean elektronskomPostom;
            protected boolean faksom;
            @XmlElement(name = "drugi_nacin", required = true)
            protected String drugiNacin;

            /**
             * Gets the value of the postom property.
             * 
             */
            public boolean isPostom() {
                return postom;
            }

            /**
             * Sets the value of the postom property.
             * 
             */
            public void setPostom(boolean value) {
                this.postom = value;
            }

            /**
             * Gets the value of the elektronskomPostom property.
             * 
             */
            public boolean isElektronskomPostom() {
                return elektronskomPostom;
            }

            /**
             * Sets the value of the elektronskomPostom property.
             * 
             */
            public void setElektronskomPostom(boolean value) {
                this.elektronskomPostom = value;
            }

            /**
             * Gets the value of the faksom property.
             * 
             */
            public boolean isFaksom() {
                return faksom;
            }

            /**
             * Sets the value of the faksom property.
             * 
             */
            public void setFaksom(boolean value) {
                this.faksom = value;
            }

            /**
             * Gets the value of the drugiNacin property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDrugiNacin() {
                return drugiNacin;
            }

            /**
             * Sets the value of the drugiNacin property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDrugiNacin(String value) {
                this.drugiNacin = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="trazioc"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="fizicko_lice"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="ime"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;simpleContent&gt;
     *                                   &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
     *                                     &lt;attribute name="property" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                                   &lt;/extension&gt;
     *                                 &lt;/simpleContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                             &lt;element name="prezime"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;simpleContent&gt;
     *                                   &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
     *                                     &lt;attribute name="property" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                                   &lt;/extension&gt;
     *                                 &lt;/simpleContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="pravno_lice" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="adresa"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="ulica" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="broj" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/&gt;
     *                   &lt;element name="grad" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="drugi_podaci_za_kontakt" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "trazioc",
        "adresa",
        "drugiPodaciZaKontakt"
    })
    public static class InformacijeOTraziocu {

        @XmlElement(required = true)
        protected Zahtev.InformacijeOTraziocu.Trazioc trazioc;
        @XmlElement(required = true)
        protected Zahtev.InformacijeOTraziocu.Adresa adresa;
        @XmlElement(name = "drugi_podaci_za_kontakt")
        @XmlSchemaType(name = "unsignedInt")
        protected long drugiPodaciZaKontakt;

        /**
         * Gets the value of the trazioc property.
         * 
         * @return
         *     possible object is
         *     {@link Zahtev.InformacijeOTraziocu.Trazioc }
         *     
         */
        public Zahtev.InformacijeOTraziocu.Trazioc getTrazioc() {
            return trazioc;
        }

        /**
         * Sets the value of the trazioc property.
         * 
         * @param value
         *     allowed object is
         *     {@link Zahtev.InformacijeOTraziocu.Trazioc }
         *     
         */
        public void setTrazioc(Zahtev.InformacijeOTraziocu.Trazioc value) {
            this.trazioc = value;
        }

        /**
         * Gets the value of the adresa property.
         * 
         * @return
         *     possible object is
         *     {@link Zahtev.InformacijeOTraziocu.Adresa }
         *     
         */
        public Zahtev.InformacijeOTraziocu.Adresa getAdresa() {
            return adresa;
        }

        /**
         * Sets the value of the adresa property.
         * 
         * @param value
         *     allowed object is
         *     {@link Zahtev.InformacijeOTraziocu.Adresa }
         *     
         */
        public void setAdresa(Zahtev.InformacijeOTraziocu.Adresa value) {
            this.adresa = value;
        }

        /**
         * Gets the value of the drugiPodaciZaKontakt property.
         * 
         */
        public long getDrugiPodaciZaKontakt() {
            return drugiPodaciZaKontakt;
        }

        /**
         * Sets the value of the drugiPodaciZaKontakt property.
         * 
         */
        public void setDrugiPodaciZaKontakt(long value) {
            this.drugiPodaciZaKontakt = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="ulica" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="broj" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/&gt;
         *         &lt;element name="grad" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "ulica",
            "broj",
            "grad"
        })
        public static class Adresa {

            @XmlElement(required = true)
            protected String ulica;
            @XmlSchemaType(name = "unsignedByte")
            protected short broj;
            @XmlSchemaType(name = "unsignedShort")
            protected int grad;

            /**
             * Gets the value of the ulica property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUlica() {
                return ulica;
            }

            /**
             * Sets the value of the ulica property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUlica(String value) {
                this.ulica = value;
            }

            /**
             * Gets the value of the broj property.
             * 
             */
            public short getBroj() {
                return broj;
            }

            /**
             * Sets the value of the broj property.
             * 
             */
            public void setBroj(short value) {
                this.broj = value;
            }

            /**
             * Gets the value of the grad property.
             * 
             */
            public int getGrad() {
                return grad;
            }

            /**
             * Sets the value of the grad property.
             * 
             */
            public void setGrad(int value) {
                this.grad = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="fizicko_lice"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="ime"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;simpleContent&gt;
         *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
         *                           &lt;attribute name="property" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *                         &lt;/extension&gt;
         *                       &lt;/simpleContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                   &lt;element name="prezime"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;simpleContent&gt;
         *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
         *                           &lt;attribute name="property" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *                         &lt;/extension&gt;
         *                       &lt;/simpleContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="pravno_lice" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "fizickoLice",
            "pravnoLice"
        })
        public static class Trazioc {

            @XmlElement(name = "fizicko_lice", required = true)
            protected Zahtev.InformacijeOTraziocu.Trazioc.FizickoLice fizickoLice;
            @XmlElement(name = "pravno_lice", required = true)
            protected Object pravnoLice;

            /**
             * Gets the value of the fizickoLice property.
             * 
             * @return
             *     possible object is
             *     {@link Zahtev.InformacijeOTraziocu.Trazioc.FizickoLice }
             *     
             */
            public Zahtev.InformacijeOTraziocu.Trazioc.FizickoLice getFizickoLice() {
                return fizickoLice;
            }

            /**
             * Sets the value of the fizickoLice property.
             * 
             * @param value
             *     allowed object is
             *     {@link Zahtev.InformacijeOTraziocu.Trazioc.FizickoLice }
             *     
             */
            public void setFizickoLice(Zahtev.InformacijeOTraziocu.Trazioc.FizickoLice value) {
                this.fizickoLice = value;
            }

            /**
             * Gets the value of the pravnoLice property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getPravnoLice() {
                return pravnoLice;
            }

            /**
             * Sets the value of the pravnoLice property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setPravnoLice(Object value) {
                this.pravnoLice = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="ime"&gt;
             *           &lt;complexType&gt;
             *             &lt;simpleContent&gt;
             *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
             *                 &lt;attribute name="property" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *               &lt;/extension&gt;
             *             &lt;/simpleContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *         &lt;element name="prezime"&gt;
             *           &lt;complexType&gt;
             *             &lt;simpleContent&gt;
             *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
             *                 &lt;attribute name="property" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *               &lt;/extension&gt;
             *             &lt;/simpleContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "ime",
                "prezime"
            })
            public static class FizickoLice {

                @XmlElement(required = true)
                protected Zahtev.InformacijeOTraziocu.Trazioc.FizickoLice.Ime ime;
                @XmlElement(required = true)
                protected Zahtev.InformacijeOTraziocu.Trazioc.FizickoLice.Prezime prezime;

                /**
                 * Gets the value of the ime property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Zahtev.InformacijeOTraziocu.Trazioc.FizickoLice.Ime }
                 *     
                 */
                public Zahtev.InformacijeOTraziocu.Trazioc.FizickoLice.Ime getIme() {
                    return ime;
                }

                /**
                 * Sets the value of the ime property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Zahtev.InformacijeOTraziocu.Trazioc.FizickoLice.Ime }
                 *     
                 */
                public void setIme(Zahtev.InformacijeOTraziocu.Trazioc.FizickoLice.Ime value) {
                    this.ime = value;
                }

                /**
                 * Gets the value of the prezime property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Zahtev.InformacijeOTraziocu.Trazioc.FizickoLice.Prezime }
                 *     
                 */
                public Zahtev.InformacijeOTraziocu.Trazioc.FizickoLice.Prezime getPrezime() {
                    return prezime;
                }

                /**
                 * Sets the value of the prezime property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Zahtev.InformacijeOTraziocu.Trazioc.FizickoLice.Prezime }
                 *     
                 */
                public void setPrezime(Zahtev.InformacijeOTraziocu.Trazioc.FizickoLice.Prezime value) {
                    this.prezime = value;
                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;simpleContent&gt;
                 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
                 *       &lt;attribute name="property" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
                 *     &lt;/extension&gt;
                 *   &lt;/simpleContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "value"
                })
                public static class Ime {

                    @XmlValue
                    protected String value;
                    @XmlAttribute(name = "property", required = true)
                    protected String property;

                    /**
                     * Gets the value of the value property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getValue() {
                        return value;
                    }

                    /**
                     * Sets the value of the value property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setValue(String value) {
                        this.value = value;
                    }

                    /**
                     * Gets the value of the property property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getProperty() {
                        return property;
                    }

                    /**
                     * Sets the value of the property property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setProperty(String value) {
                        this.property = value;
                    }

                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;simpleContent&gt;
                 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
                 *       &lt;attribute name="property" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
                 *     &lt;/extension&gt;
                 *   &lt;/simpleContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "value"
                })
                public static class Prezime {

                    @XmlValue
                    protected String value;
                    @XmlAttribute(name = "property", required = true)
                    protected String property;

                    /**
                     * Gets the value of the value property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getValue() {
                        return value;
                    }

                    /**
                     * Sets the value of the value property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setValue(String value) {
                        this.value = value;
                    }

                    /**
                     * Gets the value of the property property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getProperty() {
                        return property;
                    }

                    /**
                     * Sets the value of the property property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setProperty(String value) {
                        this.property = value;
                    }

                }

            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="mesto" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="dan" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/&gt;
     *         &lt;element name="mesec" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/&gt;
     *         &lt;element name="godina" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "mesto",
        "dan",
        "mesec",
        "godina"
    })
    public static class MestoIDatum {

        @XmlElement(required = true)
        protected String mesto;
        @XmlSchemaType(name = "unsignedByte")
        protected short dan;
        @XmlSchemaType(name = "unsignedByte")
        protected short mesec;
        @XmlSchemaType(name = "unsignedShort")
        protected int godina;

        /**
         * Gets the value of the mesto property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMesto() {
            return mesto;
        }

        /**
         * Sets the value of the mesto property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMesto(String value) {
            this.mesto = value;
        }

        /**
         * Gets the value of the dan property.
         * 
         */
        public short getDan() {
            return dan;
        }

        /**
         * Sets the value of the dan property.
         * 
         */
        public void setDan(short value) {
            this.dan = value;
        }

        /**
         * Gets the value of the mesec property.
         * 
         */
        public short getMesec() {
            return mesec;
        }

        /**
         * Sets the value of the mesec property.
         * 
         */
        public void setMesec(short value) {
            this.mesec = value;
        }

        /**
         * Gets the value of the godina property.
         * 
         */
        public int getGodina() {
            return godina;
        }

        /**
         * Sets the value of the godina property.
         * 
         */
        public void setGodina(int value) {
            this.godina = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="naziv_organa"&gt;
     *           &lt;complexType&gt;
     *             &lt;simpleContent&gt;
     *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
     *                 &lt;attribute name="property" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *               &lt;/extension&gt;
     *             &lt;/simpleContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="sediste_organa"&gt;
     *           &lt;complexType&gt;
     *             &lt;simpleContent&gt;
     *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
     *                 &lt;attribute name="property" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *               &lt;/extension&gt;
     *             &lt;/simpleContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "nazivOrgana",
        "sedisteOrgana"
    })
    public static class PodaciOOrganu {

        @XmlElement(name = "naziv_organa", required = true)
        protected Zahtev.PodaciOOrganu.NazivOrgana nazivOrgana;
        @XmlElement(name = "sediste_organa", required = true)
        protected Zahtev.PodaciOOrganu.SedisteOrgana sedisteOrgana;

        /**
         * Gets the value of the nazivOrgana property.
         * 
         * @return
         *     possible object is
         *     {@link Zahtev.PodaciOOrganu.NazivOrgana }
         *     
         */
        public Zahtev.PodaciOOrganu.NazivOrgana getNazivOrgana() {
            return nazivOrgana;
        }

        /**
         * Sets the value of the nazivOrgana property.
         * 
         * @param value
         *     allowed object is
         *     {@link Zahtev.PodaciOOrganu.NazivOrgana }
         *     
         */
        public void setNazivOrgana(Zahtev.PodaciOOrganu.NazivOrgana value) {
            this.nazivOrgana = value;
        }

        /**
         * Gets the value of the sedisteOrgana property.
         * 
         * @return
         *     possible object is
         *     {@link Zahtev.PodaciOOrganu.SedisteOrgana }
         *     
         */
        public Zahtev.PodaciOOrganu.SedisteOrgana getSedisteOrgana() {
            return sedisteOrgana;
        }

        /**
         * Sets the value of the sedisteOrgana property.
         * 
         * @param value
         *     allowed object is
         *     {@link Zahtev.PodaciOOrganu.SedisteOrgana }
         *     
         */
        public void setSedisteOrgana(Zahtev.PodaciOOrganu.SedisteOrgana value) {
            this.sedisteOrgana = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;simpleContent&gt;
         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
         *       &lt;attribute name="property" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *     &lt;/extension&gt;
         *   &lt;/simpleContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class NazivOrgana {

            @XmlValue
            protected String value;
            @XmlAttribute(name = "property", required = true)
            protected String property;

            /**
             * Gets the value of the value property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * Sets the value of the value property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

            /**
             * Gets the value of the property property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getProperty() {
                return property;
            }

            /**
             * Sets the value of the property property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setProperty(String value) {
                this.property = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;simpleContent&gt;
         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
         *       &lt;attribute name="property" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *     &lt;/extension&gt;
         *   &lt;/simpleContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class SedisteOrgana {

            @XmlValue
            protected String value;
            @XmlAttribute(name = "property", required = true)
            protected String property;

            /**
             * Gets the value of the value property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * Sets the value of the value property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

            /**
             * Gets the value of the property property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getProperty() {
                return property;
            }

            /**
             * Sets the value of the property property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setProperty(String value) {
                this.property = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="broj_clana" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/&gt;
     *         &lt;element name="stav" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/&gt;
     *         &lt;element name="zakon" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "brojClana",
        "stav",
        "zakon"
    })
    public static class ZahtevNaOsnovu {

        @XmlElement(name = "broj_clana")
        @XmlSchemaType(name = "unsignedByte")
        protected short brojClana;
        @XmlSchemaType(name = "unsignedByte")
        protected short stav;
        @XmlElement(required = true)
        protected String zakon;

        /**
         * Gets the value of the brojClana property.
         * 
         */
        public short getBrojClana() {
            return brojClana;
        }

        /**
         * Sets the value of the brojClana property.
         * 
         */
        public void setBrojClana(short value) {
            this.brojClana = value;
        }

        /**
         * Gets the value of the stav property.
         * 
         */
        public short getStav() {
            return stav;
        }

        /**
         * Sets the value of the stav property.
         * 
         */
        public void setStav(short value) {
            this.stav = value;
        }

        /**
         * Gets the value of the zakon property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getZakon() {
            return zakon;
        }

        /**
         * Sets the value of the zakon property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setZakon(String value) {
            this.zakon = value;
        }

    }

}
